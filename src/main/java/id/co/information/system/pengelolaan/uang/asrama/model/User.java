/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.model;

import id.co.information.system.pengelolaan.uang.asrama.constant.GenderConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.UserStatusConstant;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable{
    private static final long serialVersionUID = -3171600656085562565L;

    private Long id;
    private String nik;
    private String namaLengkap;
    private GenderConstant gender;
    private String alamat;
    private String noHp;
    private String username;
    private String password;
    private UserStatusConstant status;
    private String hakAkses;
    private Kamar kamar;
    private Date tglMasuk;
    private Long monthlyPayment;
    private String foto;
}
