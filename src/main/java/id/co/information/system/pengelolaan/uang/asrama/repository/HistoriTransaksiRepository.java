package id.co.information.system.pengelolaan.uang.asrama.repository;

import id.co.information.system.pengelolaan.uang.asrama.constant.FromTransactionConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.TipeTransaksiConstant;
import id.co.information.system.pengelolaan.uang.asrama.model.HistoriTransaksi;
import id.co.information.system.pengelolaan.uang.asrama.util.DateUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.SQLException;
import java.util.Optional;

public class HistoriTransaksiRepository extends CommonBaseRepository{

    private Optional<HistoriTransaksi> getFindOptional(String sql) {
        try {
            resultSet = find(sql);
            if (resultSet == null) {
                return Optional.empty();
            }
            if (resultSet.next()) {
                return Optional.of(historiTransaksiBuilder(resultSet));
            }
            return Optional.empty();
        } catch (SQLException e) {
            Logger.error("Error find optional data transaction : ", e, HistoriTransaksiRepository.class);
            return Optional.empty();
        } finally {
            closeConnection(true);
        }
    }

    private List<HistoriTransaksi> getFindList(String sql) {
        List<HistoriTransaksi> transList = new ArrayList<>();
        try {
            resultSet = find(sql);
            if (resultSet == null) {
                return transList;
            }
            while(resultSet.next()) {
                transList.add(historiTransaksiBuilder(resultSet));
            }
            return transList;
        } catch (SQLException e) {
            Logger.error("Error find list data transaction : ", e, HistoriTransaksiRepository.class);
            return transList;
        } finally {
            closeConnection(true);
        }
    }

    private HistoriTransaksi historiTransaksiBuilder(ResultSet resultSet) {
        try {
            String fromTransactionDb = resultSet.getString("transaksi_dari");
            HistoriTransaksi.HistoriTransaksiBuilder historiTransaksi = HistoriTransaksi.builder();
            historiTransaksi.id(Long.parseLong(resultSet.getString("id")));
            historiTransaksi.transaksiId(resultSet.getString("transaksi_id"));
            historiTransaksi.tglTransaksi(DateUtil.toDateTime(resultSet.getString("tgl_transaksi"), DateUtil.YYYY_MM_DD_HH_MM_SS));
            historiTransaksi.tipeTransaksi(TipeTransaksiConstant.MASUK.name().equals(resultSet.getString("tipe_transaksi")) ? TipeTransaksiConstant.MASUK : TipeTransaksiConstant.KELUAR);
            if (FromTransactionConstant.DONATUR.name().equals(fromTransactionDb)) {
                historiTransaksi.fromTransaction(FromTransactionConstant.DONATUR);
            } else if (FromTransactionConstant.ANGGOTA.name().equals(fromTransactionDb)) {
                historiTransaksi.fromTransaction(FromTransactionConstant.ANGGOTA);
                historiTransaksi.jatuhTempo(DateUtil.toDateTime(resultSet.getString("jatuh_tempo"), DateUtil.YYYY_MM_DD_HH_MM_SS));
            } else {
                historiTransaksi.fromTransaction(FromTransactionConstant.LAINNYA);
            }
            historiTransaksi.userId(Long.parseLong(resultSet.getString("user_id")));
            historiTransaksi.jangkaWaktu(Integer.parseInt(resultSet.getString("jangka_waktu")));
            historiTransaksi.monthlyPayment(Long.parseLong(resultSet.getString("total_per_bulan")));
            historiTransaksi.donaturId(Long.parseLong(resultSet.getString("donatur_id")));
            historiTransaksi.namaTransaksi(resultSet.getString("nama_transaksi"));
            historiTransaksi.namaLengkap(resultSet.getString("nama_lengkap"));
            historiTransaksi.keterangan(resultSet.getString("keterangan"));
            historiTransaksi.total(Long.parseLong(resultSet.getString("total")));
            historiTransaksi.sisaSaldo(Long.parseLong(resultSet.getString("sisa_saldo")));
            historiTransaksi.createAt(resultSet.getString("create_at"));
            return historiTransaksi.build();
        } catch (NumberFormatException | SQLException e) {
            Logger.error("Error Builder donatur : ", e, HistoriTransaksiRepository.class);
            return HistoriTransaksi.builder().build();
        }
    }

    public Boolean save(HistoriTransaksi historiTransaksi) {
        try {
            String query;
            if (FromTransactionConstant.ANGGOTA.equals(historiTransaksi.getFromTransaction())) {

                query = "INSERT INTO histori_transaksi (transaksi_id,tgl_transaksi,tipe_transaksi,transaksi_dari,user_id,jangka_waktu,total_per_bulan,donatur_id,nama_transaksi,nama_lengkap,keterangan,total,sisa_saldo,create_at,jatuh_tempo)VALUES('"+
                        historiTransaksi.getTransaksiId()+"','"+
                        DateUtil.toString(historiTransaksi.getTglTransaksi(), DateUtil.YYYY_MM_DD_HH_MM_SS)+"','"+
                        historiTransaksi.getTipeTransaksi()+"','"+
                        historiTransaksi.getFromTransaction()+"','"+
                        historiTransaksi.getUserId()+"','"+
                        historiTransaksi.getJangkaWaktu()+"','"+
                        historiTransaksi.getMonthlyPayment()+"','"+
                        historiTransaksi.getDonaturId()+"','"+
                        historiTransaksi.getNamaTransaksi()+"','"+
                        historiTransaksi.getNamaLengkap()+"','"+
                        historiTransaksi.getKeterangan()+"','"+
                        historiTransaksi.getTotal()+"','"+
                        historiTransaksi.getSisaSaldo()+"','"+
                        historiTransaksi.getCreateAt()+"','"+
                        DateUtil.toString(historiTransaksi.getJatuhTempo(), DateUtil.YYYY_MM_DD_HH_MM_SS)+"')";
            } else {
                query = "INSERT INTO histori_transaksi (transaksi_id,tgl_transaksi,tipe_transaksi,transaksi_dari,user_id,jangka_waktu,total_per_bulan,donatur_id,nama_transaksi,nama_lengkap,keterangan,total,sisa_saldo,create_at)VALUES('"+
                        historiTransaksi.getTransaksiId()+"','"+
                        DateUtil.toString(historiTransaksi.getTglTransaksi(), DateUtil.YYYY_MM_DD_HH_MM_SS)+"','"+
                        historiTransaksi.getTipeTransaksi()+"','"+
                        historiTransaksi.getFromTransaction()+"','"+
                        historiTransaksi.getUserId()+"','"+
                        historiTransaksi.getJangkaWaktu()+"','"+
                        historiTransaksi.getMonthlyPayment()+"','"+
                        historiTransaksi.getDonaturId()+"','"+
                        historiTransaksi.getNamaTransaksi()+"','"+
                        historiTransaksi.getNamaLengkap()+"','"+
                        historiTransaksi.getKeterangan()+"','"+
                        historiTransaksi.getTotal()+"','"+
                        historiTransaksi.getSisaSaldo()+"','"+
                        historiTransaksi.getCreateAt()+"')";
            }
            return execute(query);
        } catch (Exception e) {
            Logger.error("Error query save transaction history : ", e, HistoriTransaksiRepository.class);
            return false;
        } finally {
            closeConnection(false);
        }
    }

    @SuppressWarnings("all")
    public Optional<HistoriTransaksi> findAllOrderByIdDescLimit1() {
        return getFindOptional("SELECT * FROM histori_transaksi ORDER BY id DESC LIMIT 1");
    }

    @SuppressWarnings("all")
    public Optional<HistoriTransaksi> findByTransaksiIdContainingIgnoreCaseOrderByTransaksiIdLimit1(String dateString) {
        return getFindOptional("SELECT * FROM histori_transaksi WHERE transaksi_id LIKE '%"+ dateString +"%' ORDER BY transaksi_id DESC LIMIT 1");
    }

    @SuppressWarnings("all")
    public List<HistoriTransaksi> findAll() {
        return getFindList("SELECT * FROM histori_transaksi ORDER BY tgl_transaksi DESC");
    }

    @SuppressWarnings("all")
    public Optional<HistoriTransaksi> findByTransaksiId(String transaksiId) {
        return getFindOptional("SELECT * FROM histori_transaksi WHERE transaksi_id='"+ transaksiId +"'");
    }

    @SuppressWarnings("all")
    public Optional<HistoriTransaksi> findFirstByUserIdOrderByTglTransaksiDesc(Long userId) {
        return getFindOptional("SELECT * FROM histori_transaksi WHERE user_id='"+ userId +"' ORDER BY tgl_transaksi DESC LIMIT 1");
    }

    @SuppressWarnings("all")
    public List<HistoriTransaksi> findByTglTransaksiContainingIgnoreCaseOrderByTglTransaksiDesc(String tglTransaksi) {
        return getFindList("SELECT * FROM histori_transaksi WHERE tgl_transaksi LIKE '%"+ tglTransaksi +"%' ORDER BY transaksi_id DESC");
    }

    @SuppressWarnings("all")
    public List<HistoriTransaksi> findByJatuhTempoLessThanEqualAndJatuhTempoContainingIgnorCaseOrderByJatuhTempoDesc(String jatuhTempo1, String jatuhTempo2) {
        return getFindList("SELECT * FROM histori_transaksi WHERE jatuh_tempo <= '"+ jatuhTempo1 +"' OR jatuh_tempo LIKE '%"+ jatuhTempo2 +"%' ORDER BY jatuh_tempo DESC");
    }

    @SuppressWarnings("all")
    public Optional<HistoriTransaksi> findFirstByUserIdOrderByJatuhTempoDesc(Long userId) {
        return getFindOptional("SELECT * FROM histori_transaksi WHERE user_id='"+ userId +"' ORDER BY jatuh_tempo DESC LIMIT 1");
    }

    @SuppressWarnings("all")
    public List<HistoriTransaksi> findByFromTransaksiOrderByTglTransaksiDesc(String fromTransaksi) {
        return getFindList("SELECT * FROM histori_transaksi WHERE transaksi_dari='"+ fromTransaksi +"' ORDER BY tgl_transaksi DESC");
    }

    @SuppressWarnings("all")
    public List<HistoriTransaksi> findByTipeTransaksiOrderByTglTransaksiDesc(String tipeTransaksi) {
        return getFindList("SELECT * FROM histori_transaksi WHERE tipe_transaksi='"+ tipeTransaksi +"' ORDER BY tgl_transaksi DESC");
    }

    @SuppressWarnings("all")
    public List<HistoriTransaksi> findByTglTransaksiGreaterThenEqualAndTglTransaksiLessThenEqualOrderByTglTransaksiDesc(String tglTransaksi1, String tglTransaksi2) {
        return getFindList("SELECT * FROM histori_transaksi WHERE tgl_transaksi BETWEEN '"+ tglTransaksi1 +" 00:00:00' AND '"+ tglTransaksi2 +" 23:59:59' ORDER BY tgl_transaksi DESC");
    }

    @SuppressWarnings("all")
    public List<HistoriTransaksi> findByTipeTransaksiAndTglTransaksiGreaterThenEqualAndTglTransaksiLessThenEqualOrderByTglTransaksiDesc(String tipeTransaksi, String tglTransaksi1, String tglTransaksi2) {
        return getFindList("SELECT * FROM histori_transaksi WHERE tipe_transaksi='"+ tipeTransaksi +"' AND tgl_transaksi BETWEEN '"+ tglTransaksi1 +"' AND '"+ tglTransaksi2 +"' ORDER BY tgl_transaksi DESC");
    }

    @SuppressWarnings("all")
    public List<HistoriTransaksi> findByUserIdOrderByTransactionDateDesc(Long userId) {
        return getFindList("SELECT * FROM histori_transaksi WHERE user_id='"+ userId +"' ORDER BY tgl_transaksi DESC");
    }

    @SuppressWarnings("all")
    public List<HistoriTransaksi> findByUserIdAndTransactionDateGreaterThenEqualAndTransactionDateLessThenEqualOrderByTransactionDateDesc(Long userId, Date fromTransactionDate, Date toTransactionDate) {
        return getFindList("SELECT * FROM histori_transaksi WHERE user_id='"+ userId +"' AND tgl_transaksi BETWEEN '"+ DateUtil.toString(fromTransactionDate, DateUtil.YYYY_MM_DD) +" 00:00:00' AND '"+ DateUtil.toString(toTransactionDate, DateUtil.YYYY_MM_DD) +" 23:59:59' ORDER BY tgl_transaksi DESC");
    }
    
    @SuppressWarnings("all")
    public Optional<HistoriTransaksi> findByDonaturIdLimit1(Long donaturId) {
        return getFindOptional("SELECT * FROM histori_transaksi WHERE donatur_id='"+ donaturId +"' LIMIT 1");
    }
    
    @SuppressWarnings("all")
    public Optional<HistoriTransaksi> findByUserIdLimit1(Long userId) {
        return getFindOptional("SELECT * FROM histori_transaksi WHERE user_id='"+ userId +"' LIMIT 1");
    }
}
