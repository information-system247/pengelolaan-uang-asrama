package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CalculateMonthlyPaymentResponse implements Serializable {
    private static final long serialVersionUID = 5322939772383408050L;

    private Long totalMonthlyPayment;
}
