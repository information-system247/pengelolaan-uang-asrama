package id.co.information.system.pengelolaan.uang.asrama.service;

import id.co.information.system.pengelolaan.uang.asrama.constant.HakAksesConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.StatusKamarConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.UserStatusConstant;
import id.co.information.system.pengelolaan.uang.asrama.dto.Response;
import id.co.information.system.pengelolaan.uang.asrama.dto.room.GetRemainningRoomCapacityResponse;
import id.co.information.system.pengelolaan.uang.asrama.dto.room.RemainningRoomCapacityDto;
import id.co.information.system.pengelolaan.uang.asrama.dto.user.*;
import id.co.information.system.pengelolaan.uang.asrama.model.Kamar;
import id.co.information.system.pengelolaan.uang.asrama.model.User;
import id.co.information.system.pengelolaan.uang.asrama.repository.KamarRepository;
import id.co.information.system.pengelolaan.uang.asrama.repository.UserRepository;
import id.co.information.system.pengelolaan.uang.asrama.util.AESUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;
import id.co.information.system.pengelolaan.uang.asrama.util.ResponseUtil;

import java.util.*;
import java.util.stream.Collectors;

import static id.co.information.system.pengelolaan.uang.asrama.constant.InfoConstant.REQUEST;
import static id.co.information.system.pengelolaan.uang.asrama.constant.MessageCodeConstant.*;
import id.co.information.system.pengelolaan.uang.asrama.model.HistoriTransaksi;
import id.co.information.system.pengelolaan.uang.asrama.repository.HistoriTransaksiRepository;

public class UserService {

    private final UserRepository userRepository = new UserRepository();
    private final KamarRepository kamarRepository = new KamarRepository();
    private final HistoriTransaksiRepository historiTransaksiRepository = new HistoriTransaksiRepository();

    public Response getUserList() {
        Logger.info("start getUserList", UserService.class);
        try {
            List<User> userList = userRepository.findAll();
            if (userList.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            List<GetUserDto> getUserDtoList = userList.stream()
                    .map(user -> GetUserDto.builder()
                            .nik(user.getNik())
                            .namaLengkap(user.getNamaLengkap())
                            .gender(user.getGender())
                            .alamat(user.getAlamat())
                            .noHp(user.getNoHp())
                            .namaPengguna(user.getUsername())
                            .kataSandi(user.getPassword())
                            .tglMasuk(user.getTglMasuk())
                            .userStatus(user.getStatus())
                            .noKamar(user.getKamar().getNoKamar())
                            .bilAmount(user.getMonthlyPayment())
                            .hakAkses(HakAksesConstant.valueOf(user.getHakAkses()))
                            .build())
                    .collect(Collectors.toList());

            return ResponseUtil.buildSuccessResponse(GetUserListResponse.builder()
                    .userDtoList(getUserDtoList)
                    .build());
        } catch (Exception e) {
            Logger.error("Error get user list : ", e, UserService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response getUserById(GetUserByIdRequest request) {
        Logger.info("start getUserById", UserService.class);
        Logger.info(REQUEST, request, UserService.class);
        try {
            if (request.getId() == null) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }
            Optional<User> userOptional = userRepository.findById(request.getId());
            if (userOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            return ResponseUtil.buildSuccessResponse(GetUserByIdResponse.builder()
                    .user(userOptional.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error get user by id : ", e, UserService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response getUserByNik(GetUserByNikRequest request) {
        Logger.info("start getUserByNik", UserService.class);
        Logger.info(REQUEST, request, UserService.class);
        try {
            if (request.getNik()== null || request.getNik().isEmpty()) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }
            Optional<User> userOptional = userRepository.findByNik(request.getNik());
            if (userOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            return ResponseUtil.buildSuccessResponse(GetUserByNikResponse.builder()
                    .user(userOptional.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error get user by nik : ", e, UserService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response storeUser(StoreUserRequest request) {
        Logger.info("start storeUser", UserService.class);
        Logger.info(REQUEST, request, UserService.class);
        try {
            if (request.getUser().getNik() == null || request.getUser().getNik().isEmpty() || 
                    request.getUser().getNamaLengkap() == null || request.getUser().getNamaLengkap().isEmpty() || 
                    request.getUser().getGender() == null || request.getUser().getAlamat() == null || request.getUser().getAlamat().isEmpty() ||
                    request.getUser().getNoHp() == null || request.getUser().getNoHp().isEmpty() || 
                    request.getUser().getUsername() == null || request.getUser().getUsername().isEmpty() || 
                    request.getUser().getPassword() == null || request.getUser().getPassword().isEmpty() ||
                    request.getUser().getHakAkses() == null || request.getUser().getKamar().getNoKamar() == null || request.getUser().getKamar().getNoKamar().isEmpty() || 
                    request.getUser().getMonthlyPayment() == null) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Optional<User> useOptional = userRepository.findByUsername(request.getUser().getUsername());
            if (useOptional.isPresent()) {
                return ResponseUtil.buildPendingResponse(USERNAME_FOUND);
            }

            Optional<User> userOptional1 = userRepository.findByNik(request.getUser().getNik());
            if (userOptional1.isPresent()) {
                ResponseUtil.buildPendingResponse(NIK_FOUND);
            }

            Optional<User> userOptional2 = userRepository.findByHakAkses(request.getUser().getHakAkses());
            if (userOptional2.isPresent() && HakAksesConstant.BENDAHARA.name().equals(userOptional2.get().getHakAkses())) {
                return ResponseUtil.buildPendingResponse(BENDAHARA_IS_USED);
            }

            Optional<Kamar> kamarOptional = kamarRepository.findByNoKamar(request.getUser().getKamar().getNoKamar());
            if (kamarOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            List<User> userList = userRepository.findByNoKamarIn(List.of(request.getUser().getKamar().getNoKamar()));
            userList = userList.stream()
                    .filter(user -> HakAksesConstant.ANGGOTA.name().equals(user.getHakAkses()))
                    .filter(user -> UserStatusConstant.AKTIF.equals(user.getStatus()))
                    .collect(Collectors.toList());
            if (userList.size() > kamarOptional.get().getKapasitas()) {
                return ResponseUtil.buildPendingResponse(FULL_ROOM);
            }

            User user = User.builder()
                    .nik(request.getUser().getNik())
                    .namaLengkap(request.getUser().getNamaLengkap())
                    .gender(request.getUser().getGender())
                    .alamat(request.getUser().getAlamat())
                    .noHp(request.getUser().getNoHp())
                    .username(request.getUser().getUsername())
                    .password(AESUtil.encrypt(request.getUser().getPassword()))
                    .status(UserStatusConstant.AKTIF)
                    .hakAkses(request.getUser().getHakAkses())
                    .kamar(kamarOptional.get())
                    .tglMasuk(new Date())
                    .monthlyPayment(request.getUser().getMonthlyPayment())
                    .foto(request.getUser().getFoto())
                    .build();

            Boolean storeUser = userRepository.save(user);
            if (Boolean.FALSE.equals(storeUser)) {
                return ResponseUtil.buildPendingResponse(SQL_ERROR);
            }

            List<User> users = userRepository.findByNoKamarIn(List.of(user.getKamar().getNoKamar()));
            if (users.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            Kamar.KamarBuilder kamarBuilder = Kamar.builder();
            kamarBuilder.id(kamarOptional.get().getId());
            kamarBuilder.noKamar(kamarOptional.get().getNoKamar());
            kamarBuilder.kapasitas(kamarOptional.get().getKapasitas());
            if (kamarOptional.get().getKapasitas() - users.size() == 0) {
                kamarBuilder.statusKamar(StatusKamarConstant.PENUH);
            } else {
                kamarBuilder.statusKamar(StatusKamarConstant.ISI);
            }

            Boolean updateKamar = kamarRepository.update(kamarBuilder.build());
            if (Boolean.FALSE.equals(updateKamar)) {
                return ResponseUtil.buildPendingResponse(SQL_ERROR);
            }

            Optional<User> userOptional = userRepository.findByNik(user.getNik());
            return ResponseUtil.buildSuccessResponse(StoreUserResponse.builder()
                    .user(userOptional.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error store user : ", e, UserService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response updateUser(StoreUserRequest request) {
        Logger.info("start updateUser", UserService.class);
        Logger.info(REQUEST, request, UserService.class);
        try {
            if (request.getUser().getNik() == null || request.getUser().getNik().isEmpty() || 
                    request.getUser().getNamaLengkap() == null || request.getUser().getNamaLengkap().isEmpty() || 
                    request.getUser().getGender() == null || request.getUser().getAlamat() == null || request.getUser().getAlamat().isEmpty() ||
                    request.getUser().getNoHp() == null || request.getUser().getNoHp().isEmpty() || 
                    request.getUser().getUsername() == null || request.getUser().getUsername().isEmpty() || 
                    request.getUser().getPassword() == null || request.getUser().getPassword().isEmpty() || request.getUser().getMonthlyPayment() == null ||
                    request.getUser().getHakAkses() == null || request.getUser().getKamar().getNoKamar() == null || request.getUser().getKamar().getNoKamar().isEmpty() || 
                    request.getUser().getTglMasuk() == null || request.getUser().getFoto() == null) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Optional<User> userOptional = userRepository.findByNik(request.getUser().getNik());
            if (userOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            if (request.getUser().getPassword().equals(userOptional.get().getPassword())) {
                request.getUser().setPassword(AESUtil.decrypt(request.getUser().getPassword()));
            }

            Optional<User> userOptional1 = userRepository.findByUsername(request.getUser().getUsername());
            if (userOptional1.isPresent() && !userOptional1.get().getNik().equals(request.getUser().getNik())) {
                return ResponseUtil.buildPendingResponse(USERNAME_FOUND);
            }

            Optional<Kamar> kamarOptional = kamarRepository.findByNoKamar(request.getUser().getKamar().getNoKamar());
            if (kamarOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            List<User> userList = userRepository.findByNoKamarIn(List.of(request.getUser().getKamar().getNoKamar()));
            userList = userList.stream()
                    .filter(user -> HakAksesConstant.ANGGOTA.name().equals(user.getHakAkses()))
                    .filter(user -> UserStatusConstant.AKTIF.equals(user.getStatus()))
                    .collect(Collectors.toList());
            if (userList.size() > kamarOptional.get().getKapasitas()) {
                return ResponseUtil.buildPendingResponse(FULL_ROOM);
            }

            User user = User.builder()
                    .nik(request.getUser().getNik())
                    .namaLengkap(request.getUser().getNamaLengkap())
                    .gender(request.getUser().getGender())
                    .alamat(request.getUser().getAlamat())
                    .noHp(request.getUser().getNoHp())
                    .username(request.getUser().getUsername())
                    .password(AESUtil.encrypt(request.getUser().getPassword()))
                    .status(request.getUser().getStatus())
                    .hakAkses(request.getUser().getHakAkses())
                    .kamar(kamarOptional.get())
                    .tglMasuk(request.getUser().getTglMasuk())
                    .monthlyPayment(request.getUser().getMonthlyPayment())
                    .foto(request.getUser().getFoto())
                    .build();

            Boolean updateUser = userRepository.update(user);
            if (Boolean.FALSE.equals(updateUser)) {
                return ResponseUtil.buildPendingResponse(SQL_ERROR);
            }

            Logger.info("status synchronized is : ", synckamar(), UserService.class);
            Optional<User> userOptional2 = userRepository.findByNik(user.getNik());
            return ResponseUtil.buildSuccessResponse(StoreUserResponse.builder()
                    .user(userOptional2.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error updated user : ", e, UserService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response login(GetUserByUsernameAndPasswordRequest request) {
        Logger.info("start login", UserService.class);
        Logger.info(REQUEST, request, UserService.class);
        try {
            if (request.getUsername() == null || request.getUsername().isEmpty() || 
                    request.getPassword() == null || request.getPassword().isEmpty()) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Optional<User> userOptional = userRepository.findByUsernameAndPassword(request.getUsername(), AESUtil.encrypt(request.getPassword()));
            if (userOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(USERNAME_OR_PASSWORD_WRONG);
            }

            return ResponseUtil.buildSuccessResponse(GetUserByUsernameAndPasswordResponse.builder()
                    .user(userOptional.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error login : ", e, UserService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response getUserListNotExistById(GetUserByIdRequest request) {
        Logger.info("start getUserListNotExistById", UserService.class);
        Logger.info(REQUEST, request, UserService.class);
        try {
            if (request.getId() == null) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            List<User> userList = userRepository.findNotExistId(request.getId());
            if (userList.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            List<GetUserDto> getUserDtoList = userList.stream()
                    .map( user -> GetUserDto.builder()
                            .nik(user.getNik())
                            .alamat(user.getAlamat())
                            .bilAmount(user.getMonthlyPayment())
                            .gender(user.getGender())
                            .kataSandi(user.getPassword())
                            .namaLengkap(user.getNamaLengkap())
                            .namaPengguna(user.getUsername())
                            .noHp(user.getNoHp())
                            .noKamar(user.getKamar().getNoKamar())
                            .tglMasuk(user.getTglMasuk())
                            .userStatus(user.getStatus())
                            .build())
                    .collect(Collectors.toList());

            return ResponseUtil.buildSuccessResponse(GetUserListResponse.builder()
                    .userDtoList(getUserDtoList)
                    .build());
        } catch (Exception e) {
            Logger.error("Error get user list not exist by id : ", e, UserService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response deleteUser(GetUserByNikRequest request) {
        Logger.info("start deleteUser", UserService.class);
        Logger.info(REQUEST, request, UserService.class);
        try {
            if (request.getNik()== null || request.getNik().isEmpty()) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Optional<User> userOptional1 = userRepository.findByNik(request.getNik());
            if (userOptional1.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            User user = User.builder()
                    .id(userOptional1.get().getId())
                    .nik(userOptional1.get().getNik())
                    .namaLengkap(userOptional1.get().getNamaLengkap())
                    .gender(userOptional1.get().getGender())
                    .alamat(userOptional1.get().getAlamat())
                    .noHp(userOptional1.get().getNoHp())
                    .username(userOptional1.get().getUsername())
                    .password(userOptional1.get().getPassword())
                    .status(UserStatusConstant.TIDAK_AKTIF)
                    .hakAkses(userOptional1.get().getHakAkses())
                    .kamar(userOptional1.get().getKamar())
                    .tglMasuk(userOptional1.get().getTglMasuk())
                    .monthlyPayment(userOptional1.get().getMonthlyPayment())
                    .foto(userOptional1.get().getFoto())
                    .build();
            Boolean deleteUser = userRepository.update(user);
            if (Boolean.FALSE.equals(deleteUser)) {
                return ResponseUtil.buildPendingResponse(SQL_ERROR);
            }

            Logger.info("status synchronized is : ", synckamar(), UserService.class);

            return ResponseUtil.buildSuccessResponse(DeleteUserResponse.builder()
                    .success(deleteUser)
                    .build());
        } catch (Exception e) {
            Logger.error("Error deleted user : ", e, UserService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response getRemainingRoomCapacity() {
        Logger.info("start getRemainingRoomCapacity", UserService.class);
        try {
            List<User> userList = userRepository.findByStatus(UserStatusConstant.AKTIF.name());
            Map<Kamar, Long> remainingRoomCapacityMap = userList.stream().collect(Collectors.groupingBy(User::getKamar, Collectors.counting()));

            List<RemainningRoomCapacityDto> remainingRoomCapacityList = new ArrayList<>();
            remainingRoomCapacityMap.forEach((kamar, amount) ->
                    remainingRoomCapacityList.add(RemainningRoomCapacityDto.builder()
                            .kamar(kamar)
                            .amount(kamar.getKapasitas() - amount)
                            .build())
            );

            return ResponseUtil.buildSuccessResponse(GetRemainningRoomCapacityResponse.builder()
                    .remainningRoomCapacityList(remainingRoomCapacityList)
                    .build());
        } catch (Exception e) {
            Logger.error("Error get remaining room capacity : ", e, UserService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    private Boolean synckamar() {
        try {
            List<Kamar> kamarList = kamarRepository.findAll();
            if (kamarList.isEmpty()) {
                return Boolean.FALSE;
            }

            List<String> noKamarList = kamarList.stream()
                    .map(Kamar::getNoKamar)
                    .collect(Collectors.toList());

            List<User> userList = userRepository.findByNoKamarIn(noKamarList);
            if (userList.isEmpty()) {
                return Boolean.FALSE;
            }

            userList = userList.stream()
                    .filter(user -> UserStatusConstant.AKTIF.equals(user.getStatus()))
                    .collect(Collectors.toList());

            Map<Kamar, Long> usedRoomCapacityMap = userList.stream().collect(Collectors.groupingBy(User::getKamar, Collectors.counting()));
            usedRoomCapacityMap.forEach((kamar, amount) -> {
                int sisa = kamar.getKapasitas() - amount.intValue();
                if (sisa == 0) {
                    kamarRepository.update(Kamar.builder()
                            .id(kamar.getId())
                            .noKamar(kamar.getNoKamar())
                            .kapasitas(kamar.getKapasitas())
                            .statusKamar(StatusKamarConstant.PENUH)
                            .build());
                }
            });

            kamarList = kamarRepository.findNotExistsByUser();
            if (kamarList.isEmpty()) {
                return Boolean.FALSE;
            }

            kamarList.forEach( kamar -> {
                if (StatusKamarConstant.ISI.equals(kamar.getStatusKamar())) {
                    kamarRepository.update(Kamar.builder()
                            .id(kamar.getId())
                            .noKamar(kamar.getNoKamar())
                            .kapasitas(kamar.getKapasitas())
                            .statusKamar(StatusKamarConstant.KOSONG)
                            .build());
                }
            });

            return Boolean.TRUE;
        } catch (Exception e) {
            Logger.error("Error synchronized room : ", e, UserService.class);
            return false;
        }
    }

    public Response getUserByHakAksesAndStatus(GetUserByHakAksesAndStatusRequest request) {
        Logger.info("start getUserByHakAksesAndStatus", UserService.class);
        Logger.info(REQUEST, request, UserService.class);
        try {
            if (request.getHakAkses() == null || request.getUserStatus() == null) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            List<User> userList = userRepository.findByHakAksesAndStatusOrderByNamaLengkapAsc(request.getHakAkses().name(), request.getUserStatus().name());
            if (userList.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            return ResponseUtil.buildSuccessResponse(GetUserByHakAksesAndStatusResponse.builder()
                    .userList(userList)
                    .build());
        } catch (Exception e) {
            Logger.error("Error get user by access rights and status : ", e, UserService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }
    
    public Response deleteUserById(GetUserByNikRequest request) {
        Logger.info("start deleteUserById", UserService.class);
        Logger.info(REQUEST, UserService.class);
        try {
            if (request.getNik()== null) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }
            
            Optional<User> userOptional = userRepository.findByNik(request.getNik());
            if (userOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }
            
            Optional<HistoriTransaksi> historiTransaksiOptional = historiTransaksiRepository.findByUserIdLimit1(userOptional.get().getId());
            if (historiTransaksiOptional.isPresent()) {
                return ResponseUtil.buildPendingResponse(CANNOT_DELETED);
            }
            
            Boolean deleteUser = userRepository.deleteById(userOptional.get().getId());
            if (Boolean.FALSE.equals(deleteUser)) {
                return ResponseUtil.buildPendingResponse(SQL_ERROR);
            }
            
            Logger.info("status synchronized is : ", synckamar(), UserService.class);
            return ResponseUtil.buildSuccessResponse(DeleteUserResponse.builder()
                        .success(deleteUser)
                        .build());
        } catch (Exception e) {
            Logger.error("Error deleted anggota : ", e, UserService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }
}
