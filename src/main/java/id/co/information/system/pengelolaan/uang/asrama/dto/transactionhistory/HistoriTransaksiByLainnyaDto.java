package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HistoriTransaksiByLainnyaDto implements Serializable {
    private static final long serialVersionUID = -6266478731004001847L;

    private Date tglTransaksi;
    private String namaTranaksi;
    private String fullName;
    private Long amount;
    private String keterangan;
}
