package id.co.information.system.pengelolaan.uang.asrama.dto.room;

import id.co.information.system.pengelolaan.uang.asrama.constant.StatusKamarConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreKamarRequest implements Serializable {
    private static final long serialVersionUID = 2099260196898466493L;

    private String noKamar;
    private int kapasitas;
    private StatusKamarConstant statusKamar;
}
