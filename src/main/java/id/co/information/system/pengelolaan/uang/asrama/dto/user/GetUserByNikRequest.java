package id.co.information.system.pengelolaan.uang.asrama.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUserByNikRequest implements Serializable {
    private static final long serialVersionUID = 7707368754498120559L;

    private String nik;
}
