package id.co.information.system.pengelolaan.uang.asrama.dto.donatur;

import id.co.information.system.pengelolaan.uang.asrama.constant.GenderConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreDonaturRequest implements Serializable {
    private static final long serialVersionUID = 694569679372863305L;

    private Long id;
    private String nik;
    private String namaLengkap;
    private GenderConstant gender;
    private String alamat;
    private String noHp;
}
