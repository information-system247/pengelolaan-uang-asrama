/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.util;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import static id.co.information.system.pengelolaan.uang.asrama.constant.EncryptConstant.ALGORITHM;
import static id.co.information.system.pengelolaan.uang.asrama.constant.EncryptConstant.SECRET_KEY;

/**
 *
 * @author shinriokudo
 */
public class AESUtil {
    private static SecretKey secretKeySpec;

    public static void prepareSecreteKey(String myKey) {
        MessageDigest sha;
        try {
            byte[] key = myKey.getBytes(StandardCharsets.UTF_8);
            sha = MessageDigest.getInstance("SHA-512");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKeySpec = new SecretKeySpec(key, ALGORITHM.getKey());
        } catch (NoSuchAlgorithmException e) {
            Logger.error("error in method prepareSecreteKey : ", e, AESUtil.class);
        }
    }

    public static String encrypt(String pass) {
        try {
            prepareSecreteKey(SECRET_KEY.getKey());
            Cipher cipher = Cipher.getInstance(ALGORITHM.getKey());
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            return Base64.getEncoder().encodeToString(cipher.doFinal(pass.getBytes(StandardCharsets.UTF_8)));
        } catch (InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            Logger.error("Error while encrypting : {}", e, AESUtil.class);
            return null;
        }
    }

    public static String decrypt(String pass) {
        try {
            prepareSecreteKey(SECRET_KEY.getKey());
            Cipher cipher = Cipher.getInstance(ALGORITHM.getKey());
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            return new String(cipher.doFinal(Base64.getDecoder().decode(pass)));
        } catch (InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            Logger.error("Error while decrypting : {}", e, AESUtil.class);
        }
        return null;
    }
}
