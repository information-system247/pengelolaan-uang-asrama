package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import id.co.information.system.pengelolaan.uang.asrama.model.HistoriTransaksi;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetSisaSaldoResponse implements Serializable {
    private static final long serialVersionUID = 8591384814824399776L;

    private HistoriTransaksi historiTransaksi;
}
