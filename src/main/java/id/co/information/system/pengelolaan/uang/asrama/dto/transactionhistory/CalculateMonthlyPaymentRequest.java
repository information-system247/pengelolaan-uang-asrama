package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CalculateMonthlyPaymentRequest implements Serializable {
    private static final long serialVersionUID = 7215402998845798539L;

    private String nik;
    private Integer jangkaWaktu;
}
