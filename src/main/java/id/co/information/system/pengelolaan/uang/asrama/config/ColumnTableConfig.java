/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.config;

import id.co.information.system.pengelolaan.uang.asrama.constant.ColumnTableConstant;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import javax.swing.JTable;
import static javax.swing.JTable.AUTO_RESIZE_OFF;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author shinriokudo
 */
public class ColumnTableConfig extends ColumnTableConstant{
    
    public final DefaultTableModel modelDaftarKamarKosongInBerandaAdmin = new DefaultTableModel(ColumnTableConstant.DAFTAR_KAMAR_KOSONG_BERANDA_ADMIN, 0);
    public final DefaultTableModel modelDaftarKeterlambatanInBerandaAdmin = new DefaultTableModel(ColumnTableConstant.DAFTAR_KETERLAMBATAN_BERANDA_ADMIN, 0);
    public final DefaultTableModel modelDaftarJatuhTempoInBerandaAdmin = new DefaultTableModel(ColumnTableConstant.DAFTAR_JATUH_TEMPO_BERANDA_ADMIN, 0);
    public final DefaultTableModel modelDaftarKamar = new DefaultTableModel(ColumnTableConstant.DAFTAR_KAMAR, 0);
    public final DefaultTableModel modelDaftarPenyewa = new DefaultTableModel(ColumnTableConstant.DAFTAR_PENYEWA, 0);
    public final DefaultTableModel modelDaftarDonatur = new DefaultTableModel(ColumnTableConstant.DAFTAR_DONATUR, 0);
    public final DefaultTableModel modelDaftarTransaksiMasukFromPenyewa = new DefaultTableModel(ColumnTableConstant.DAFTAR_TRANSAKSI_MASUK_FROM_PENYEWA, 0);
    public final DefaultTableModel modelDaftarTransaksiMasukFromDonatur = new DefaultTableModel(ColumnTableConstant.DAFTAR_TRANSAKSI_MASUK_FROM_DONATUR, 0);
    public final DefaultTableModel modelDaftarTransaksiMasukFromLainnya = new DefaultTableModel(ColumnTableConstant.DAFTAR_TRANSAKSI_MASUK_FROM_LAINNYA, 0);
    public final DefaultTableModel modelDaftarTransaksiKeluar = new DefaultTableModel(ColumnTableConstant.DAFTAR_TRANSAKSI_KELUAR, 0);
    public final DefaultTableModel modelDaftarLaporanSemuaTransaksi = new DefaultTableModel(ColumnTableConstant.DAFTAR_LAPORAN_TRANSAKSI_SEMUA, 0);
    public final DefaultTableModel modelDaftarLaporanTransaksiMasuk = new DefaultTableModel(ColumnTableConstant.DAFTAR_LAPORAN_TRANSAKSI_MASUK, 0);
    public final DefaultTableModel modelDaftarLaporanTransaksiKeluar = new DefaultTableModel(ColumnTableConstant.DAFTAR_LAPORAN_TRANSAKSI_KELUAR, 0);
    public final DefaultTableModel modelDaftarLaporanTransaksiPenyewa = new DefaultTableModel(ColumnTableConstant.DAFTAR_LAPORAN_TRANSAKSI_PENYEWA, 0);
    public final DefaultTableModel modelDaftarLaporanTunggakan = new DefaultTableModel(ColumnTableConstant.DAFTAR_KETERLAMBATAN_BERANDA_ADMIN, 0);
    
    private void setResizeTable(List<Integer> columnWidthList, JTable tableName) {
        try {
            tableName.setAutoResizeMode(AUTO_RESIZE_OFF);
            AtomicInteger counter = new AtomicInteger(0);
            columnWidthList.forEach( res -> tableName.getColumnModel().getColumn(counter.getAndIncrement()).setPreferredWidth(res));
        } catch (Exception e) {
            Logger.error("Error resize table : ", e, ColumnTableConfig.class);
            throw e;
        }
    }
    public void setColumnTableBeranda (JTable dueDateTable ,JTable overDueTable, JTable roomEmptyTable) {
        try {
            setResizeTable(List.of(100, 250, 360, 200, 200), dueDateTable);
            setResizeTable(List.of(100, 250, 360, 200, 200), overDueTable);
            setResizeTable(List.of(100, 350, 220, 220, 220), roomEmptyTable);
        } catch (Exception e) {
            Logger.error("Error set treasurer table column : ", e, ColumnTableConfig.class);
            throw e;
        }
    }
    
    public void setColumnTableDonatur(JTable donaturTable) {
        try {
            setResizeTable(List.of(50, 150, 250, 150, 200, 150), donaturTable);
        } catch (Exception e) {
            Logger.error("Error set donatur table column : ", e, ColumnTableConfig.class);
            throw e;
        }
    }
    
    public void setColumnTableKamar(JTable roomTable) {
        try {
            setResizeTable(List.of(100, 250, 150, 150, 150), roomTable);
        } catch (Exception e) {
            Logger.error("Error set room table column : ", e, ColumnTableConfig.class);
            throw e;
        }
      
    }
    
    public void setColumnTablePenyewa (JTable renterTable) {
        try {
            setResizeTable(List.of(50, 150, 300, 100, 300, 150, 200, 150, 150, 150, 200, 150), renterTable);
        } catch (Exception e) {
            Logger.error("Error set renter table column : ", e, ColumnTableConfig.class);
            throw e;
        }
        
    }
    
    public void setColumnTableTransaksiMasuk(JTable renterTable, JTable donaturTable, JTable otherTable) {
        try {
            setResizeTable(List.of(50, 150, 300, 150, 150, 150, 150, 150), renterTable);
            setResizeTable(List.of(50, 150, 350, 200, 250), donaturTable);
            setResizeTable(List.of(50, 150, 150, 350, 150, 200), otherTable);
        } catch (Exception e) {
            Logger.error("Error set incoming transaction table column : ", e, ColumnTableConfig.class);
            throw e;
        }
    }
    
    public void setColumnTableTransaksiKeluar(JTable outgoingTransactionTable) {
        try {
            setResizeTable(List.of(100, 250, 200, 350), outgoingTransactionTable); 
        } catch (Exception e) {
            Logger.error("Error set outgoing transaction table column : ", e, ColumnTableConfig.class);
            throw e;
        }
   
    }
    
    public void setColumnTableLaporan( JTable allTable, JTable incomingTable, JTable outgoingTable) {
        try {
            setResizeTable(List.of(50, 150, 150, 350, 150, 200), allTable);
            setResizeTable(List.of(50, 150, 100, 350, 150, 150, 150, 100, 200), incomingTable);
            setResizeTable(List.of(50, 150, 150, 350), outgoingTable);
        } catch (Exception e) {
            Logger.error("Error set report table column : ", e, ColumnTableConfig.class);
            throw e;
        }
    }
    
    public void setColumnTableLaporanTunggakan(JTable tableTunggakan) {
        try {
            setResizeTable(List.of(100, 250, 360, 200, 200), tableTunggakan);
        } catch (Exception e) {
            Logger.error("Error set report table column : ", e, ColumnTableConfig.class);
            throw e;
        }
    }
    
    public void setColumnTableLaporanPenyewa (JTable renterTable) {
        try {
            setResizeTable(List.of(100, 200, 200, 200, 200), renterTable);
        } catch (Exception e) {
            Logger.error("Error set renter report table column : ", e, ColumnTableConfig.class);
            throw e;
        }
    }
}
