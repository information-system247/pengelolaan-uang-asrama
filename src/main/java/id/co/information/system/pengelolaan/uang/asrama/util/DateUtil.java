/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author shinriokudo
 */
public class DateUtil {
    
    public static final String YYYYMMDD = "yyyyMMdd";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_DATE_TIME_IDN = "dd MMMM yyyy HH:mm:ss";
    public static final String FORMAT_DATE_TIME_IDN_V2 = "dd-MMM-yyyy HH:mm:ss.sss";
    public static final String FORMAT_DATE_TIME_IDN_V3 = "EEEE, dd MMMM yyyy HH:mm:ss";
    public static final String FORMAT_DATE_IDN = "dd MMMM yyyy";
    public static final String FORMAT_DATE = "MMM d, y";
    public static final String FORMAT_DATE_IDN_V2 = "dd-MMM-yyy";
    
    private static void timeZoneIDN(SimpleDateFormat dateFormat) {
        dateFormat.setTimeZone(TimeZone.getTimeZone(ZoneId.of("Asia/Jakarta")));
    }
    
    private static void timeZoneIDN(Calendar calendar) {
        calendar.setTimeZone(TimeZone.getTimeZone(ZoneId.of("Asia/Jakarta")));
    }
    
    public static Date toDateTime(String dateString, String pattern) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
            DateUtil.timeZoneIDN(dateFormat);
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            Logger.error("Error parse date : ", e, DateUtil.class);
            return null;
        }
    }
    
    public static String toString(Date date, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        DateUtil.timeZoneIDN(dateFormat);
        return dateFormat.format(date);
    }
    
    public static Date addMonth(Date date, int month)
    {
        try {
            Calendar cal = Calendar.getInstance();
            DateUtil.timeZoneIDN(cal);
            cal.setTime(date);
            cal.add(Calendar.MONTH, month);
            return cal.getTime();
        } catch (Exception e) {
            Logger.error("addMonth : ", e, DateUtil.class);
            throw e;
        }
    }
}
