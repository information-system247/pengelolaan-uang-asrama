package id.co.information.system.pengelolaan.uang.asrama.repository;

import id.co.information.system.pengelolaan.uang.asrama.config.DatabaseConnectionConfig;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;

import java.sql.*;

public class CommonBaseRepository {

    protected Connection connection;
    protected Statement statement;
    protected PreparedStatement preparedStatement;
    protected ResultSet resultSet;

    protected ResultSet find(String query) {
        try {
            connection = DatabaseConnectionConfig.get();
            statement = connection.createStatement();
            return statement.executeQuery(query);
        } catch (SQLException e) {
            Logger.error("Error executeQuery : ", e, CommonBaseRepository.class);
            return null;
        }
    }

    protected Boolean execute(String query) {
        try {
            connection = DatabaseConnectionConfig.get();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.execute();
            return true;
        } catch (SQLException e) {
            Logger.error("Error executeQuery : ", e, CommonBaseRepository.class);
            return false;
        }
    }

    protected void closeConnection(boolean isFind) {
        try {
            connection.close();
            statement.close();
            if (isFind) {
                resultSet.close();
            } else {
                preparedStatement.close();
            }
        } catch (SQLException | NullPointerException e) {
            Logger.error("Error close connection database : {}", e, CommonBaseRepository.class);
        }
    }
}
