package id.co.information.system.pengelolaan.uang.asrama.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUserByIdRequest implements Serializable {
    private static final long serialVersionUID = -8681343139703590324L;

    private Long id;
}
