package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetHistoriTransaksiMasukListByFromTransaksiResponse implements Serializable {
    private static final long serialVersionUID = 8415733945081608555L;

    private Long sisaSaldo;
    private Long total;
    private List<HistoriTransaksiByDonaturDto> historiTransaksiByDonaturList;
    private List<HistoriTransaksiByPenyewaDto> historiTransaksiByPenyewaList;
    private List<HistoriTransaksiByLainnyaDto> historiTransaksiByLainnyaList;
}
