package id.co.information.system.pengelolaan.uang.asrama.dto.donatur;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetDonaturByNikRequest implements Serializable {
    private static final long serialVersionUID = -507896157448321048L;

    private String nik;
}
