package id.co.information.system.pengelolaan.uang.asrama.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUserListResponse implements Serializable {
    private static final long serialVersionUID = -5636756222557137619L;

    private List<GetUserDto> userDtoList;
}
