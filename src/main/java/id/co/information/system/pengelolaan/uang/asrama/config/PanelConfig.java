package id.co.information.system.pengelolaan.uang.asrama.config;

import id.co.information.system.pengelolaan.uang.asrama.util.Logger;

import javax.swing.*;

public class PanelConfig {

    public void setPanel(JPanel panelDasar, JPanel panel) {
        try {
            panelDasar.removeAll();
            panelDasar.repaint();
            panelDasar.revalidate();
            panelDasar.add(panel);
            panelDasar.repaint();
            panelDasar.revalidate();
        } catch (Exception e) {
            Logger.error("Error Exception : ", e, PanelConfig.class);
            throw e;
        }
    }
}
