/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package id.co.information.system.pengelolaan.uang.asrama.constant;

/**
 *
 * @author wahyu
 */
public class AsramaConstant {
    
    public static final String PATH_DOWNLOAD = "C:\\Users\\wahyu\\Downloads\\";
    
    public static final String LABEL_CARI_TRANSAKSI_MASUK_BENDAHARA_ANGGOTA = "Nama Anggota :";
    public static final String LABEL_CARI_TRANSAKSI_MASUK_BENDAHARA_DONATUR = "Nama Donatur : ";
}
