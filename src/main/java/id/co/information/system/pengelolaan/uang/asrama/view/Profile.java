/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.view;

import id.co.information.system.pengelolaan.uang.asrama.constant.MessageCodeConstant;
import id.co.information.system.pengelolaan.uang.asrama.dto.Response;
import id.co.information.system.pengelolaan.uang.asrama.dto.user.StoreUserRequest;
import id.co.information.system.pengelolaan.uang.asrama.model.Kamar;
import id.co.information.system.pengelolaan.uang.asrama.model.User;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;
import id.co.information.system.pengelolaan.uang.asrama.util.RadioButtonUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.RequestUtil;
import java.awt.Frame;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 *
 * @author shinriokudo
 */
public class Profile extends JDialog {

    private Main main = new Main();
    /**
     * Creates new form Profile
     */
    public Profile(Main main, Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.main = main;
        index();
    }
    
    private void index() {
        try {
            txtNamaLengkap.setText(main.getUserOptional().getUser().getNamaLengkap());
            txtNik.setText(main.getUserOptional().getUser().getNik());
            RadioButtonUtil.setGender(main.getUserOptional().getUser().getGender(), rbPria, rbWanita);
            txtAlamat.setText(main.getUserOptional().getUser().getAlamat());
            txtUsername.setText(main.getUserOptional().getUser().getUsername());
            txtPassword.setText(main.getUserOptional().getUser().getPassword());
            txtNoHp.setText(main.getUserOptional().getUser().getNoHp());
        } catch (Exception e) {
            Logger.error("Error user optional : ", e, Profile.class);
            throw e;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        txtUsername = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btnBatal = new javax.swing.JButton();
        btnSimpan = new javax.swing.JButton();
        txtNamaLengkap = new javax.swing.JTextField();
        lblNama = new javax.swing.JLabel();
        lblJumlahUang = new javax.swing.JLabel();
        txtPassword = new javax.swing.JPasswordField();
        jLabel9 = new javax.swing.JLabel();
        txtNik = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        rbPria = new javax.swing.JRadioButton();
        rbWanita = new javax.swing.JRadioButton();
        jLabel33 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txtAlamat = new javax.swing.JTextArea();
        jLabel32 = new javax.swing.JLabel();
        txtNoHp = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(1, 15, 30)));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Akun Anda");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(-1, 4, 520, 40));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 52, 520, 10));

        txtUsername.setEditable(false);
        txtUsername.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.add(txtUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 310, 260, 30));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Nama Lengkap :");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 70, 110, -1));

        btnBatal.setBackground(new java.awt.Color(239, 10, 10));
        btnBatal.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnBatal.setForeground(new java.awt.Color(255, 255, 255));
        btnBatal.setText("Batal");
        btnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBatalActionPerformed(evt);
            }
        });
        jPanel1.add(btnBatal, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 420, 180, 30));

        btnSimpan.setBackground(new java.awt.Color(0, 102, 0));
        btnSimpan.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSimpan.setForeground(new java.awt.Color(255, 255, 255));
        btnSimpan.setText("Simpan");
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });
        jPanel1.add(btnSimpan, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 420, 180, 30));

        txtNamaLengkap.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.add(txtNamaLengkap, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, 260, 30));

        lblNama.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblNama.setText("Nama Pengguna :");
        jPanel1.add(lblNama, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 280, 140, 30));

        lblJumlahUang.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblJumlahUang.setText("Kata Sandi");
        jPanel1.add(lblJumlahUang, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 290, -1, -1));

        txtPassword.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtPassword.setText("jPasswordField1");
        jPanel1.add(txtPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 310, 180, 30));

        jLabel9.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel9.setText("NIK :");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 70, 50, -1));

        txtNik.setEditable(false);
        txtNik.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtNik.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel1.add(txtNik, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 90, 170, 30));

        jLabel30.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel30.setText(" Gender :");
        jPanel1.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, -1, -1));

        rbPria.setBackground(new java.awt.Color(255, 255, 255));
        rbPria.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbPria.setText("Laki - Laki");
        rbPria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbPriaActionPerformed(evt);
            }
        });
        jPanel1.add(rbPria, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, -1, -1));

        rbWanita.setBackground(new java.awt.Color(255, 255, 255));
        rbWanita.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbWanita.setText("Perempuan");
        jPanel1.add(rbWanita, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 150, -1, -1));

        jLabel33.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel33.setText("Alamat :");
        jPanel1.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 180, -1, -1));

        txtAlamat.setColumns(20);
        txtAlamat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtAlamat.setLineWrap(true);
        txtAlamat.setRows(5);
        txtAlamat.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jScrollPane6.setViewportView(txtAlamat);

        jPanel1.add(jScrollPane6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 200, 260, 80));

        jLabel32.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel32.setText("No Hp :");
        jPanel1.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 350, -1, -1));

        txtNoHp.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtNoHp.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel1.add(txtNoHp, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 370, 260, 30));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 520, 480));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBatalActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
    }//GEN-LAST:event_btnBatalActionPerformed

    private void rbPriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbPriaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbPriaActionPerformed

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
        // TODO add your handling code here:
        Boolean noHpNumberOnly = RequestUtil.numberOnly(txtNoHp.getText());
        Boolean nameAlfabetOnly = RequestUtil.alfabetOnly(txtNamaLengkap.getText());
        Response response = main.getUserController().updateUser(StoreUserRequest.builder()
                .user(User.builder()
                        .nik(txtNik.getText())
                        .namaLengkap((Boolean.TRUE.equals(nameAlfabetOnly)) ? txtNamaLengkap.getText() : null)
                        .gender(RadioButtonUtil.getGender(rbPria, rbWanita))
                        .alamat(txtAlamat.getText())
                        .username(txtUsername.getText())
                        .password(String.valueOf(txtPassword.getPassword()))
                        .noHp((Boolean.TRUE.equals(noHpNumberOnly)) ? txtNoHp.getText() : null)
                        .status(main.getUserOptional().getUser().getStatus())
                        .hakAkses(main.getUserOptional().getUser().getHakAkses())
                        .kamar(Kamar.builder().noKamar(main.getUserOptional().getUser().getKamar().getNoKamar()).build())
                        .tglMasuk(main.getUserOptional().getUser().getTglMasuk())
                        .monthlyPayment(main.getUserOptional().getUser().getMonthlyPayment())
                        .build())
                .build());
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            JOptionPane.showMessageDialog(this, "Update data berhasil. Silahkan login kembali!");
            this.setVisible(false);
            main.setVisible(false);
            new Login().setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this, response.getResponseDto().getMessage());
        }
    }//GEN-LAST:event_btnSimpanActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(Profile.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(Profile.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(Profile.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(Profile.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new Profile().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBatal;
    private javax.swing.JButton btnSimpan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblJumlahUang;
    private javax.swing.JLabel lblNama;
    private javax.swing.JRadioButton rbPria;
    private javax.swing.JRadioButton rbWanita;
    private javax.swing.JTextArea txtAlamat;
    private javax.swing.JTextField txtNamaLengkap;
    private javax.swing.JTextField txtNik;
    private javax.swing.JTextField txtNoHp;
    private javax.swing.JPasswordField txtPassword;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables
}
