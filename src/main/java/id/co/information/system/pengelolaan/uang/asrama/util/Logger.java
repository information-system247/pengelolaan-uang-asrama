/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 *
 * @author shinriokudo
 */
public class Logger {
    
    private static String split(Class<?> classes) {
        return classes.toString().replace("class ", "");
    }
    
    public static void info(String message, Class<?> classes) {
        System.out.println(DateUtil.toString(new Date(), DateUtil.FORMAT_DATE_TIME_IDN_V2)+ " [INFO] " + split(classes)+ " - " + message);
    }
    
    public static void info(String message, Object value2, Class<?> classes) {
        String value = Logger.messageBuilder(message, List.of(value2));
        System.out.println(DateUtil.toString(new Date(), DateUtil.FORMAT_DATE_TIME_IDN_V2)+ " [INFO] " + split(classes)+ " - " + value);
    }
    
    public static void info(String message, Object value1, Object value2, Class<?> classes) {
        String value = Logger.messageBuilder(message, List.of(value1, value2));
        System.out.println(DateUtil.toString(new Date(), DateUtil.FORMAT_DATE_TIME_IDN_V2)+ " [INFO] " + split(classes)+ " - " + value);
    }
    
    public static void error(String message, Throwable th, Class<?> classes) {
        System.out.println(DateUtil.toString(new Date(), DateUtil.FORMAT_DATE_TIME_IDN_V2)+ " [ERROR] " + split(classes)+ " - " + message + th.getMessage());
    }
    
    public static void warn(String message, Object value2, Class<?> classes) {
        String value = Logger.messageBuilder(message, List.of(value2));
        System.out.println(DateUtil.toString(new Date(), DateUtil.FORMAT_DATE_TIME_IDN_V2)+ " [WARN] " + split(classes)+ " - " + value);
    }
    
    public static void warn(String message, Class<?> classes) {
        System.out.println(DateUtil.toString(new Date(), DateUtil.FORMAT_DATE_TIME_IDN_V2)+ " [WARN] " + split(classes)+ " - " + message);
    }
    
    public static void error(String message, Class<?> classes) {
        System.out.println(DateUtil.toString(new Date(), DateUtil.FORMAT_DATE_TIME_IDN_V2)+ " [ERROR] " + split(classes)+ " - " + message);
    }
    
    private static String messageBuilder(String message, List<Object> valueList) {
        String[] stringList = message.split("\\{}");
        List<String> messageList = Arrays.asList(stringList);
        String value = "";
        if (!messageList.isEmpty() && !valueList.isEmpty()) {
            int counter = messageList.size() - valueList.size();
            for (int i = 0; i < messageList.size(); i++) {
                if (i == 0) {
                    value = messageList.get(i) + valueList.get(i);
                } else if (counter == 1) {
                    value = value + messageList.get(i);
                }  else {
                    value = value + messageList.get(i) + valueList.get(i);
                }
            }
        }
        return value;
    }
}
