package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import id.co.information.system.pengelolaan.uang.asrama.model.HistoriTransaksi;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreHistoriTransaksiRequest implements Serializable {
    private static final long serialVersionUID = 4670879584808422356L;

    private HistoriTransaksi historiTransaksi;
}
