package id.co.information.system.pengelolaan.uang.asrama.dto.room;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetKamarByNoKamarRequest implements Serializable {
    private static final long serialVersionUID = -142954323816447799L;

    private String noKamar;
}
