/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.util;

import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author shinriokudo
 */
public class FormatCurencyUtil {
    
    public static String indonesia(Long amount) {
        try {
            String amountString = NumberFormat.getCurrencyInstance(new Locale("in", "ID")).format(amount);
            return amountString.replace("Rp", "Rp. ").replace(",00", "");
        } catch (Exception e) {
            Logger.error("Error format curency to IDN : ", e, FormatCurencyUtil.class);
            return null;
        }
    }
    
    public static Long toLong(String amount) {
        try {
            String fullAmount = amount.replace("Rp. ", "").replace(".", "");
            return Long.parseLong(fullAmount);
        } catch (NumberFormatException e) {
            Logger.error("Error parse numbering : {}", e, FormatCurencyUtil.class);
            return null;
        }
    }
}
