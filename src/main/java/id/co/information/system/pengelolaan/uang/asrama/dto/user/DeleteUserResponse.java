package id.co.information.system.pengelolaan.uang.asrama.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeleteUserResponse implements Serializable {
    private static final long serialVersionUID = -3002401401578193776L;

    private Boolean success;
}
