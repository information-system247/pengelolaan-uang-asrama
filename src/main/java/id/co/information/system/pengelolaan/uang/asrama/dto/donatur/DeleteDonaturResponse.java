package id.co.information.system.pengelolaan.uang.asrama.dto.donatur;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeleteDonaturResponse implements Serializable {
    private static final long serialVersionUID = -1814794892620284677L;

    private Boolean success;
}
