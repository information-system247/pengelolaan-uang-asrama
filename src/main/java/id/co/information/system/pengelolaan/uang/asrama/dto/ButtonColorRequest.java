/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.dto;

import id.co.information.system.pengelolaan.uang.asrama.constant.ButtonConstant;
import java.io.Serializable;
import javax.swing.JButton;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author shinriokudo
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ButtonColorRequest implements Serializable{
    private static final long serialVersionUID = -4627359119301611865L;

    private ButtonConstant btn;
    private JButton btnMenuDashboard;
    private JButton btnMenuKamar;
    private JButton btnMenuDonatur;
    private JButton btnMenuTransaksiMasuk;
    private JButton btnMenuTransaksiKeluar;
    private JButton btnMenuLaporan;
    private JButton btnMenuLaporanTunggakan;
    private JButton btnMenuPenyewa;
    private JButton btnMenuDashboardPenyewa;
    private JButton btnMenuLaporanPenyewa;
}
