package id.co.information.system.pengelolaan.uang.asrama.dto.donatur;

import id.co.information.system.pengelolaan.uang.asrama.model.Donatur;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetDonaturListResponse implements Serializable {
    private static final long serialVersionUID = -6049029462405590784L;
    
    private List<Donatur> donaturList;
}
