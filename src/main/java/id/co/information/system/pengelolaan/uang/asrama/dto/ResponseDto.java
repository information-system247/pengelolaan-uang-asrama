/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class ResponseDto<T> implements Serializable{
    private static final long serialVersionUID = -1506862858659883277L;

    private String messageCode;
    private String message;
    private T data;
    
    public ResponseDto() {
        
    }
    
    public ResponseDto(String messageCode) {
        this(messageCode, null, null);
    }
    
    public ResponseDto(String messageCode, String message) {
        this(messageCode, message, null);
    }
    
    public ResponseDto(String messageCode, T data) {
        this(messageCode, null, data);
    }
    
    public ResponseDto(String messageCode, String message, T data) {
        this.messageCode = messageCode;
        this.message = message;
        this.data = data;
    }
}
