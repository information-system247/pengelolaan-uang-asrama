/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.model;

import id.co.information.system.pengelolaan.uang.asrama.constant.StatusKamarConstant;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Kamar implements Serializable{
    private static final long serialVersionUID = -2305497688655868984L;

    private Long id;
    private String noKamar;
    private int kapasitas;
    private StatusKamarConstant statusKamar;
}
