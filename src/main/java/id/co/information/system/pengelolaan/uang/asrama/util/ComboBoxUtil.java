/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package id.co.information.system.pengelolaan.uang.asrama.util;

import id.co.information.system.pengelolaan.uang.asrama.annotation.Overide;
import java.util.List;
import javax.swing.JComboBox;

/**
 *
 * @author wahyu
 */
public class ComboBoxUtil {
    
    @Overide
    @SuppressWarnings("unchecked")
    public static void valueBuilder(JComboBox cbb, List<String> value) {
        cbb.removeAllItems();
        if (!value.isEmpty()) {
            value.forEach(
                    res -> cbb.addItem(res)
            );
            cbb.setSelectedIndex(0);
        }
    }
}
