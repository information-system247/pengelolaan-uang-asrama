package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetHistoriTransaksiKeluarListResponse implements Serializable {
    private static final long serialVersionUID = -8726713883893459305L;

    private Long sisaSaldo;
    private Long total;
    private List<HistoriTransaksiKeluarDto> historiTransaksiKeluarList;
}
