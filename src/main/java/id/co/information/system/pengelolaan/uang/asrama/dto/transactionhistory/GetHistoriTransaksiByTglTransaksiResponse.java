package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import id.co.information.system.pengelolaan.uang.asrama.model.HistoriTransaksi;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetHistoriTransaksiByTglTransaksiResponse implements Serializable {
    private static final long serialVersionUID = -5098047191111737243L;

    private List<HistoriTransaksi> historiTransaksiList;
}
