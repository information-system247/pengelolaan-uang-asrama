package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetDueDateOverDueDateRequest implements Serializable {
    private static final long serialVersionUID = -3267485615222721317L;

    private Boolean isDueDate;
}
