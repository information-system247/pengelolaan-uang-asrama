/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.model;

import id.co.information.system.pengelolaan.uang.asrama.constant.GenderConstant;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Donatur implements Serializable{
    private static final long serialVersionUID = -6045761027119513785L;

    private Long id;
    private String nik;
    private String namaLengkap;
    private GenderConstant gender;
    private String alamat;
    private String noHp;
}
