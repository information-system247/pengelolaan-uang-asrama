package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HistoriTransaksiByDonaturDto implements Serializable {
    private static final long serialVersionUID = -1169601612589101130L;

    private Date tglTransaksi;
    private String fullName;
    private Long amount;
    private String keterangan;
}
