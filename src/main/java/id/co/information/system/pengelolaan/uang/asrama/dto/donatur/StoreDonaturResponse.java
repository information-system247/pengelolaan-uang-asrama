package id.co.information.system.pengelolaan.uang.asrama.dto.donatur;

import id.co.information.system.pengelolaan.uang.asrama.model.Donatur;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreDonaturResponse implements Serializable {
    private static final long serialVersionUID = -5074422801892386677L;

    private Donatur donatur;
}
