/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.config;

import static id.co.information.system.pengelolaan.uang.asrama.constant.ButtonConstant.BERANDA;
import static id.co.information.system.pengelolaan.uang.asrama.constant.ButtonConstant.BERANDA_PENYEWA;
import static id.co.information.system.pengelolaan.uang.asrama.constant.ButtonConstant.KAMAR;
import static id.co.information.system.pengelolaan.uang.asrama.constant.ButtonConstant.LAPORAN;
import static id.co.information.system.pengelolaan.uang.asrama.constant.ButtonConstant.LAPORAN_PENYEWA;
import static id.co.information.system.pengelolaan.uang.asrama.constant.ButtonConstant.TRANSAKSI_KELUAR;
import static id.co.information.system.pengelolaan.uang.asrama.constant.ButtonConstant.TRANSAKSI_MASUK;
import id.co.information.system.pengelolaan.uang.asrama.dto.ButtonColorRequest;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;
import java.awt.Color;

/**
 *
 * @author shinriokudo
 */
public class ButtonConfig {
    
    public void setWarnaButton(ButtonColorRequest request) {
        try {
            switch(request.getBtn()) {
                case BERANDA:
                    request.getBtnMenuDashboard().setBackground(new Color(2,44,90));
                    request.getBtnMenuKamar().setBackground(new Color(1,15,30));
                    request.getBtnMenuDonatur().setBackground(new Color(1,15,30));
                    request.getBtnMenuTransaksiMasuk().setBackground(new Color(1,15,30));
                    request.getBtnMenuTransaksiKeluar().setBackground(new Color(1,15,30));
                    request.getBtnMenuLaporan().setBackground(new Color(1,15,30));
                    request.getBtnMenuLaporanTunggakan().setBackground(new Color(1,15,30));
                    request.getBtnMenuPenyewa().setBackground(new Color(1,15,30));
                    break;
                case DONATUR:
                    request.getBtnMenuDonatur().setBackground(new Color(2,44,90));
                    request.getBtnMenuKamar().setBackground(new Color(1,15,30));
                    request.getBtnMenuDashboard().setBackground(new Color(1,15,30));
                    request.getBtnMenuTransaksiMasuk().setBackground(new Color(1,15,30));
                    request.getBtnMenuTransaksiKeluar().setBackground(new Color(1,15,30));
                    request.getBtnMenuLaporan().setBackground(new Color(1,15,30));
                    request.getBtnMenuLaporanTunggakan().setBackground(new Color(1,15,30));
                    request.getBtnMenuPenyewa().setBackground(new Color(1,15,30));
                    break;
                case KAMAR:
                    request.getBtnMenuKamar().setBackground(new Color(2,44,90));
                    request.getBtnMenuDonatur().setBackground(new Color(1,15,30));
                    request.getBtnMenuDashboard().setBackground(new Color(1,15,30));
                    request.getBtnMenuTransaksiMasuk().setBackground(new Color(1,15,30));
                    request.getBtnMenuTransaksiKeluar().setBackground(new Color(1,15,30));
                    request.getBtnMenuLaporan().setBackground(new Color(1,15,30));
                    request.getBtnMenuLaporanTunggakan().setBackground(new Color(1,15,30));
                    request.getBtnMenuPenyewa().setBackground(new Color(1,15,30));
                    break;
                case TRANSAKSI_MASUK:
                    request.getBtnMenuTransaksiMasuk().setBackground(new Color(2,44,90));
                    request.getBtnMenuDonatur().setBackground(new Color(1,15,30));
                    request.getBtnMenuDashboard().setBackground(new Color(1,15,30));
                    request.getBtnMenuKamar().setBackground(new Color(1,15,30));
                    request.getBtnMenuTransaksiKeluar().setBackground(new Color(1,15,30));
                    request.getBtnMenuLaporan().setBackground(new Color(1,15,30));
                    request.getBtnMenuLaporanTunggakan().setBackground(new Color(1,15,30));
                    request.getBtnMenuPenyewa().setBackground(new Color(1,15,30));
                    break;
                case TRANSAKSI_KELUAR:
                    request.getBtnMenuTransaksiKeluar().setBackground(new Color(2,44,90));
                    request.getBtnMenuDonatur().setBackground(new Color(1,15,30));
                    request.getBtnMenuDashboard().setBackground(new Color(1,15,30));
                    request.getBtnMenuKamar().setBackground(new Color(1,15,30));
                    request.getBtnMenuTransaksiMasuk().setBackground(new Color(1,15,30));
                    request.getBtnMenuLaporan().setBackground(new Color(1,15,30));
                    request.getBtnMenuLaporanTunggakan().setBackground(new Color(1,15,30));
                    request.getBtnMenuPenyewa().setBackground(new Color(1,15,30));
                    break;
                case LAPORAN:
                    request.getBtnMenuLaporan().setBackground(new Color(2,44,90));
                    request.getBtnMenuDonatur().setBackground(new Color(1,15,30));
                    request.getBtnMenuDashboard().setBackground(new Color(1,15,30));
                    request.getBtnMenuKamar().setBackground(new Color(1,15,30));
                    request.getBtnMenuTransaksiKeluar().setBackground(new Color(1,15,30));
                    request.getBtnMenuTransaksiMasuk().setBackground(new Color(1,15,30));
                    request.getBtnMenuLaporanTunggakan().setBackground(new Color(1,15,30));
                    request.getBtnMenuPenyewa().setBackground(new Color(1,15,30));
                    break;
                case LAPORAN_TUNGGAKAN:
                    request.getBtnMenuLaporanTunggakan().setBackground(new Color(2,44,90));
                    request.getBtnMenuLaporan().setBackground(new Color(1,15,30));
                    request.getBtnMenuDonatur().setBackground(new Color(1,15,30));
                    request.getBtnMenuDashboard().setBackground(new Color(1,15,30));
                    request.getBtnMenuKamar().setBackground(new Color(1,15,30));
                    request.getBtnMenuTransaksiKeluar().setBackground(new Color(1,15,30));
                    request.getBtnMenuTransaksiMasuk().setBackground(new Color(1,15,30));
                    request.getBtnMenuPenyewa().setBackground(new Color(1,15,30));
                    break;
                case PENYEWA:
                    request.getBtnMenuPenyewa().setBackground(new Color(2,44,90));
                    request.getBtnMenuDonatur().setBackground(new Color(1,15,30));
                    request.getBtnMenuDashboard().setBackground(new Color(1,15,30));
                    request.getBtnMenuKamar().setBackground(new Color(1,15,30));
                    request.getBtnMenuTransaksiKeluar().setBackground(new Color(1,15,30));
                    request.getBtnMenuTransaksiMasuk().setBackground(new Color(1,15,30));
                    request.getBtnMenuLaporan().setBackground(new Color(1,15,30));
                    request.getBtnMenuLaporanTunggakan().setBackground(new Color(1,15,30));
                    break;
                case BERANDA_PENYEWA:
                    request.getBtnMenuDashboardPenyewa().setBackground(new Color(2,44,90));
                    request.getBtnMenuLaporanPenyewa().setBackground(new Color(1,15,30));
                    break;
                case LAPORAN_PENYEWA:
                    request.getBtnMenuLaporanPenyewa().setBackground(new Color(2,44,90));
                    request.getBtnMenuDashboardPenyewa().setBackground(new Color(1,15,30));
                    break;
            }
        } catch (Exception e) {
            Logger.error("Error Exception : ", e, ButtonConfig.class);
            throw e;
        }
    }
}
