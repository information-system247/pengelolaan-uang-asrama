package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import id.co.information.system.pengelolaan.uang.asrama.constant.TipeTransaksiConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HistoriTransaksiDto implements Serializable {
    private static final long serialVersionUID = -503415683720663053L;

    private Date tglTransaksi;
    private TipeTransaksiConstant tipeTransaksi;
    private String fullName;
    private Long total;
    private String keterangan;
}
