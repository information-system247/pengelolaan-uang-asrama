package id.co.information.system.pengelolaan.uang.asrama.repository;

import id.co.information.system.pengelolaan.uang.asrama.constant.HakAksesConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.StatusKamarConstant;
import id.co.information.system.pengelolaan.uang.asrama.model.Kamar;
import id.co.information.system.pengelolaan.uang.asrama.util.ListUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class KamarRepository extends CommonBaseRepository{

    private List<Kamar> getFindList(String sql) {
        List<Kamar> kamarList = new ArrayList<>();
        try {
            resultSet = find(sql);
            if (resultSet == null) {
                return kamarList;
            }
            while(resultSet.next()) {
                kamarList.add(kamarBuilder(resultSet));
            }
            return kamarList;
        } catch (SQLException e) {
            Logger.error("Error find list data room : ", e, KamarRepository.class);
            return kamarList;
        } finally {
            closeConnection(true);
        }
    }

    private Optional<Kamar> getFindOptional(String sql) {
        try {
            resultSet = find(sql);
            if (resultSet == null) {
                return Optional.empty();
            }
            if (resultSet.next()) {
                return Optional.of(kamarBuilder(resultSet));
            }
            return Optional.empty();
        } catch (SQLException e) {
            Logger.error("Error find optional data room : ", e, KamarRepository.class);
            return Optional.empty();
        } finally {
            closeConnection(true);
        }
    }

    private Kamar kamarBuilder(ResultSet resultSet) {
        try {
            return Kamar.builder()
                    .id(Long.parseLong(resultSet.getString("id")))
                    .noKamar(resultSet.getString("no_kamar"))
                    .kapasitas(Integer.parseInt(resultSet.getString("kapasitas")))
                    .statusKamar(StatusKamarConstant.valueOf(resultSet.getString("status")))
                    .build();
        } catch (NumberFormatException | SQLException e) {
            Logger.error("Error Builder room : ", e, KamarRepository.class);
            return Kamar.builder().build();
        }
    }

    public Boolean save(Kamar kamar) {
        try {
            String query = "INSERT INTO kamar (no_kamar,kapasitas,status)VALUES('"+
                    kamar.getNoKamar()+"','"+
                    kamar.getKapasitas()+"','"+
                    kamar.getStatusKamar()+"')";
            return execute(query);
        } catch (Exception e) {
            Logger.error("Error query save room : ", e, KamarRepository.class);
            return false;
        } finally {
            closeConnection(false);
        }
    }

    public Boolean update(Kamar kamar) {
        try {
            String query = "UPDATE`kamar`SET`kapasitas` = '"+kamar.getKapasitas()+
                    "',status = '"+kamar.getStatusKamar()+
                    "' WHERE no_kamar = '"+kamar.getNoKamar()+"'";
            return execute(query);
        } catch (Exception e) {
            Logger.error("Error query update room : ", e, KamarRepository.class);
            return false;
        } finally {
            closeConnection(false);
        }
    }

    public Boolean deleteByNoKamar(String noKamar) {
        try {
            String query = "DELETE FROM`kamar`WHERE no_kamar = '"+noKamar+"'";
            return execute(query);
        } catch (Exception e) {
            Logger.error("Error query delete room : ", e, KamarRepository.class);
            return false;
        } finally {
            closeConnection(false);
        }
    }

    @SuppressWarnings("all")
    public List<Kamar> findAll() {
        return getFindList("SELECT * FROM kamar ORDER BY no_kamar ASC");
    }

    @SuppressWarnings("all")
    public Optional<Kamar> findByNoKamar(String noKamar) {
        return getFindOptional("SELECT * FROM kamar WHERE no_kamar='"+ noKamar +"'");
    }

    @SuppressWarnings("all")
    public Optional<Kamar> findFisrtByNoKamarExistsUser(String noKamar) {
        return getFindOptional("SELECT * FROM kamar k JOIN user u WHERE k.no_kamar=u.no_kamar AND k.no_kamar='"+ noKamar +"' LIMIT 1");
    }

    @SuppressWarnings("all")
    public List<Kamar> findNotExistsByUser() {
        return getFindList("SELECT * FROM kamar k WHERE NOT EXISTS (SELECT 1 FROM user u WHERE u.no_kamar=k.no_kamar AND u.status='AKTIF')");
    }

    @SuppressWarnings("all")
    public List<Kamar> findByStatusIn(List<StatusKamarConstant> status) {
        return getFindList("SELECT * FROM kamar WHERE status IN('"+ ListUtil.toString(status.toString()) +"')");
    }

    @SuppressWarnings("all")
    public List<Kamar> findExistsByUser() {
        return getFindList("SELECT * FROM kamar k WHERE EXISTS (SELECT 1 FROM user u WHERE u.no_kamar=k.no_kamar AND u.status='AKTIF' AND u.hak_akses='"+ HakAksesConstant.ANGGOTA.name() +"')");
    }
    
    @SuppressWarnings("all")
    public List<Kamar> findByNoKamarIn(List<String> noKamarList) {
        return getFindList("SELECT * FROM kamar k where no_kamar IN('"+ ListUtil.toString(noKamarList.toString()) +"')");
    }
}
