/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.constant;

/**
 *
 * @author shinriokudo
 */
public enum ButtonConstant {
    BERANDA,
    DONATUR,
    KAMAR,
    TRANSAKSI_MASUK,
    TRANSAKSI_KELUAR,
    LAPORAN,
    LAPORAN_TUNGGAKAN,
    PENYEWA,
    BERANDA_PENYEWA,
    LAPORAN_PENYEWA;
}
