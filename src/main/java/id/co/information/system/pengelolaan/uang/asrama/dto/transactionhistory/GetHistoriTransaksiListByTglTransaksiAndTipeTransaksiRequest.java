package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import id.co.information.system.pengelolaan.uang.asrama.constant.TipeTransaksiConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetHistoriTransaksiListByTglTransaksiAndTipeTransaksiRequest implements Serializable {
    private static final long serialVersionUID = 7010887645036122531L;

    private Date fromTglTransaksi;
    private Date toTglTransaksi;
    private TipeTransaksiConstant tipeTransaksi;
}
