package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DueDateOverDueDateDto implements Serializable {
    private static final long serialVersionUID = 3684374375638508797L;

    private Date dueDate;
    private String fullName;
    private String noKamar;
    private Long billAmount;
}
