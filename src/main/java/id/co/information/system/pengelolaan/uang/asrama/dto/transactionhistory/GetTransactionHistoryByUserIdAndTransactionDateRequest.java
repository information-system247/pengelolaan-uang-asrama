package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetTransactionHistoryByUserIdAndTransactionDateRequest implements Serializable {
    private static final long serialVersionUID = -7762796278995748050L;

    private Long userId;
    private Date fromTransactionDate;
    private Date toTransactionDate;
}
