/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package id.co.information.system.pengelolaan.uang.asrama.util;

import id.co.information.system.pengelolaan.uang.asrama.constant.GenderConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.UserStatusConstant;
import javax.swing.JRadioButton;

/**
 *
 * @author wahyu
 */
public class RadioButtonUtil {
    
    public static void setGender(GenderConstant gender, JRadioButton jRadioButtonPria, JRadioButton jRadioButtonWanita) {
        if (GenderConstant.PRIA.equals(GenderConstant.valueOf(gender.name()))) {
            jRadioButtonPria.setSelected(true);
        } else {
            jRadioButtonWanita.setSelected(true);
        }
    }
    
    public static GenderConstant getGender(JRadioButton jRadioButtonPria, JRadioButton jRadioButtonWanita) {
        try {
            if (jRadioButtonPria.isSelected()) {
                return GenderConstant.PRIA;
            } else if (jRadioButtonWanita.isSelected()) {
                return GenderConstant.WANITA;
            } else {
                Logger.info("gender not selected", RadioButtonUtil.class);
            }
        } catch (Exception e) {
            Logger.error("Error get gender :", e, RadioButtonUtil.class);
        }
        return null;
    }
    
    public static void setStatusUser(UserStatusConstant status, JRadioButton jRadioButtonAktif, JRadioButton jRadioButtonNonAktif) {
        if (UserStatusConstant.AKTIF.equals(UserStatusConstant.valueOf(status.name()))) {
            jRadioButtonAktif.setSelected(true);
        } else {
            jRadioButtonNonAktif.setSelected(true);
        }
    }
    
    public static UserStatusConstant getStatusUser(JRadioButton jRadioButtonAktif, JRadioButton jRadioButtonNonAktif) {
        try {
            if (jRadioButtonAktif.isSelected()) {
                return UserStatusConstant.AKTIF;
            } else if (jRadioButtonNonAktif.isSelected()) {
                return UserStatusConstant.TIDAK_AKTIF;
            } else {
                Logger.error("gender not selected", RadioButtonUtil.class);
            }
        } catch (Exception e) {
            Logger.error("Error get gender : ", e, RadioButtonUtil.class);
        }
        return null;
    }
}
