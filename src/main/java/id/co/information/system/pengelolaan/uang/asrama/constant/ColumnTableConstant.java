/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.constant;

/**
 *
 * @author shinriokudo
 */
public class ColumnTableConstant {
    private static final String NO = "No.";
    private static final String NAMA_LENGKAP = "Nama Lengkap";
    private static final String TANGGAL_JATUH_TEMPO = "Tanggal Jatuh Tempo";
    private static final String NO_KAMAR = "Nama / No. Kamar";
    private static final String TANGGAL_TRANSAKSI = "Tanggal Transaksi";
    private static final String KETERANGAN = "Keterangan";
    private static final String TOTAL = "Total";
    
    protected static final String[] DAFTAR_JATUH_TEMPO_BERANDA_ADMIN = {NO, TANGGAL_JATUH_TEMPO, NAMA_LENGKAP, NO_KAMAR, "Jumlah Tagihan"};
    protected static final String[] DAFTAR_KETERLAMBATAN_BERANDA_ADMIN = {NO, TANGGAL_JATUH_TEMPO, NAMA_LENGKAP, NO_KAMAR, "Jumlah Tagihan"};
    protected static final String[] DAFTAR_KAMAR_KOSONG_BERANDA_ADMIN = {NO, NO_KAMAR, "Kapasitas (Orang)", "Terisi (Orang)", "Tersedia (Orang)"};
    protected static final String[] DAFTAR_KAMAR = {NO, NO_KAMAR, "Kapasitas", "Status", "Tersedia (Orang)"};
    protected static final String[] DAFTAR_PENYEWA = {NO, "NIK", NAMA_LENGKAP, "Gender", "Alamat", "No. Hp", "Nama Pengguna", "Kata Sandi", "Tanggal Masuk", "Aktif", NO_KAMAR, "Jumlah Tagihan / Bulan"};
    protected static final String[] DAFTAR_DONATUR = {NO, "NIK", NAMA_LENGKAP, "Gender", "Alamat", "No. Hp"};
    protected static final String[] DAFTAR_TRANSAKSI_MASUK_FROM_PENYEWA = {NO, TANGGAL_TRANSAKSI, NAMA_LENGKAP, NO_KAMAR, "Jangka Waktu (Bulan)", TOTAL, KETERANGAN, TANGGAL_JATUH_TEMPO};
    protected static final String[] DAFTAR_TRANSAKSI_MASUK_FROM_DONATUR = {NO, TANGGAL_TRANSAKSI, NAMA_LENGKAP, "Jumlah uang Masuk", KETERANGAN};
    protected static final String[] DAFTAR_TRANSAKSI_MASUK_FROM_LAINNYA = {NO, TANGGAL_TRANSAKSI, "Dari", NAMA_LENGKAP, "Jumlah Uang Masuk", KETERANGAN};
    protected static final String[] DAFTAR_TRANSAKSI_KELUAR = {NO, TANGGAL_TRANSAKSI, "Jumlah Uang Keluar", KETERANGAN};
    protected static final String[] DAFTAR_LAPORAN_TRANSAKSI_SEMUA = {NO, TANGGAL_TRANSAKSI, "Jenis Transaksi", NAMA_LENGKAP, TOTAL, KETERANGAN};
    protected static final String[] DAFTAR_LAPORAN_TRANSAKSI_MASUK = {NO, TANGGAL_TRANSAKSI, "Dari", NAMA_LENGKAP, NO_KAMAR, "Jangka Waktu (Bulan)", TANGGAL_JATUH_TEMPO, TOTAL, KETERANGAN};
    protected static final String[] DAFTAR_LAPORAN_TRANSAKSI_KELUAR = {NO, TANGGAL_TRANSAKSI, TOTAL, KETERANGAN};
    protected static final String[] DAFTAR_LAPORAN_TRANSAKSI_PENYEWA = {NO, TANGGAL_TRANSAKSI, "jangka Waktu (bulan)", TANGGAL_JATUH_TEMPO, "Subtotal"};
}
