package id.co.information.system.pengelolaan.uang.asrama.dto.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUserByUsernameAndPasswordRequest implements Serializable {
    private static final long serialVersionUID = -509211761321695647L;

    private String username;
    private String password;
}
