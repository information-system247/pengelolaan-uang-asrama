package id.co.information.system.pengelolaan.uang.asrama.controller;

import id.co.information.system.pengelolaan.uang.asrama.constant.MessageCodeConstant;
import id.co.information.system.pengelolaan.uang.asrama.dto.Response;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;

import static id.co.information.system.pengelolaan.uang.asrama.constant.InfoConstant.RESPONSE;

public class BaseController {

    protected void displayResponse (Response response) {
        try {
            if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
                Logger.info(RESPONSE, response.getResponseDto(), BaseController.class);
            } else {
                Logger.warn(RESPONSE, response.getResponseDto(), BaseController.class);
            }
        } catch (Exception e) {
            Logger.error("Error display response : ", e, BaseController.class);
        }
    }
}
