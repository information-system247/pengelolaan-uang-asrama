package id.co.information.system.pengelolaan.uang.asrama.dto.user;

import id.co.information.system.pengelolaan.uang.asrama.constant.GenderConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.HakAksesConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.UserStatusConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUserDto implements Serializable {
    private static final long serialVersionUID = 3375752902589171992L;

    private String nik;
    private String namaLengkap;
    private GenderConstant gender;
    private String alamat;
    private String noHp;
    private String namaPengguna;
    private String kataSandi;
    private Date tglMasuk;
    private UserStatusConstant userStatus;
    private String noKamar;
    private Long bilAmount;
    private HakAksesConstant hakAkses;
}
