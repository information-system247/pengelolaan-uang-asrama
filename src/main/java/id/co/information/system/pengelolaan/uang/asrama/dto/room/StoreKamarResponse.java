package id.co.information.system.pengelolaan.uang.asrama.dto.room;

import id.co.information.system.pengelolaan.uang.asrama.model.Kamar;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreKamarResponse implements Serializable {
    private static final long serialVersionUID = -2197110083796681281L;

    private Kamar kamar;
}
