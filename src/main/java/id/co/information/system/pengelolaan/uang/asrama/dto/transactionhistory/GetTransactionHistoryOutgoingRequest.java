/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author wahyu
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetTransactionHistoryOutgoingRequest implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String keyword;
}
