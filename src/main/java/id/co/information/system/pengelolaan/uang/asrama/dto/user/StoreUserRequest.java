package id.co.information.system.pengelolaan.uang.asrama.dto.user;

import id.co.information.system.pengelolaan.uang.asrama.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreUserRequest implements Serializable {
    private static final long serialVersionUID = 504092039968670363L;

    private User user;
}
