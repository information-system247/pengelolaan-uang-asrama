package id.co.information.system.pengelolaan.uang.asrama.dto.room;

import id.co.information.system.pengelolaan.uang.asrama.model.Kamar;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetKamarByNoKamarResponse implements Serializable {
    private static final long serialVersionUID = -4079959540324088525L;

    private Kamar kamar;
}
