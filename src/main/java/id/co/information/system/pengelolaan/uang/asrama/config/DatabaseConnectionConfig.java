package id.co.information.system.pengelolaan.uang.asrama.config;

import id.co.information.system.pengelolaan.uang.asrama.util.Logger;
import lombok.SneakyThrows;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import static id.co.information.system.pengelolaan.uang.asrama.constant.DatabaseConstant.*;

public class DatabaseConnectionConfig {

    @SneakyThrows
    public static Connection get() {
        Properties info = new Properties();
        info.put("user", USERNAME);
        info.put("password", PASSWORD);
        try {
            Connection con = DriverManager.getConnection(URL, info);
//            Logger.info("Connection success", DatabaseConnectionConfig.class);
            return con;
        } catch (SQLException ex) {
            Logger.error("Connection failed : ", ex, DatabaseConnectionConfig.class);
            throw ex;
        }
    }
}
