package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import id.co.information.system.pengelolaan.uang.asrama.constant.FromTransactionConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HistoriTransaksiMasukDto implements Serializable {
    private static final long serialVersionUID = 530544376539119864L;

    private Date tglTransaksi;
    private String namaTransaksi;
    private String fullName;
    private String noKamar;
    private Integer jangkaWaktu;
    private Date jatuhTempo;
    private Long total;
    private String keterangan;
    private FromTransactionConstant fromTransaction;
}
