package id.co.information.system.pengelolaan.uang.asrama.controller;

import id.co.information.system.pengelolaan.uang.asrama.constant.MessageCodeConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.TipeTransaksiConstant;
import id.co.information.system.pengelolaan.uang.asrama.dto.Response;
import id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory.*;
import id.co.information.system.pengelolaan.uang.asrama.model.HistoriTransaksi;
import id.co.information.system.pengelolaan.uang.asrama.service.TransaksiService;
import id.co.information.system.pengelolaan.uang.asrama.util.DateUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import java.util.Date;

public class HistoriTransaksiController extends BaseController{

    private final TransaksiService transaksiService = new TransaksiService();

    public Response getHistoriTransaksiToday() {
        try {
            Date tgl = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
            Response getHistoriTransaksiByTglTransaksiResponse = transaksiService.getHistoriTransaksiByTglTransaksi(GetHistoriTransaksiByTglTransaksiRequest.builder().tglTransaksi(tgl).build());
            displayResponse(getHistoriTransaksiByTglTransaksiResponse);
            return getHistoriTransaksiByTglTransaksiResponse;
        } catch (Exception e) {
            Logger.error("Error response transaction history today  : ", e, HistoriTransaksiController.class);
            throw e;
        }
    }

    public Long amountOfMoneyInToday() {
        try {
            Response response = getHistoriTransaksiToday();
            if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
                GetHistoriTransaksiByTglTransaksiResponse getHistoriTransaksiByTglTransaksiResponse = (GetHistoriTransaksiByTglTransaksiResponse) response.getResponseDto().getData();
                return getHistoriTransaksiByTglTransaksiResponse.getHistoriTransaksiList().stream()
                        .filter(historiTransaksi -> historiTransaksi.getTipeTransaksi().equals(TipeTransaksiConstant.MASUK))
                        .mapToLong(HistoriTransaksi::getTotal).sum();
            }
            return 0L;
        } catch (Exception e) {
            Logger.error("Error response amount of money in today : ", e, HistoriTransaksiController.class);
            throw e;
        }
    }

    public Long amountOfMoneyOutToday() {
        try {
            Response response = getHistoriTransaksiToday();
            if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
                GetHistoriTransaksiByTglTransaksiResponse getHistoriTransaksiByTglTransaksiResponse = (GetHistoriTransaksiByTglTransaksiResponse) response.getResponseDto().getData();
                return getHistoriTransaksiByTglTransaksiResponse.getHistoriTransaksiList().stream()
                        .filter(historiTransaksi -> historiTransaksi.getTipeTransaksi().equals(TipeTransaksiConstant.KELUAR))
                        .mapToLong(HistoriTransaksi::getTotal).sum();
            }
            return 0L;
        } catch (Exception e) {
            Logger.error("Error response amount of money out today : ", e, HistoriTransaksiController.class);
            throw e;
        }
    }

    public Long getSisaSaldo() {
        try {
            Response getSisaSaldoResponse = transaksiService.getSisaSaldo();
            displayResponse(getSisaSaldoResponse);
            GetSisaSaldoResponse response = (GetSisaSaldoResponse) getSisaSaldoResponse.getResponseDto().getData();
            return response.getHistoriTransaksi().getSisaSaldo();
        } catch (Exception e) {
            Logger.error("Error response the remaining balance : ", e, HistoriTransaksiController.class);
            throw e;
        }
    }

    public Response getDueDateOverDueDate(GetDueDateOverDueDateRequest request) {
        try {
            Response getDueDateOverDueDateResponse = transaksiService.getDueDateOrLateDate(request);
            displayResponse(getDueDateOverDueDateResponse);
            return getDueDateOverDueDateResponse;
        } catch (Exception e) {
            Logger.error("Error response due or late : ", e, HistoriTransaksiController.class);
            throw e;
        }
    }

    public Response incomingTransactionHistoryList(GetHistoriTransaksiMasukListByFromTransaksiRequest request) {
        try {
            Response getHistoriTransaksiMasukListByFromTransaksiResponse = transaksiService.getHistoriTransaksiMasukListByFromTransaksi(request);
            displayResponse(getHistoriTransaksiMasukListByFromTransaksiResponse);
            return getHistoriTransaksiMasukListByFromTransaksiResponse;
        } catch (Exception e) {
            Logger.error("Error response incoming transaction history list : ", e, HistoriTransaksiController.class);
            throw e;
        }
    }

    public Response outgoingTransactionHistoryList(GetTransactionHistoryOutgoingRequest request) {
        try {
            Response getHistoriTransaksiKeluarListResponse = transaksiService.getHistoriTransaksiKeluarList(request);
            displayResponse(getHistoriTransaksiKeluarListResponse);
            return getHistoriTransaksiKeluarListResponse;
        } catch (Exception e) {
            Logger.error("Error response outgoing transaction history list : ", e, HistoriTransaksiController.class);
            throw e;
        }
    }

    public Response getHistoriTransaksiList(GetHistoriTransaksiListByTglTransaksiAndTipeTransaksiRequest request) {
        try {
            Response getHistoriTransaksiListResponse = transaksiService.getTransactionHistoryList(request);
            displayResponse(getHistoriTransaksiListResponse);
            return getHistoriTransaksiListResponse;
        } catch (Exception e) {
            Logger.error("Error response transaction history list : ", e, HistoriTransaksiController.class);
            throw e;
        }
    }

    public Response storeTransactionHistory(StoreHistoriTransaksiRequest request) {
        try {
            Response response = transaksiService.storeHistoriTransaksi(request);
            displayResponse(response);
            return response;
        } catch (Exception e) {
            Logger.error("Error response store transaction history : ", e, HistoriTransaksiController.class);
            throw e;
        }
    }

    public Response getTransactionHistoryByUserId(GetTransactionHistoryByUserIdAndTransactionDateRequest request) {
        try {
            Response response = transaksiService.getTransactionHistoryByUserIdAndTransactionDate(request);
            displayResponse(response);
            return response;
        } catch (Exception e) {
            Logger.error("Error get transaction history by user id : ", e, HistoriTransaksiController.class);
            throw e;
        }
    }
}
