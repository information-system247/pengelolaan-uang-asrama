/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package id.co.information.system.pengelolaan.uang.asrama.util;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import id.co.information.system.pengelolaan.uang.asrama.constant.AsramaConstant;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import javax.swing.JTable;

/**
 *
 * @author wahyu
 */
public class ExportFileUtil {
    
    public static Boolean toPDF(JTable table, String namaFile) {
        try {
            int totalData = table.getRowCount();
            if (totalData > 0) {
                Document document = new Document(PageSize.A4.rotate());
                PdfWriter.getInstance(document, new FileOutputStream(AsramaConstant.PATH_DOWNLOAD+ namaFile + ".pdf"));
                document.open();
                PdfPTable pdfPTable = new PdfPTable(table.getColumnCount());
                for (int i = 0; i < table.getColumnCount(); i++) {
                    pdfPTable.addCell(table.getColumnName(i));
                }
                
                for (int i = 0; i < totalData; i++) {
                    for (int j = 0; j < table.getColumnCount(); j++) {
                        Object data = table.getModel().getValueAt(i, j);
                        pdfPTable.addCell(data.toString());
                    }
                }
                document.add(pdfPTable);
                document.close();
                return Boolean.TRUE;
            }
        } catch (DocumentException | FileNotFoundException e) {
            Logger.error("Error export data to pdf file : {}", e, ExportFileUtil.class);
            return Boolean.FALSE;
        }
        return Boolean.FALSE;
    }
}
