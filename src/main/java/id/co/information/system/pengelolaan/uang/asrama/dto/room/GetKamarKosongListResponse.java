package id.co.information.system.pengelolaan.uang.asrama.dto.room;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetKamarKosongListResponse implements Serializable {
    private static final long serialVersionUID = 8242263472571435635L;

    private List<GetKamarKosongDto> kamarList;
}
