/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.constant;

/**
 *
 * @author shinriokudo
 */
public enum EncryptConstant {
    
    ALGORITHM("AES"),
    SECRET_KEY("46b2a506e1d43f350a59303afbaaadd7");
    
    private final String encrypt;
    
    private EncryptConstant(String encrypt) {
        this.encrypt = encrypt;
    }
    
    public String getKey() {
        return this.encrypt;
    }
}
