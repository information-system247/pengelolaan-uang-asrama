package id.co.information.system.pengelolaan.uang.asrama.service;

import id.co.information.system.pengelolaan.uang.asrama.constant.FromTransactionConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.HakAksesConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.TipeTransaksiConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.UserStatusConstant;
import id.co.information.system.pengelolaan.uang.asrama.dto.Response;
import id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory.*;
import id.co.information.system.pengelolaan.uang.asrama.model.Donatur;
import id.co.information.system.pengelolaan.uang.asrama.model.HistoriTransaksi;
import id.co.information.system.pengelolaan.uang.asrama.model.User;
import id.co.information.system.pengelolaan.uang.asrama.repository.DonaturRepository;
import id.co.information.system.pengelolaan.uang.asrama.repository.HistoriTransaksiRepository;
import id.co.information.system.pengelolaan.uang.asrama.repository.UserRepository;
import id.co.information.system.pengelolaan.uang.asrama.util.DateUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;
import id.co.information.system.pengelolaan.uang.asrama.util.ResponseUtil;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static id.co.information.system.pengelolaan.uang.asrama.constant.InfoConstant.REQUEST;
import static id.co.information.system.pengelolaan.uang.asrama.constant.MessageCodeConstant.*;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class TransaksiService {

    private final HistoriTransaksiRepository historiTransaksiRepository = new HistoriTransaksiRepository();
    private final UserRepository userRepository = new UserRepository();
    private final DonaturRepository donaturRepository = new DonaturRepository();

    public String generateTransaksiId() {
        String code = "TRX";
        int serialNumber;
        Optional<HistoriTransaksi> historyTransaksiOptional = historiTransaksiRepository.findByTransaksiIdContainingIgnoreCaseOrderByTransaksiIdLimit1(DateUtil.toString(new Date(), DateUtil.YYYYMMDD));
        if (historyTransaksiOptional.isEmpty()) {
            return code+ DateUtil.toString(new Date(), DateUtil.YYYYMMDD)+"0001";
        } else {
            serialNumber = Integer.parseInt(historyTransaksiOptional.get().getTransaksiId().substring(historyTransaksiOptional.get().getTransaksiId().length() - 4));
            serialNumber++;
            if (serialNumber < 10) {
                return code+ DateUtil.toString(new Date(), DateUtil.YYYYMMDD)+"000"+ serialNumber;
            } else if (serialNumber < 100) {
                return code+ DateUtil.toString(new Date(), DateUtil.YYYYMMDD)+"00"+ serialNumber;
            } else if (serialNumber < 1000) {
                return code+ DateUtil.toString(new Date(), DateUtil.YYYYMMDD)+"0"+ serialNumber;
            } else {
                return code+ DateUtil.toString(new Date(), DateUtil.YYYYMMDD)+ serialNumber;
            }
        }
    }

    public Response storeHistoriTransaksi(StoreHistoriTransaksiRequest request) {
        String bashInfo = "start store incoming transaction history";
        try {
            switch (request.getHistoriTransaksi().getTipeTransaksi()) {
                case MASUK:
                    switch (request.getHistoriTransaksi().getFromTransaction()) {
                        case DONATUR:
                            Logger.info(bashInfo+" by donatur", this.getClass());
                            Logger.info(REQUEST, request, this.getClass());
                            return storeHistoriTransaksiByDonatur(request);

                        case ANGGOTA:
                            Logger.info(bashInfo+" by renter", this.getClass());
                            Logger.info(REQUEST, request, this.getClass());
                            return storeHistoriTransaksiByPenyewa(request);

                        case LAINNYA:
                            Logger.info(bashInfo+" by other", this.getClass());
                            Logger.info(REQUEST, request, this.getClass());
                            return storeHistoriTransaksiByLainnya(request);
                        default:
                            return ResponseUtil.buildPendingResponse(BAD_REQUEST);
                    }

                case KELUAR:
                    Logger.info("start store outgoing transaction history", this.getClass());
                    Logger.info(REQUEST, request, this.getClass());
                    return storeHistoriTransaksiKeluar(request);
                default:
                    return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }
        } catch (Exception e) {
            String tipeTransaksi;
            if (request.getHistoriTransaksi().getTipeTransaksi().equals(TipeTransaksiConstant.MASUK)) {
                tipeTransaksi = "incoming";
            } else {
                tipeTransaksi = "outgoing";
            }
            Logger.error("Error store "+ tipeTransaksi +" transaction history : ", e, this.getClass());
//            return ResponseUtil.buildFailedResponse(e.getMessage());
            throw e;
        }
    }

    private Response storeHistoriTransaksiByDonatur(StoreHistoriTransaksiRequest request) {
        try {
            if (request.getHistoriTransaksi().getDonaturId() == null || request.getHistoriTransaksi().getTglTransaksi() == null ||
                request.getHistoriTransaksi().getTotal() == null || request.getHistoriTransaksi().getKeterangan() == null || request.getHistoriTransaksi().getKeterangan().isEmpty()) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Optional<HistoriTransaksi> historiTransaksiOptional = historiTransaksiRepository.findAllOrderByIdDescLimit1();
            Long sisaSaldo = 0L;
            if (historiTransaksiOptional.isPresent()) {
                sisaSaldo = historiTransaksiOptional.get().getSisaSaldo();
            }

            Optional<User> userOptional = userRepository.findByHakAkses(HakAksesConstant.BENDAHARA.name());
            if (userOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            HistoriTransaksi historiTransaksi = HistoriTransaksi.builder()
                    .transaksiId(generateTransaksiId())
                    .tglTransaksi(request.getHistoriTransaksi().getTglTransaksi())
                    .tipeTransaksi(TipeTransaksiConstant.MASUK)
                    .fromTransaction(FromTransactionConstant.DONATUR)
                    .donaturId(request.getHistoriTransaksi().getDonaturId())
                    .keterangan(request.getHistoriTransaksi().getKeterangan())
                    .total(request.getHistoriTransaksi().getTotal())
                    .sisaSaldo(sisaSaldo + request.getHistoriTransaksi().getTotal())
                    .createAt(userOptional.get().getNamaLengkap())
                    .userId(0L)
                    .jangkaWaktu(0)
                    .monthlyPayment(0L)
                    .build();

            Boolean storeHistoriTransaksi = historiTransaksiRepository.save(historiTransaksi);
            if (Boolean.FALSE.equals(storeHistoriTransaksi)) {
                return ResponseUtil.buildPendingResponse(SQL_ERROR);
            }

            Optional<HistoriTransaksi> historiTransaksiOptional1 = historiTransaksiRepository.findByTransaksiId(historiTransaksi.getTransaksiId());
            return ResponseUtil.buildSuccessResponse(StoreHistoriTransaksiResponse.builder()
                    .historiTransaksi(historiTransaksiOptional1.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error store incoming transaction history by donatur : ", e, this.getClass());
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    private Response storeHistoriTransaksiByPenyewa(StoreHistoriTransaksiRequest request) {
        try {
            if (request.getHistoriTransaksi().getTglTransaksi() == null || request.getHistoriTransaksi().getUserId() == null ||
                request.getHistoriTransaksi().getJangkaWaktu() == null  || request.getHistoriTransaksi().getKeterangan() == null || request.getHistoriTransaksi().getKeterangan().isEmpty()) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Date payStartDate;
            Optional<HistoriTransaksi> historiTransaksiOptional = historiTransaksiRepository.findFirstByUserIdOrderByTglTransaksiDesc(request.getHistoriTransaksi().getUserId());
            if (historiTransaksiOptional.isPresent()) {
                payStartDate = historiTransaksiOptional.get().getJatuhTempo();
            } else {
                Optional<User> userOptional = userRepository.findById(request.getHistoriTransaksi().getUserId());
                if (userOptional.isEmpty()) {
                    return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
                }
                payStartDate = userOptional.get().getTglMasuk();
            }

            Long sisaSaldo = 0L;
            Optional<HistoriTransaksi> historiTransaksiOptional1 = historiTransaksiRepository.findAllOrderByIdDescLimit1();
            if (historiTransaksiOptional1.isPresent()) {
                sisaSaldo = historiTransaksiOptional1.get().getSisaSaldo();
            }

            Optional<User> userOptional = userRepository.findByHakAkses(HakAksesConstant.BENDAHARA.name());
            if (userOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            Date jatuhTempo = DateUtil.addMonth(payStartDate, request.getHistoriTransaksi().getJangkaWaktu());
            String transaksiId = generateTransaksiId();
            Long total = request.getHistoriTransaksi().getTotal();
            HistoriTransaksi historiTransaksi = HistoriTransaksi.builder()
                    .transaksiId(transaksiId)
                    .tglTransaksi(request.getHistoriTransaksi().getTglTransaksi())
                    .tipeTransaksi(TipeTransaksiConstant.MASUK)
                    .fromTransaction(FromTransactionConstant.ANGGOTA)
                    .userId(request.getHistoriTransaksi().getUserId())
                    .jangkaWaktu(request.getHistoriTransaksi().getJangkaWaktu())
                    .monthlyPayment(request.getHistoriTransaksi().getMonthlyPayment())
                    .jatuhTempo(jatuhTempo)
                    .donaturId(0L)
                    .keterangan(request.getHistoriTransaksi().getKeterangan())
                    .total(total)
                    .sisaSaldo(sisaSaldo + total)
                    .createAt(userOptional.get().getNamaLengkap())
                    .build();
            Boolean storeHistoriTransaksi = historiTransaksiRepository.save(historiTransaksi);
            if (Boolean.FALSE.equals(storeHistoriTransaksi)) {
                return ResponseUtil.buildPendingResponse(SQL_ERROR);
            }

            Optional<HistoriTransaksi> historiTransaksiOptional2 = historiTransaksiRepository.findByTransaksiId(transaksiId);
            return ResponseUtil.buildSuccessResponse(StoreHistoriTransaksiResponse.builder()
                    .historiTransaksi(historiTransaksiOptional2.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error store incoming transaction history by renter : ", e, this.getClass());
//            return ResponseUtil.buildFailedResponse(e.getMessage());
            throw e;
        }
    }

    private Response storeHistoriTransaksiByLainnya(StoreHistoriTransaksiRequest request) {
        try {
            if (request.getHistoriTransaksi().getTglTransaksi() == null || 
                    request.getHistoriTransaksi().getNamaTransaksi() == null || request.getHistoriTransaksi().getNamaTransaksi().isEmpty() ||
                    request.getHistoriTransaksi().getNamaLengkap() == null || request.getHistoriTransaksi().getNamaLengkap().isEmpty() || request.getHistoriTransaksi().getTotal() == null ||
                request.getHistoriTransaksi().getKeterangan() == null || request.getHistoriTransaksi().getKeterangan().isEmpty()) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Long sisaSaldo = 0L;
            Optional<HistoriTransaksi> historiTransaksiOptional1 = historiTransaksiRepository.findAllOrderByIdDescLimit1();
            if (historiTransaksiOptional1.isPresent()) {
                sisaSaldo = historiTransaksiOptional1.get().getSisaSaldo();
            }

            Optional<User> userOptional = userRepository.findByHakAkses(HakAksesConstant.BENDAHARA.name());
            if (userOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            String transaksiId = generateTransaksiId();
            HistoriTransaksi historiTransaksi = HistoriTransaksi.builder()
                    .transaksiId(transaksiId)
                    .tglTransaksi(request.getHistoriTransaksi().getTglTransaksi())
                    .fromTransaction(FromTransactionConstant.LAINNYA)
                    .tipeTransaksi(TipeTransaksiConstant.MASUK)
                    .namaTransaksi(request.getHistoriTransaksi().getNamaTransaksi())
                    .namaLengkap(request.getHistoriTransaksi().getNamaLengkap())
                    .total(request.getHistoriTransaksi().getTotal())
                    .sisaSaldo(sisaSaldo + request.getHistoriTransaksi().getTotal())
                    .keterangan(request.getHistoriTransaksi().getKeterangan())
                    .userId(0L)
                    .jangkaWaktu(0)
                    .donaturId(0L)
                    .monthlyPayment(0L)
                    .createAt(userOptional.get().getNamaLengkap())
                    .build();
            Boolean storeHistoriTransaksi = historiTransaksiRepository.save(historiTransaksi);
            if (Boolean.FALSE.equals(storeHistoriTransaksi)) {
                return ResponseUtil.buildPendingResponse(SQL_ERROR);
            }

            Optional<HistoriTransaksi> historiTransaksiOptional = historiTransaksiRepository.findByTransaksiId(transaksiId);
            return ResponseUtil.buildSuccessResponse(StoreHistoriTransaksiResponse.builder()
                    .historiTransaksi(historiTransaksiOptional.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error store incoming transaction history by other : ", e, this.getClass());
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    private Response storeHistoriTransaksiKeluar(StoreHistoriTransaksiRequest request) {
        try {
            if (request.getHistoriTransaksi().getTglTransaksi() == null || 
                    request.getHistoriTransaksi().getKeterangan() == null || request.getHistoriTransaksi().getKeterangan().isEmpty() ||
                    request.getHistoriTransaksi().getTotal() == null) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Long sisaSaldo = 0L;
            Optional<HistoriTransaksi> historiTransaksiOptional1 = historiTransaksiRepository.findAllOrderByIdDescLimit1();
            if (historiTransaksiOptional1.isPresent()) {
                sisaSaldo = historiTransaksiOptional1.get().getSisaSaldo();
            }

            Optional<User> userOptional = userRepository.findByHakAkses(HakAksesConstant.BENDAHARA.name());
            if (userOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            String transaksiId = generateTransaksiId();
            HistoriTransaksi historiTransaksi = HistoriTransaksi.builder()
                    .tglTransaksi(request.getHistoriTransaksi().getTglTransaksi())
                    .transaksiId(transaksiId)
                    .tipeTransaksi(TipeTransaksiConstant.KELUAR)
                    .userId(0L)
                    .jangkaWaktu(0)
                    .monthlyPayment(0L)
                    .donaturId(0L)
                    .total(request.getHistoriTransaksi().getTotal())
                    .keterangan(request.getHistoriTransaksi().getKeterangan())
                    .sisaSaldo(sisaSaldo - request.getHistoriTransaksi().getTotal())
                    .createAt(userOptional.get().getNamaLengkap())
                    .build();
            Boolean storeHistoriTransaksi = historiTransaksiRepository.save(historiTransaksi);
            if (Boolean.FALSE.equals(storeHistoriTransaksi)) {
                return ResponseUtil.buildPendingResponse(SQL_ERROR);
            }

            Optional<HistoriTransaksi> historiTransaksiOptional = historiTransaksiRepository.findByTransaksiId(transaksiId);
            return ResponseUtil.buildSuccessResponse(StoreHistoriTransaksiResponse.builder()
                    .historiTransaksi(historiTransaksiOptional.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error store outgoing transaction history : ", e, this.getClass());
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response getHistoriTransaksiByTglTransaksi(GetHistoriTransaksiByTglTransaksiRequest request) {
        Logger.info("start getHistoriTransaksiByTglTransaksi", this.getClass());
        Logger.info(REQUEST, request, this.getClass());
        try {
            if (request.getTglTransaksi() == null) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            LocalDate localDate = request.getTglTransaksi().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            List<HistoriTransaksi> historiTransaksiList = historiTransaksiRepository.findByTglTransaksiContainingIgnoreCaseOrderByTglTransaksiDesc(localDate.format(DateTimeFormatter.ISO_DATE));
            if (historiTransaksiList.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            return ResponseUtil.buildSuccessResponse(GetHistoriTransaksiByTglTransaksiResponse.builder()
                    .historiTransaksiList(historiTransaksiList)
                    .build());
        } catch (Exception e) {
            Logger.error("Error get transaction history by transaction date : ", e, this.getClass());
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response getSisaSaldo() {
        Logger.info("start getSisaSaldo", this.getClass());
        try {
            Optional<HistoriTransaksi> historiTransaksiOptional = historiTransaksiRepository.findAllOrderByIdDescLimit1();
            if (historiTransaksiOptional.isEmpty()) {
                GetSisaSaldoResponse getSisaSaldoResponse = GetSisaSaldoResponse.builder()
                        .historiTransaksi(HistoriTransaksi.builder().sisaSaldo(0L).build())
                        .build();
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND,getSisaSaldoResponse );
            }

            return ResponseUtil.buildSuccessResponse(GetSisaSaldoResponse.builder()
                    .historiTransaksi(historiTransaksiOptional.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error get the remaining balance : ", e, this.getClass());
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    @SuppressWarnings("all")
    public Response getDueDateOrLateDate(GetDueDateOverDueDateRequest request) {
        Logger.info("start getDueDateOrOverDueDate", this.getClass());
        Logger.info(REQUEST, request, this.getClass());
        try {
            if (request.getIsDueDate() == null) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            List<User> userList = userRepository.findByHakAksesAndStatusOrderByNamaLengkapAsc(HakAksesConstant.ANGGOTA.name(), UserStatusConstant.AKTIF.name());
            if (userList.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            List<DueDateOverDueDateDto> dueDateOverDueDateDtoList = new ArrayList<>();
            userList.forEach( user -> {
                Date dateNow = DateUtil.toDateTime(DateUtil.toString(new Date(), DateUtil.YYYY_MM_DD), DateUtil.YYYY_MM_DD);
                Long datenowLong = dateNow.getTime();
                Optional<HistoriTransaksi> historiTransaksiOptional = historiTransaksiRepository.findFirstByUserIdOrderByJatuhTempoDesc(user.getId());
                if (historiTransaksiOptional.isEmpty()) {
                    Optional<User> userOptional = userRepository.findById(user.getId());
                    if (userOptional.isPresent()) {
                        Date tglMasuk = DateUtil.toDateTime(DateUtil.toString(userOptional.get().getTglMasuk(), DateUtil.YYYY_MM_DD), DateUtil.YYYY_MM_DD);
                        Date date = tglMasuk;
                        Long dateLong = date.getTime();
                        if (Boolean.TRUE.equals(request.getIsDueDate())) {
                            if (dateLong >= datenowLong) {
                                dueDateOverDueDateDtoList.add(DueDateOverDueDateDto.builder()
                                        .noKamar(userOptional.get().getKamar().getNoKamar())
                                        .fullName(userOptional.get().getNamaLengkap())
                                        .billAmount(userOptional.get().getMonthlyPayment())
                                        .dueDate(date)
                                        .build());
                            }
                        } else {
                            if (dateLong <= datenowLong) {
                                long monthsBetween = ChronoUnit.MONTHS.between(YearMonth.from(LocalDate.parse(DateUtil.toString(date, DateUtil.YYYY_MM_DD))), YearMonth.from(LocalDate.parse(DateUtil.toString(dateNow, DateUtil.YYYY_MM_DD))));
                                dueDateOverDueDateDtoList.add(DueDateOverDueDateDto.builder()
                                        .noKamar(userOptional.get().getKamar().getNoKamar())
                                        .fullName(userOptional.get().getNamaLengkap())
                                        .billAmount(userOptional.get().getMonthlyPayment() * monthsBetween)
                                        .dueDate(date)
                                        .build());
                            }
                        }
                    }
                } else {
                    Optional<User> userOptional = userRepository.findById(historiTransaksiOptional.get().getUserId());
                    Date date = historiTransaksiOptional.get().getJatuhTempo();
                    Long dueDateLong = date.getTime();
                    if (Boolean.TRUE.equals(request.getIsDueDate())) {
                        if (dueDateLong >= datenowLong && userOptional.isPresent()) {
                            dueDateOverDueDateDtoList.add(DueDateOverDueDateDto.builder()
                                    .noKamar(userOptional.get().getKamar().getNoKamar())
                                    .fullName(userOptional.get().getNamaLengkap())
                                    .billAmount(historiTransaksiOptional.get().getMonthlyPayment())
                                    .dueDate(date)
                                    .build());

                        }
                    } else {
                        if (dueDateLong <= datenowLong) {
                            long monthsBetween = ChronoUnit.MONTHS.between(YearMonth.from(LocalDate.parse(DateUtil.toString(date, DateUtil.YYYY_MM_DD))), YearMonth.from(LocalDate.parse(DateUtil.toString(dateNow, DateUtil.YYYY_MM_DD))));
                            dueDateOverDueDateDtoList.add(DueDateOverDueDateDto.builder()
                                    .noKamar(userOptional.get().getKamar().getNoKamar())
                                    .fullName(userOptional.get().getNamaLengkap())
                                    .billAmount(historiTransaksiOptional.get().getMonthlyPayment() * monthsBetween)
                                    .dueDate(date)
                                    .build());
                        }
                    }
                }
            });

            return ResponseUtil.buildSuccessResponse(GetDueDateOverDueDateResponse.builder()
                    .dueDateOverDueDateList(dueDateOverDueDateDtoList)
                    .build());
        } catch (Exception e) {
            Logger.error("Error get due date or over due date : ", e, this.getClass());
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response getHistoriTransaksiMasukListByFromTransaksi(GetHistoriTransaksiMasukListByFromTransaksiRequest request) {
        Logger.info("start getHistoriTransaksiMasukListByFromTransaksi", this.getClass());
        Logger.info(REQUEST, request, this.getClass());
        try {
            if (request.getFromTransaksi() == null) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            List<HistoriTransaksiByDonaturDto> historiTransaksiByDonaturList = new ArrayList<>();
            List<HistoriTransaksiByPenyewaDto> historiTransaksiByPenyewaList = new ArrayList<>();
            List<HistoriTransaksiByLainnyaDto> historiTransaksiByLainnyaList = new ArrayList<>();

            Optional<HistoriTransaksi> historiTransaksiOptional = historiTransaksiRepository.findAllOrderByIdDescLimit1();
            if (historiTransaksiOptional.isEmpty()) {
                
                return ResponseUtil.buildSuccessResponse(GetHistoriTransaksiMasukListByFromTransaksiResponse.builder()
                        .historiTransaksiByDonaturList(historiTransaksiByDonaturList)
                        .historiTransaksiByLainnyaList(historiTransaksiByLainnyaList)
                        .historiTransaksiByPenyewaList(historiTransaksiByPenyewaList)
                        .sisaSaldo(0L)
                        .total(0L)
                        .build());
            }

            List<HistoriTransaksi> historiTransaksiList = historiTransaksiRepository.findByFromTransaksiOrderByTglTransaksiDesc(request.getFromTransaksi().name());

            if (historiTransaksiList.isEmpty()) {
                return ResponseUtil.buildSuccessResponse(GetHistoriTransaksiMasukListByFromTransaksiResponse.builder()
                        .historiTransaksiByDonaturList(historiTransaksiByDonaturList)
                        .historiTransaksiByLainnyaList(historiTransaksiByLainnyaList)
                        .historiTransaksiByPenyewaList(historiTransaksiByPenyewaList)
                        .sisaSaldo(historiTransaksiOptional.get().getSisaSaldo())
                        .total(0L)
                        .build());
            }

            switch(request.getFromTransaksi()) {
                case DONATUR:
                    historiTransaksiList.forEach(historiTransaksi -> {
                        Optional<Donatur> donaturOptional = donaturRepository.findById(historiTransaksi.getDonaturId());
                        donaturOptional.ifPresent(donatur -> historiTransaksiByDonaturList.add(HistoriTransaksiByDonaturDto.builder()
                                .tglTransaksi(historiTransaksi.getTglTransaksi())
                                .fullName(donatur.getNamaLengkap())
                                .amount(historiTransaksi.getTotal())
                                .keterangan(historiTransaksi.getKeterangan())
                                .build()));
                    });
                    break;
                case ANGGOTA:
                    historiTransaksiList.forEach( historiTransaksi -> {
                        Optional<User> userOptional = userRepository.findById(historiTransaksi.getUserId());
                        userOptional.ifPresent(user -> historiTransaksiByPenyewaList.add(HistoriTransaksiByPenyewaDto.builder()
                                .noKamar(user.getKamar().getNoKamar())
                                .fullName(user.getNamaLengkap())
                                .tglTransaksi(historiTransaksi.getTglTransaksi())
                                .jangkaWaktu(historiTransaksi.getJangkaWaktu())
                                .jatuhTempo(historiTransaksi.getJatuhTempo())
                                .keterangan(historiTransaksi.getKeterangan())
                                .total(historiTransaksi.getTotal())
                                .build()));
                    });
                    break;
                case LAINNYA:
                    historiTransaksiList.forEach( historiTransaksi ->
                            historiTransaksiByLainnyaList.add(HistoriTransaksiByLainnyaDto.builder()
                                    .tglTransaksi(historiTransaksi.getTglTransaksi())
                                    .namaTranaksi(historiTransaksi.getNamaTransaksi())
                                    .fullName(historiTransaksi.getNamaLengkap())
                                    .amount(historiTransaksi.getTotal())
                                    .keterangan(historiTransaksi.getKeterangan())
                                    .build())
                    );
                    break;
                default:
                    return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            Long fullAmount = historiTransaksiList.stream().mapToLong(HistoriTransaksi::getTotal).sum();
            List<HistoriTransaksiByDonaturDto> searchHistoriTransaksiByDonaturList = new ArrayList<>();
            List<HistoriTransaksiByPenyewaDto> searchHistoriTransaksiByPenyewaList = new ArrayList<>();
            List<HistoriTransaksiByLainnyaDto> searchHistoriTransaksiByLainnyaList = new ArrayList<>();
            if (request.getKeyword() != null) {
                if (!historiTransaksiByDonaturList.isEmpty()) {
                    searchHistoriTransaksiByDonaturList = historiTransaksiByDonaturList.stream()
                            .filter(transactionHistory -> transactionHistory.getFullName().trim().contains(request.getKeyword()))
                            .collect(Collectors.toList());
                }
                
                if (!historiTransaksiByPenyewaList.isEmpty()) {
                    searchHistoriTransaksiByPenyewaList = historiTransaksiByPenyewaList.stream()
                                .filter(transactionHistory -> transactionHistory.getFullName().trim().contains(request.getKeyword()))
                                .collect(Collectors.toList());
                }
                
                if (!historiTransaksiByLainnyaList.isEmpty()) {
                    searchHistoriTransaksiByLainnyaList = historiTransaksiByLainnyaList.stream()
                            .filter(transactionHistory -> transactionHistory.getFullName().trim().contains(request.getKeyword()))
                            .collect(Collectors.toList());
                }
            }
            
            return ResponseUtil.buildSuccessResponse(GetHistoriTransaksiMasukListByFromTransaksiResponse.builder()
                    .historiTransaksiByDonaturList((request.getKeyword() == null || request.getKeyword().isEmpty()) ? historiTransaksiByDonaturList : searchHistoriTransaksiByDonaturList)
                    .historiTransaksiByLainnyaList((request.getKeyword() == null || request.getKeyword().isEmpty()) ? historiTransaksiByLainnyaList : searchHistoriTransaksiByLainnyaList)
                    .historiTransaksiByPenyewaList((request.getKeyword() == null || request.getKeyword().isEmpty()) ? historiTransaksiByPenyewaList : searchHistoriTransaksiByPenyewaList)
                    .sisaSaldo(historiTransaksiOptional.get().getSisaSaldo())
                    .total(fullAmount)
                    .build());
        } catch (Exception e) {
            Logger.error("Error get incoming transaction history list by from transaction : ", e, this.getClass());
//            return ResponseUtil.buildFailedResponse(e.getMessage());
            throw e;
        }
    }

    public Response getHistoriTransaksiKeluarList(GetTransactionHistoryOutgoingRequest request) {
        Logger.info("start getHistoriTransaksiKeluarList", this.getClass());
        Logger.info(REQUEST, request, this.getClass());
        try {
            Optional<HistoriTransaksi> historiTransaksiOptional = historiTransaksiRepository.findAllOrderByIdDescLimit1();
            if (historiTransaksiOptional.isEmpty()) {
                return ResponseUtil.buildSuccessResponse(GetHistoriTransaksiKeluarListResponse.builder()
                        .historiTransaksiKeluarList(new ArrayList<>())
                        .sisaSaldo(0L)
                        .total(0L)
                        .build());
            }

            List<HistoriTransaksi> historiTransaksiList = historiTransaksiRepository.findByTipeTransaksiOrderByTglTransaksiDesc(TipeTransaksiConstant.KELUAR.name());
            if (historiTransaksiList.isEmpty()) {
                return ResponseUtil.buildSuccessResponse(GetHistoriTransaksiKeluarListResponse.builder()
                        .historiTransaksiKeluarList(new ArrayList<>())
                        .sisaSaldo(historiTransaksiOptional.get().getSisaSaldo())
                        .total(0L)
                        .build());
            }

            List<HistoriTransaksiKeluarDto> historiTransaksiKeluarDtoList = new ArrayList<>();
            historiTransaksiList.forEach(historiTransaksi ->
                    historiTransaksiKeluarDtoList.add(HistoriTransaksiKeluarDto.builder()
                            .tglTransaksi(historiTransaksi.getTglTransaksi())
                            .amount(historiTransaksi.getTotal())
                            .keterangan(historiTransaksi.getKeterangan())
                            .build())
            );

            Long fullAmount = historiTransaksiList.stream().mapToLong(HistoriTransaksi::getTotal).sum();
            List<HistoriTransaksiKeluarDto> searchHistoriTransaksiKeluarDtoList = new ArrayList<>();
            if (request.getKeyword() != null) {
                searchHistoriTransaksiKeluarDtoList = historiTransaksiKeluarDtoList.stream()
                        .filter(transactoinHistory -> transactoinHistory.getKeterangan().trim().contains(request.getKeyword()))
                        .collect(Collectors.toList());
            }
            return ResponseUtil.buildSuccessResponse(GetHistoriTransaksiKeluarListResponse.builder()
                    .historiTransaksiKeluarList((request.getKeyword() == null) ? historiTransaksiKeluarDtoList : searchHistoriTransaksiKeluarDtoList)
                    .sisaSaldo(historiTransaksiOptional.get().getSisaSaldo())
                    .total(fullAmount)
                    .build());
        } catch (Exception e) {
            Logger.error("Error outgoing transaction history list : ", e, this.getClass());
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    @SuppressWarnings("all")
    public Response getTransactionHistoryList(GetHistoriTransaksiListByTglTransaksiAndTipeTransaksiRequest request) {
        Logger.info("start getTransactionHistoryList", this.getClass());
        Logger.info(REQUEST, request, this.getClass());
        try {
            List<HistoriTransaksiDto> historiTransaksiDtoList = new ArrayList<>();
            List<HistoriTransaksiKeluarDto> historiTransaksiKeluarDtoList = new ArrayList<>();
            List<HistoriTransaksiMasukDto> historiTransaksiMasukDtoList = new ArrayList<>();
            if ((request.getFromTglTransaksi() != null && request.getToTglTransaksi() == null) ||
            (request.getFromTglTransaksi() == null && request.getToTglTransaksi() != null) ||
            request.getTipeTransaksi() == null) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            List<HistoriTransaksi> historiTransaksiList;
            if (TipeTransaksiConstant.ALL.equals(request.getTipeTransaksi())) {
                if (request.getFromTglTransaksi() == null && request.getToTglTransaksi() == null) {
                    historiTransaksiList = historiTransaksiRepository.findAll();
                } else {
                    historiTransaksiList = historiTransaksiRepository
                            .findByTglTransaksiGreaterThenEqualAndTglTransaksiLessThenEqualOrderByTglTransaksiDesc(
                                    DateUtil.toString(request.getFromTglTransaksi(), DateUtil.YYYY_MM_DD), DateUtil.toString(request.getToTglTransaksi(), DateUtil.YYYY_MM_DD));
                }
            } else {
                if (request.getFromTglTransaksi() == null && request.getToTglTransaksi() == null) {
                    historiTransaksiList = historiTransaksiRepository.findByTipeTransaksiOrderByTglTransaksiDesc(request.getTipeTransaksi().name());
                } else {
                    historiTransaksiList = historiTransaksiRepository
                            .findByTipeTransaksiAndTglTransaksiGreaterThenEqualAndTglTransaksiLessThenEqualOrderByTglTransaksiDesc(
                                    request.getTipeTransaksi().name(), DateUtil.toString(request.getFromTglTransaksi(), DateUtil.YYYY_MM_DD), DateUtil.toString(request.getToTglTransaksi(), DateUtil.YYYY_MM_DD));
                }
            }

            if (historiTransaksiList.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            switch (request.getTipeTransaksi()){
                case MASUK:
                    historiTransaksiList.forEach( historiTransaksi -> {
                        switch(historiTransaksi.getFromTransaction()) {
                            case DONATUR:
                                Optional<Donatur> donaturOptional = donaturRepository.findById(historiTransaksi.getDonaturId());
                                if (donaturOptional.isPresent()) {
                                    historiTransaksiMasukDtoList.add(HistoriTransaksiMasukDto.builder()
                                            .tglTransaksi(historiTransaksi.getTglTransaksi())
                                            .fromTransaction(historiTransaksi.getFromTransaction())
                                            .fullName(donaturOptional.get().getNamaLengkap())
                                            .total(historiTransaksi.getTotal())
                                            .keterangan(historiTransaksi.getKeterangan())
                                            .build());
                                }
                                break;
                            case ANGGOTA:
                                Optional<User> userOptional = userRepository.findById(historiTransaksi.getUserId());
                                if (userOptional.isPresent()) {
                                    historiTransaksiMasukDtoList.add(HistoriTransaksiMasukDto.builder()
                                            .tglTransaksi(historiTransaksi.getTglTransaksi())
                                            .fullName(userOptional.get().getNamaLengkap())
                                            .noKamar(userOptional.get().getKamar().getNoKamar())
                                            .jangkaWaktu(historiTransaksi.getJangkaWaktu())
                                            .total(historiTransaksi.getTotal())
                                            .keterangan(historiTransaksi.getKeterangan())
                                            .jatuhTempo(historiTransaksi.getJatuhTempo())
                                            .fromTransaction(historiTransaksi.getFromTransaction())
                                            .build());
                                }
                                break;
                            case LAINNYA:
                                historiTransaksiMasukDtoList.add(HistoriTransaksiMasukDto.builder()
                                        .tglTransaksi(historiTransaksi.getTglTransaksi())
                                        .fromTransaction(historiTransaksi.getFromTransaction())
                                        .namaTransaksi(historiTransaksi.getNamaTransaksi())
                                        .fullName(historiTransaksi.getNamaLengkap())
                                        .total(historiTransaksi.getTotal())
                                        .keterangan(historiTransaksi.getKeterangan())
                                        .build());
                                break;
                            default:
                                Logger.warn("transaction form not found", this.getClass());
                                break;
                        }
                    });
                    break;
                case KELUAR:
                    historiTransaksiList.forEach( historiTransaksi ->
                            historiTransaksiKeluarDtoList.add(HistoriTransaksiKeluarDto.builder()
                                    .tglTransaksi(historiTransaksi.getTglTransaksi())
                                    .amount(historiTransaksi.getTotal())
                                    .keterangan(historiTransaksi.getKeterangan())
                                    .build())
                    );
                    break;
                case ALL:
                    historiTransaksiList.forEach( historiTransaksi -> {
                        HistoriTransaksiDto.HistoriTransaksiDtoBuilder historiTransaksiDto = HistoriTransaksiDto.builder();
                        historiTransaksiDto.tglTransaksi(historiTransaksi.getTglTransaksi());
                        historiTransaksiDto.tipeTransaksi(historiTransaksi.getTipeTransaksi());
                        historiTransaksiDto.total(historiTransaksi.getTotal());
                        historiTransaksiDto.keterangan(historiTransaksi.getKeterangan());
                        if (TipeTransaksiConstant.MASUK.equals(historiTransaksi.getTipeTransaksi())) {
                            switch(historiTransaksi.getFromTransaction()) {
                                case ANGGOTA:
                                    Optional<User> userOptional = userRepository.findById(historiTransaksi.getUserId());
                                    if (userOptional.isPresent()) {
                                        historiTransaksiDto.fullName(userOptional.get().getNamaLengkap());
                                    }
                                    break;
                                case DONATUR:
                                    Optional<Donatur> donaturOptional = donaturRepository.findById(historiTransaksi.getDonaturId());
                                    if (donaturOptional.isPresent()) {
                                        historiTransaksiDto.fullName(donaturOptional.get().getNamaLengkap());
                                    }
                                    break;
                                default:
                                    historiTransaksiDto.fullName(historiTransaksi.getNamaLengkap());
                                    break;
                            }
                        } else {
                            historiTransaksiDto.fullName("-");
                        }
                        historiTransaksiDtoList.add(historiTransaksiDto.build());
                    });
                    break;
                default:
                    Logger.warn("transaction type not found", this.getClass());
                    break;
            }

            return ResponseUtil.buildSuccessResponse(GetHistoriTransaksiListResponse.builder()
                    .historiTransaksiKeluarList(historiTransaksiKeluarDtoList)
                    .historiTransaksiList(historiTransaksiDtoList)
                    .historiTransaksiMasukList(historiTransaksiMasukDtoList)
                    .build());
        } catch (Exception e) {
            Logger.error("Error get transaction history list : ", e, this.getClass());
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response calculateMonthlyPayment(CalculateMonthlyPaymentRequest request) {
        Logger.info("start calculateMonthlyPayment", this.getClass());
        Logger.info(REQUEST, request, this.getClass());
        try {
            if (request.getNik() == null || request.getNik().isEmpty() || request.getJangkaWaktu() == null) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Optional<User> userOptional = userRepository.findByNik(request.getNik());
            if (userOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            return ResponseUtil.buildSuccessResponse(CalculateMonthlyPaymentResponse.builder()
                    .totalMonthlyPayment(userOptional.get().getMonthlyPayment() * request.getJangkaWaktu())
                    .build());
        } catch (Exception e) {
            Logger.error("Error calculate monthly payment : ", e, this.getClass());
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response getTransactionHistoryByUserIdAndTransactionDate(GetTransactionHistoryByUserIdAndTransactionDateRequest request) {
        Logger.info("start getTransactionHistoryByUserIdAndTransactionDate", this.getClass());
        Logger.info(REQUEST, request, this.getClass());
        try {
            if (request.getUserId() == null || (request.getFromTransactionDate() == null && request.getToTransactionDate() != null) ||
            (request.getFromTransactionDate() != null && request.getToTransactionDate() == null)) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            List<HistoriTransaksi> historiTransaksiList;
            if (request.getFromTransactionDate() == null && request.getToTransactionDate() == null) {
                historiTransaksiList = historiTransaksiRepository.findByUserIdOrderByTransactionDateDesc(request.getUserId());
            } else if (request.getFromTransactionDate().getTime() > request.getToTransactionDate().getTime()) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            } else {
                historiTransaksiList = historiTransaksiRepository.findByUserIdAndTransactionDateGreaterThenEqualAndTransactionDateLessThenEqualOrderByTransactionDateDesc(request.getUserId(), request.getFromTransactionDate(), request.getToTransactionDate());
            }

            if (historiTransaksiList.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            List<GetTransactionHistoryByUserIdAndTransactionDateDto> transactionHistoryDtoList = historiTransaksiList.stream()
                    .map(transactionHistory -> GetTransactionHistoryByUserIdAndTransactionDateDto.builder()
                            .transactionDate(transactionHistory.getTglTransaksi())
                            .jangkaWaktu(transactionHistory.getJangkaWaktu())
                            .dueDate(transactionHistory.getJatuhTempo())
                            .total(transactionHistory.getTotal())
                            .build())
                    .collect(Collectors.toList());

            return ResponseUtil.buildSuccessResponse(GetTransactionHistoryByUserIdAndTransactionDateResponse.builder()
                    .getTransactionHistoryByUserIdAndTransactionDateDtoList(transactionHistoryDtoList)
                    .build());
        } catch (Exception e) {
            Logger.error("Error get transaction history : ", e, this.getClass());
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }
}
