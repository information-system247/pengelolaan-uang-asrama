package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetHistoriTransaksiListResponse implements Serializable {
    private static final long serialVersionUID = -2398768145047528548L;

    private List<HistoriTransaksiDto> historiTransaksiList;
    private List<HistoriTransaksiMasukDto> historiTransaksiMasukList;
    private List<HistoriTransaksiKeluarDto> historiTransaksiKeluarList;
}
