/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.constant;

/**
 *
 * @author shinriokudo
 */
public enum MessageConstant {
    
    SUCCESS(MessageCodeConstant.SUCCESS, "Success"),
    SOMETHING_WENT_WRONG(MessageCodeConstant.SOMETHING_WENT_WRONG, "Something Went Wrong"),
    DATA_NOT_FOUND(MessageCodeConstant.DATA_NOT_FOUND, "Data not found"),
    BAD_REQUEST(MessageCodeConstant.BAD_REQUEST, "Bad Request"),
    SQL_ERROR(MessageCodeConstant.SQL_ERROR, "SQL Error"),
    USERNAME_FOUND(MessageCodeConstant.USERNAME_FOUND, "Username Found"),
    NIK_FOUND(MessageCodeConstant.NIK_FOUND, "NIK Found"),
    USERNAME_OR_PASSWORD_WRONG(MessageCodeConstant.USERNAME_OR_PASSWORD_WRONG, "Username Or Password Wrong"),
    CANNOT_DELETED(MessageCodeConstant.CANNOT_DELETED, "Cannot Deleted This Data"),
    BENDAHARA_IS_USED_CODE(MessageCodeConstant.BENDAHARA_IS_USED, "Hak akses bendahara sudah digunakan"),
    ROOM_FOUND(MessageCodeConstant.ROOM_FOUND, "Kode kamar sudah tersedia."),
    FULL_ROOM(MessageCodeConstant.FULL_ROOM, "Full Room"),
    CANNOT_UPDATE_CAPACITY(MessageCodeConstant.CANNOT_UPDATE_CAPACITY, "Cannot Update Capacity");

    private final String messageCode;
    private final String messageValue;

    private MessageConstant(String messageCode, String messageValue) {
        this.messageCode = messageCode;
        this.messageValue = messageValue;
    }

    public String getMessageCode() {
        return messageCode;
    }
    
    public String getMessageValue() {
        return messageValue;
    }
    
    public static MessageConstant getMessageUtilCode(String messageCode) {
        for (MessageConstant messageConstant : MessageConstant.values()) {
            if (messageConstant.getMessageCode().equals(messageCode)) {
                return messageConstant;
            }
        }
        return null;
    }
    
    public static String getMessageUtilValue(String messageCode) {
        MessageConstant messageConstant = getMessageUtilCode(messageCode);
        return (messageConstant != null) ? messageConstant.getMessageValue() : null;
    }
}
