/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.constant;

/**
 *
 * @author shinriokudo
 */
public enum GenderConstant {
    PRIA("L"),
    WANITA("P");
    
    private final String gender;
    
    private GenderConstant(String gender) {
        this.gender = gender;
    }
    
    public String getGender() {
        return gender;
    }
    
    public static GenderConstant getGenderCodeUtil(String genderCode) {
        for (GenderConstant genderConstant : GenderConstant.values()) {
            if (genderConstant.getGender().equals(genderCode)) {
                return genderConstant;
            }
        }
        return null;
    }
}
