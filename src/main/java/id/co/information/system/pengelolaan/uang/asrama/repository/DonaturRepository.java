package id.co.information.system.pengelolaan.uang.asrama.repository;

import id.co.information.system.pengelolaan.uang.asrama.constant.GenderConstant;
import id.co.information.system.pengelolaan.uang.asrama.model.Donatur;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;

import java.sql.ResultSet;
import java.util.List;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

public class DonaturRepository extends CommonBaseRepository{

    private Optional<Donatur> getFindOptional(String sql) {
        try {
            resultSet = find(sql);
            if (resultSet == null) {
                return Optional.empty();
            }
            if(resultSet.next()) {
                return Optional.of(donaturBuilder(resultSet));
            }
            return Optional.empty();
        } catch (SQLException e) {
            Logger.error("Error find optional data donatur : ", e, DonaturRepository.class);
            return Optional.empty();
        } finally {
            closeConnection(true);
        }
    }

    private List<Donatur> getFindList(String sql) {
        List<Donatur> donaturList = new ArrayList<>();
        try {
            resultSet = find(sql);
            if (resultSet == null) {
                return donaturList;
            }
            while(resultSet.next()) {
                donaturList.add(donaturBuilder(resultSet));
            }
            return donaturList;
        } catch (SQLException e) {
            Logger.error("Error find list data donatur : ", e, DonaturRepository.class);
            return donaturList;
        } finally {
            closeConnection(true);
        }
    }

    private Donatur donaturBuilder(ResultSet resultSet) {
        try {
            return Donatur.builder()
                    .id(Long.parseLong(resultSet.getString("id")))
                    .nik(resultSet.getString("nik"))
                    .namaLengkap(resultSet.getString("nama_lengkap"))
                    .gender(GenderConstant.getGenderCodeUtil(resultSet.getString("gender")))
                    .alamat(resultSet.getString("alamat"))
                    .noHp(resultSet.getString("no_hp"))
                    .build();
        } catch (NumberFormatException | SQLException e) {
            Logger.error("Error Builder donatur : ", e, DonaturRepository.class);
            return Donatur.builder().build();
        }
    }

    public Boolean save(Donatur donatur) {
        try {
            String query = "INSERT INTO donatur (nik,nama_lengkap,gender,alamat,no_hp)VALUES('"+
                    donatur.getNik()+"','"+
                    donatur.getNamaLengkap()+"','"+
                    donatur.getGender().getGender()+"','"+
                    donatur.getAlamat()+"','"+
                    donatur.getNoHp()+"')";
            return execute(query);
        } catch (Exception e) {
            Logger.error("Error query save donatur : ", e, DonaturRepository.class);
            return false;
        } finally {
            closeConnection(false);
        }
    }

    public Boolean update(Donatur donatur) {
        try {
            String query = "UPDATE`donatur`SET`nama_lengkap` = '"+donatur.getNamaLengkap()+
                    "',gender = '"+donatur.getGender().getGender()+
                    "',alamat = '"+donatur.getAlamat()+
                    "',no_hp = '"+donatur.getNoHp()+"' WHERE nik = '"+donatur.getNik()+"'";
            return execute(query);
        } catch (Exception e) {
            Logger.error("Error query update donatur : ", e, DonaturRepository.class);
            return false;
        } finally {
            closeConnection(false);
        }
    }

    public Boolean deleteByNik(String nik) {
        try {
            String query = "DELETE FROM donatur WHERE nik = '"+ nik +"'";
            return execute(query);
        } catch (Exception e) {
            Logger.error("Error query delete donatur : ", e, DonaturRepository.class);
            return false;
        } finally {
            closeConnection(false);
        }
    }

    @SuppressWarnings("all")
    public Optional<Donatur> findById(Long id) {
        return getFindOptional("SELECT * FROM donatur WHERE id = '"+ id +"'");
    }

    @SuppressWarnings("all")
    public List<Donatur> findAll() {
        return getFindList("SELECT * FROM donatur ORDER BY nama_lengkap ASC");
    }

    @SuppressWarnings("all")
    public Optional<Donatur> findByNik(String nik) {
        return getFindOptional("SELECT * FROM donatur WHERE nik='"+ nik +"'");
    }
}
