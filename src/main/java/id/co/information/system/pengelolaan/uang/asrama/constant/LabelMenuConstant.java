/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package id.co.information.system.pengelolaan.uang.asrama.constant;

/**
 *
 * @author wahyu
 */
public class LabelMenuConstant {
    
    public static final String BERANDA = "Beranda";
    public static final String DONATUR = "Donatur";
    public static final String TRANSAKSI_MASUK = "Transaksi Masuk";
    public static final String TRANSAKSI_KELUAR = "Transaksi Keluar";
    public static final String KAMAR = "Kamar";
    public static final String LAPORAN_TRANSAKSI = "Laporan Transaksi";
    public static final String LAPORAN_TUNGGAKAN = "Laporan Tunggakan";
    public static final String PENYEWA = "Anggota";
}
