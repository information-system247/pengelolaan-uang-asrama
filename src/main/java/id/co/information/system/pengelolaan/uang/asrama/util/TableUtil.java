/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.util;

import id.co.information.system.pengelolaan.uang.asrama.dto.ColumnTableConfigRequest;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author shinriokudo
 */
public class TableUtil {
    
    public void clearValueTable(DefaultTableModel defaultTableModel) {
        defaultTableModel.getDataVector().removeAllElements();
        defaultTableModel.fireTableDataChanged();
    }
    
    public void setColumnTableModel(ColumnTableConfigRequest request) {
        try {
            if (request.getColumnTableConfigModelMap().isEmpty()) {
                throw new NullPointerException("bad request");
            }
            
            request.getColumnTableConfigModelMap().forEach(
                    (key, value) -> key.setModel(value)
            );
        } catch (NullPointerException e) {
            Logger.error("Error set table column : ", e, TableUtil.class);
            throw e;
        }
    }
}
