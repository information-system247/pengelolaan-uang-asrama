package id.co.information.system.pengelolaan.uang.asrama.dto.room;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetKamarKosongDto implements Serializable {
    private static final long serialVersionUID = 5725780643936925207L;

    private String noKamar;
    private int kapasitas;
    private int terisi;
    private int tersedia;
}
