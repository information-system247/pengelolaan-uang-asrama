package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HistoriTransaksiByPenyewaDto implements Serializable {
    private static final long serialVersionUID = 3988349433795121113L;

    private Date tglTransaksi;
    private String fullName;
    private String noKamar;
    private int jangkaWaktu;
    private Long total;
    private String keterangan;
    private Date jatuhTempo;
}
