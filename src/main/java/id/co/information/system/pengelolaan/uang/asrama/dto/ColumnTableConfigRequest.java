/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.dto;

import java.io.Serializable;
import java.util.Map;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ColumnTableConfigRequest implements Serializable{
    private static final long serialVersionUID = -7805051269170444097L;

    private Map<JTable, DefaultTableModel> columnTableConfigModelMap;
}
