package id.co.information.system.pengelolaan.uang.asrama.dto.room;

import id.co.information.system.pengelolaan.uang.asrama.constant.StatusKamarConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetkamarDto implements Serializable {
    private static final long serialVersionUID = -4119463355344014095L;

    private String noKamar;
    private Integer kapasitas;
    private StatusKamarConstant statusKamar;
    private Integer tersedia;
}
