/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.view;

import id.co.information.system.pengelolaan.uang.asrama.config.ButtonConfig;
import id.co.information.system.pengelolaan.uang.asrama.config.ColumnTableConfig;
import id.co.information.system.pengelolaan.uang.asrama.config.PanelConfig;
import id.co.information.system.pengelolaan.uang.asrama.constant.AsramaConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.ButtonConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.FromTransactionConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.GenderConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.HakAksesConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.LabelMenuConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.MessageCodeConstant;
import static id.co.information.system.pengelolaan.uang.asrama.constant.PanelMenuConstant.PANEL_MENU_BERANDA;
import static id.co.information.system.pengelolaan.uang.asrama.constant.PanelMenuConstant.PANEL_MENU_BERANDA_PENYEWA;
import static id.co.information.system.pengelolaan.uang.asrama.constant.PanelMenuConstant.PANEL_MENU_DONATUR;
import static id.co.information.system.pengelolaan.uang.asrama.constant.PanelMenuConstant.PANEL_MENU_KAMAR;
import static id.co.information.system.pengelolaan.uang.asrama.constant.PanelMenuConstant.PANEL_MENU_LAPORAN;
import static id.co.information.system.pengelolaan.uang.asrama.constant.PanelMenuConstant.PANEL_MENU_LAPORAN_PENYEWA;
import static id.co.information.system.pengelolaan.uang.asrama.constant.PanelMenuConstant.PANEL_MENU_LAPORAN_TUNGGAKAN;
import static id.co.information.system.pengelolaan.uang.asrama.constant.PanelMenuConstant.PANEL_MENU_PENYEWA;
import static id.co.information.system.pengelolaan.uang.asrama.constant.PanelMenuConstant.PANEL_SHOW_TRANSAKSI_KELUAR;
import static id.co.information.system.pengelolaan.uang.asrama.constant.PanelMenuConstant.PANEL_SHOW_TRANSAKSI_MASUK;
import id.co.information.system.pengelolaan.uang.asrama.constant.StatusKamarConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.TipeTransaksiConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.UserStatusConstant;
import id.co.information.system.pengelolaan.uang.asrama.controller.HistoriTransaksiController;
import id.co.information.system.pengelolaan.uang.asrama.controller.PortalController;
import id.co.information.system.pengelolaan.uang.asrama.controller.UserController;
import id.co.information.system.pengelolaan.uang.asrama.dto.ButtonColorRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.ColumnTableConfigRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.Response;
import id.co.information.system.pengelolaan.uang.asrama.dto.donatur.GetDonaturByNikRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.donatur.GetDonaturListResponse;
import id.co.information.system.pengelolaan.uang.asrama.dto.donatur.StoreDonaturRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.room.GetKamarByNoKamarRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.room.GetKamarKosongDto;
import id.co.information.system.pengelolaan.uang.asrama.dto.room.GetKamarKosongListResponse;
import id.co.information.system.pengelolaan.uang.asrama.dto.room.GetKamarListResponse;
import id.co.information.system.pengelolaan.uang.asrama.dto.room.StoreKamarRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory.GetDueDateOverDueDateRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory.GetDueDateOverDueDateResponse;
import id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory.GetHistoriTransaksiKeluarListResponse;
import id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory.GetHistoriTransaksiListByTglTransaksiAndTipeTransaksiRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory.GetHistoriTransaksiListResponse;
import id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory.GetHistoriTransaksiMasukListByFromTransaksiRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory.GetHistoriTransaksiMasukListByFromTransaksiResponse;
import id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory.GetTransactionHistoryByUserIdAndTransactionDateDto;
import id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory.GetTransactionHistoryByUserIdAndTransactionDateRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory.GetTransactionHistoryByUserIdAndTransactionDateResponse;
import id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory.GetTransactionHistoryOutgoingRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.user.GetUserByNikRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.user.GetUserByNikResponse;
import id.co.information.system.pengelolaan.uang.asrama.dto.user.GetUserByUsernameAndPasswordResponse;
import id.co.information.system.pengelolaan.uang.asrama.dto.user.GetUserListResponse;
import id.co.information.system.pengelolaan.uang.asrama.dto.user.StoreUserRequest;
import id.co.information.system.pengelolaan.uang.asrama.model.Kamar;
import id.co.information.system.pengelolaan.uang.asrama.model.User;
import id.co.information.system.pengelolaan.uang.asrama.util.ComboBoxUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.DateUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.ExportFileUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.FormatCurencyUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.ImageUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;
import id.co.information.system.pengelolaan.uang.asrama.util.RadioButtonUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.RequestUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.TableUtil;
import static java.awt.Image.SCALE_DEFAULT;

import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import static java.lang.System.exit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import static javax.swing.JFileChooser.APPROVE_OPTION;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author wahyu
 */
public class Main extends javax.swing.JFrame implements Runnable {

    private TableUtil tableUtil = new TableUtil();
    private ButtonConfig buttonConfig = new ButtonConfig();
    private PanelConfig panelConfig = new PanelConfig();
    private ColumnTableConfig columnTableConfig = new ColumnTableConfig();
    private UserController userController = new UserController();
    private PortalController portalController = new PortalController();
    private HistoriTransaksiController historiTransaksiController = new HistoriTransaksiController();
    private GetUserByUsernameAndPasswordResponse userOptional = new GetUserByUsernameAndPasswordResponse();
    
    String imageBase64 = null;
    /**
     * Creates new form Main
     * @param getUserResponse
     */
    public Main(GetUserByUsernameAndPasswordResponse getUserResponse) {
        initComponents();
        this.userOptional = getUserResponse;
        index();
    }

    public Main() {
        initComponents();
    }

    private void index() {
        lblMenu.setFocusable(true);
        panelMainBorder.setVisible(true);
        lbFullName.setText(userOptional.getUser().getNamaLengkap());
        tableUtil.setColumnTableModel(columnTableModel());
        lblMenu.setText(LabelMenuConstant.BERANDA);
        if (userOptional.getUser().getHakAkses().equals(HakAksesConstant.BENDAHARA.name())) {
            setMenuBendahara();
        } else {
            setMenuPenyewa();
        }
        
        Thread thread = new Thread(this);
        thread.start();
    }

    private void setMenuBendahara() {
        panelConfig.setPanel(panelMenu, panelMenuBendahara);
        panelConfig.setPanel(panelMainBorder, panelBeranda);
        columnTableConfig.setColumnTableBeranda(tbBerandaDaftarJatuhTempo, tbBerandaDaftarKeterlambatan, tbBerandaDaftarKamarKosong);
        columnTableConfig.setColumnTableLaporanTunggakan(tbLaporanTunggakan);
        lbJmlKamarInBerandaAdmin.setText(String.valueOf(portalController.numberOfRooms()));
        lbJmlPenyewaInBerandaAdmin.setText(String.valueOf(userController.numberOfTenants()));
        lbJmlDonaturInBerandaAdmin.setText(String.valueOf(portalController.numberOfDonatur()));
        lbUangMasukTodayInBerandaAdmin.setText(String.valueOf(FormatCurencyUtil.indonesia(historiTransaksiController.amountOfMoneyInToday())));
        lbJmlUangKelarTodayInBerandaAdmin.setText(String.valueOf(FormatCurencyUtil.indonesia(historiTransaksiController.amountOfMoneyOutToday())));
        lbSisaSaldoInBerandaAdmin.setText(FormatCurencyUtil.indonesia(historiTransaksiController.getSisaSaldo()));
        tabTableHomePageAdmin.setSelectedIndex(0);
        showDueDateOverDueDateListInAdminHomepage(true);
        spTbDaftarjatuhTempo.requestFocus();
        lbFullName.setText(userOptional.getUser().getNamaLengkap());
        //hide
        if (tabTableIncomingTransaction.getComponentCount() > 2) {
            tabTableIncomingTransaction.removeTabAt(2);
        }
        txtSearchOutgoingTransaction.setVisible(false);
        btnSearchOutgoingTransaction.setVisible(false);
    }

    private void setMenuPenyewa() {
        panelConfig.setPanel(panelMenu, panelMenuPenyewa);
        panelConfig.setPanel(panelMainBorder, panelBerandaPenyewa);
        Response response = historiTransaksiController.getTransactionHistoryByUserId(GetTransactionHistoryByUserIdAndTransactionDateRequest.builder().userId(userOptional.getUser().getId()).build());
        lbTglMasuk.setText(DateUtil.toString(userOptional.getUser().getTglMasuk(), DateUtil.FORMAT_DATE_TIME_IDN));
        lbTagihanBulanan.setText(FormatCurencyUtil.indonesia(userOptional.getUser().getMonthlyPayment()));
        lbDueDate.setText(DateUtil.toString(DateUtil.addMonth(userOptional.getUser().getTglMasuk(), 1), DateUtil.FORMAT_DATE_IDN));
        lbTotalTransaction.setText(FormatCurencyUtil.indonesia(0L));
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            GetTransactionHistoryByUserIdAndTransactionDateResponse transactionHistory = (GetTransactionHistoryByUserIdAndTransactionDateResponse) response.getResponseDto().getData();
            Optional<GetTransactionHistoryByUserIdAndTransactionDateDto> transactionHistoryOptional = transactionHistory.getGetTransactionHistoryByUserIdAndTransactionDateDtoList().stream().findFirst();
            if (transactionHistoryOptional.isPresent()) {
                lbDueDate.setText(DateUtil.toString(transactionHistoryOptional.get().getDueDate(), DateUtil.FORMAT_DATE_IDN));
                Long fullAmount = transactionHistory.getGetTransactionHistoryByUserIdAndTransactionDateDtoList().stream().mapToLong(GetTransactionHistoryByUserIdAndTransactionDateDto::getTotal).sum();
                lbTotalTransaction.setText(FormatCurencyUtil.indonesia(fullAmount));
            }
        }
        txtFullName.setText(userOptional.getUser().getNamaLengkap());
        txtNik.setText(userOptional.getUser().getNik());
        txtgender.setText(userOptional.getUser().getGender().getGender());
        txtTglMasukRenter.setText(DateUtil.toString(userOptional.getUser().getTglMasuk(), DateUtil.FORMAT_DATE_TIME_IDN));
        txtAlamat.setText(userOptional.getUser().getAlamat());
        txtPhoneNumber.setText(userOptional.getUser().getNoHp());
        txtUsername.setText(userOptional.getUser().getUsername());
        txtPasswordRenter.setText(userOptional.getUser().getPassword());
        lbFullNameUser.setText(userOptional.getUser().getNamaLengkap());
        ImageUtil.convertToIcon(userOptional.getUser().getFoto(), lbFotoMenuPenyewa);
    }

    private void showEmptyRoomListInAdminHomepage() {
        Response response = portalController.getKamarKosongList();
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            GetKamarKosongListResponse getKamarKosongListResponse = (GetKamarKosongListResponse) response.getResponseDto().getData();
            tableUtil.clearValueTable(columnTableConfig.modelDaftarKamarKosongInBerandaAdmin);
            AtomicInteger  counter = new AtomicInteger(1);
            getKamarKosongListResponse.getKamarList().forEach(
                    res -> columnTableConfig.modelDaftarKamarKosongInBerandaAdmin.addRow(new Object[]{
                            counter.getAndIncrement(), res.getNoKamar(), res.getKapasitas(), res.getTerisi(), res.getTersedia()
                    })
            );
        }
    }

    private void showDueDateOverDueDateListInAdminHomepage(boolean isDueDate) {
        DefaultTableModel defaultTableModel;
        if (isDueDate) {
            defaultTableModel = columnTableConfig.modelDaftarJatuhTempoInBerandaAdmin;
        } else {
            defaultTableModel = columnTableConfig.modelDaftarKeterlambatanInBerandaAdmin;
        }
        Response response = historiTransaksiController.getDueDateOverDueDate(GetDueDateOverDueDateRequest.builder().isDueDate(isDueDate).build());
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            GetDueDateOverDueDateResponse getDueDateOverDueDateResponse = (GetDueDateOverDueDateResponse) response.getResponseDto().getData();
            tableUtil.clearValueTable(defaultTableModel);
            AtomicInteger counter = new AtomicInteger(1);
            getDueDateOverDueDateResponse.getDueDateOverDueDateList().forEach(
                    res -> defaultTableModel.addRow(new Object[]{
                            counter.getAndIncrement(), DateUtil.toString(res.getDueDate(), DateUtil.FORMAT_DATE_IDN), res.getFullName(), res.getNoKamar(), FormatCurencyUtil.indonesia(res.getBillAmount())
                    })
            );
        }
    }
    
    private void showLaporanTunggakan() {
        DefaultTableModel defaultTableModel = columnTableConfig.modelDaftarLaporanTunggakan;
        Response response = historiTransaksiController.getDueDateOverDueDate(GetDueDateOverDueDateRequest.builder().isDueDate(false).build());
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            GetDueDateOverDueDateResponse getDueDateOverDueDateResponse = (GetDueDateOverDueDateResponse) response.getResponseDto().getData();
            tableUtil.clearValueTable(defaultTableModel);
            AtomicInteger counter = new AtomicInteger(1);
            getDueDateOverDueDateResponse.getDueDateOverDueDateList().forEach(
                    res -> defaultTableModel.addRow(new Object[]{
                            counter.getAndIncrement(), DateUtil.toString(res.getDueDate(), DateUtil.FORMAT_DATE_IDN), res.getFullName(), res.getNoKamar(), FormatCurencyUtil.indonesia(res.getBillAmount())
                    })
            );
        }
    }

    private void showRoomList() {
        Response response = portalController.getKamarList();
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            GetKamarListResponse getKamarListResponse = (GetKamarListResponse) response.getResponseDto().getData();
            tableUtil.clearValueTable(columnTableConfig.modelDaftarKamar);
            AtomicInteger counter = new AtomicInteger(1);
            getKamarListResponse.getKamarList().forEach(
                    res -> columnTableConfig.modelDaftarKamar.addRow(new Object[]{
                            counter.getAndIncrement(), res.getNoKamar(), res.getKapasitas(), res.getStatusKamar(), res.getTersedia()
                    })
            );
        }
    }

    private void showUserList() {
        Response response = userController.getUserList();
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            GetUserListResponse getUserListResponse = (GetUserListResponse) response.getResponseDto().getData();
            tableUtil.clearValueTable(columnTableConfig.modelDaftarPenyewa);
            AtomicInteger counter = new AtomicInteger(1);
            getUserListResponse.getUserDtoList().forEach(
                    res -> {
                        if (!HakAksesConstant.BENDAHARA.equals(res.getHakAkses())) {
                            columnTableConfig.modelDaftarPenyewa.addRow(new Object[]{
                                    counter.getAndIncrement(), res.getNik(), res.getNamaLengkap(), res.getGender(), res.getAlamat(), res.getNoHp(), res.getNamaPengguna(), res.getKataSandi(), DateUtil.toString(res.getTglMasuk(), DateUtil.FORMAT_DATE_IDN), res.getUserStatus(), res.getNoKamar(), FormatCurencyUtil.indonesia(res.getBilAmount())
                            });
                        }
                    }
            );
        }
    }

    private void showDonaturList() {
        Response response = portalController.getDonaturList();
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            GetDonaturListResponse getDonaturListResponse = (GetDonaturListResponse) response.getResponseDto().getData();
            tableUtil.clearValueTable(columnTableConfig.modelDaftarDonatur);
            AtomicInteger counter = new AtomicInteger(1);
            getDonaturListResponse.getDonaturList().forEach(
                    res -> columnTableConfig.modelDaftarDonatur.addRow(new Object[]{
                            counter.getAndIncrement(), res.getNik(), res.getNamaLengkap(), res.getGender(), res.getAlamat(), res.getNoHp()
                    })
            );
        }
    }

    public void showIncomingTransactionHistoryList(GetHistoriTransaksiMasukListByFromTransaksiRequest request) {
        Response response = historiTransaksiController.incomingTransactionHistoryList(request);
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            GetHistoriTransaksiMasukListByFromTransaksiResponse getHistoriTransaksiMasukListByFromTransaksiResponse = (GetHistoriTransaksiMasukListByFromTransaksiResponse) response.getResponseDto().getData();
            AtomicInteger counter = new AtomicInteger(1);
            switch(request.getFromTransaksi()) {
                case ANGGOTA:
                    lblCariBendahara.setText(AsramaConstant.LABEL_CARI_TRANSAKSI_MASUK_BENDAHARA_ANGGOTA);
                    tableUtil.clearValueTable(columnTableConfig.modelDaftarTransaksiMasukFromPenyewa);
                    getHistoriTransaksiMasukListByFromTransaksiResponse.getHistoriTransaksiByPenyewaList().forEach(
                            res -> columnTableConfig.modelDaftarTransaksiMasukFromPenyewa.addRow(new Object[]{
                                    counter.getAndIncrement(), DateUtil.toString(res.getTglTransaksi(), DateUtil.FORMAT_DATE_TIME_IDN),
                                    res.getFullName(), res.getNoKamar(), res.getJangkaWaktu(), FormatCurencyUtil.indonesia(res.getTotal()), res.getKeterangan(),
                                    DateUtil.toString(res.getJatuhTempo(), DateUtil.FORMAT_DATE_IDN)
                            })
                    );
                    break;
                case DONATUR:
                    lblCariBendahara.setText(AsramaConstant.LABEL_CARI_TRANSAKSI_MASUK_BENDAHARA_DONATUR);
                    tableUtil.clearValueTable(columnTableConfig.modelDaftarTransaksiMasukFromDonatur);
                    getHistoriTransaksiMasukListByFromTransaksiResponse.getHistoriTransaksiByDonaturList().forEach(
                            res -> columnTableConfig.modelDaftarTransaksiMasukFromDonatur.addRow(new Object[]{
                                    counter.getAndIncrement(), DateUtil.toString(res.getTglTransaksi(), DateUtil.FORMAT_DATE_TIME_IDN),
                                    res.getFullName(), FormatCurencyUtil.indonesia(res.getAmount()), res.getKeterangan()
                            })
                    );
                    break;
                case LAINNYA:
                    tableUtil.clearValueTable(columnTableConfig.modelDaftarTransaksiMasukFromLainnya);
                    getHistoriTransaksiMasukListByFromTransaksiResponse.getHistoriTransaksiByLainnyaList().forEach(
                            res -> columnTableConfig.modelDaftarTransaksiMasukFromLainnya.addRow(new Object[]{
                                    counter.getAndIncrement(), DateUtil.toString(res.getTglTransaksi(), DateUtil.FORMAT_DATE_TIME_IDN),
                                    res.getNamaTranaksi(), res.getFullName(), FormatCurencyUtil.indonesia(res.getAmount()), res.getKeterangan()
                            })
                    );
                    break;
                default:
                    Logger.error("Error show incoming transaction history", Main.class);
                    break;
            }
            lbSisaSaldo.setText(FormatCurencyUtil.indonesia(getHistoriTransaksiMasukListByFromTransaksiResponse.getSisaSaldo()));
            lblTotalTransaksi.setText(FormatCurencyUtil.indonesia(getHistoriTransaksiMasukListByFromTransaksiResponse.getTotal()));
        }
    }

    public void showOutgoingTransactionHistoryList(GetTransactionHistoryOutgoingRequest request) {
        Response response = historiTransaksiController.outgoingTransactionHistoryList(request);
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            GetHistoriTransaksiKeluarListResponse getHistoriTransaksiKeluarListResponse = (GetHistoriTransaksiKeluarListResponse) response.getResponseDto().getData();
            tableUtil.clearValueTable(columnTableConfig.modelDaftarTransaksiKeluar);
            AtomicInteger counter = new AtomicInteger(1);
            getHistoriTransaksiKeluarListResponse.getHistoriTransaksiKeluarList().forEach(
                    res -> columnTableConfig.modelDaftarTransaksiKeluar.addRow(new Object[]{
                            counter.getAndIncrement(), DateUtil.toString(res.getTglTransaksi(), DateUtil.FORMAT_DATE_TIME_IDN),
                            FormatCurencyUtil.indonesia(res.getAmount()), res.getKeterangan()
                    })
            );
            lbSisaSaldoInTransKeluar.setText(FormatCurencyUtil.indonesia(getHistoriTransaksiKeluarListResponse.getSisaSaldo()));
            lblTotalTransaksiKeluar.setText(FormatCurencyUtil.indonesia(getHistoriTransaksiKeluarListResponse.getTotal()));
        }
    }

    private void showTransactionHistoryList(GetHistoriTransaksiListByTglTransaksiAndTipeTransaksiRequest request) {
        Response response = historiTransaksiController.getHistoriTransaksiList(request);
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            GetHistoriTransaksiListResponse getHistoriTransaksiListResponse = (GetHistoriTransaksiListResponse) response.getResponseDto().getData();
            AtomicInteger counter = new AtomicInteger(1);
            switch(request.getTipeTransaksi()) {
                case MASUK:
                    tableUtil.clearValueTable(columnTableConfig.modelDaftarLaporanTransaksiMasuk);
                    getHistoriTransaksiListResponse.getHistoriTransaksiMasukList().forEach(
                            res -> columnTableConfig.modelDaftarLaporanTransaksiMasuk.addRow(new Object[]{
                                    counter.getAndIncrement(), DateUtil.toString(res.getTglTransaksi(), DateUtil.FORMAT_DATE_TIME_IDN), res.getFromTransaction(), res.getFullName(), res.getNoKamar(), res.getJangkaWaktu(), FromTransactionConstant.ANGGOTA.equals(res.getFromTransaction()) ? DateUtil.toString(res.getJatuhTempo(), DateUtil.FORMAT_DATE_IDN) : "-", FormatCurencyUtil.indonesia(res.getTotal()), res.getKeterangan()
                            })
                    );
                    break;
                case KELUAR:
                    tableUtil.clearValueTable(columnTableConfig.modelDaftarLaporanTransaksiKeluar);
                    getHistoriTransaksiListResponse.getHistoriTransaksiKeluarList().forEach(
                            res -> columnTableConfig.modelDaftarLaporanTransaksiKeluar.addRow(new Object[]{
                                    counter.getAndIncrement(), DateUtil.toString(res.getTglTransaksi(), DateUtil.FORMAT_DATE_TIME_IDN), FormatCurencyUtil.indonesia(res.getAmount()), res.getKeterangan()
                            })
                    );
                    break;
                case ALL:
                    tableUtil.clearValueTable(columnTableConfig.modelDaftarLaporanSemuaTransaksi);
                    getHistoriTransaksiListResponse.getHistoriTransaksiList().forEach(
                            res -> columnTableConfig.modelDaftarLaporanSemuaTransaksi.addRow(new Object[]{
                                    counter.getAndIncrement(), DateUtil.toString(res.getTglTransaksi(), DateUtil.FORMAT_DATE_TIME_IDN),
                                    res.getTipeTransaksi(), res.getFullName(), FormatCurencyUtil.indonesia(res.getTotal()), res.getKeterangan()
                            })
                    );
                    break;
                default:
                    Logger.error("transaction type not found", Main.class);
                    break;
            }
        }
    }

    private void showTransactionHistoryByUser(GetTransactionHistoryByUserIdAndTransactionDateRequest request) {
        Response response = historiTransaksiController.getTransactionHistoryByUserId(request);
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            GetTransactionHistoryByUserIdAndTransactionDateResponse transactionHistoryResponse = (GetTransactionHistoryByUserIdAndTransactionDateResponse) response.getResponseDto().getData();
            tableUtil.clearValueTable(columnTableConfig.modelDaftarLaporanTransaksiPenyewa);
            AtomicInteger counter = new AtomicInteger(1);
            transactionHistoryResponse.getGetTransactionHistoryByUserIdAndTransactionDateDtoList().forEach(
                    res -> columnTableConfig.modelDaftarLaporanTransaksiPenyewa.addRow(new Object[] {
                            counter.getAndIncrement(), DateUtil.toString(res.getTransactionDate(), DateUtil.FORMAT_DATE_TIME_IDN),
                            res.getJangkaWaktu(), DateUtil.toString(res.getDueDate(), DateUtil.FORMAT_DATE_TIME_IDN), FormatCurencyUtil.indonesia(res.getTotal())
                    })
            );
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        genderGrup = new javax.swing.ButtonGroup();
        statusUserGrup = new javax.swing.ButtonGroup();
        pmTbUser = new javax.swing.JPopupMenu();
        mnUbah = new javax.swing.JMenuItem();
        mnHapus = new javax.swing.JMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        pmTbDonatur = new javax.swing.JPopupMenu();
        updateDonatur = new javax.swing.JMenuItem();
        deleteDonatur = new javax.swing.JMenuItem();
        pmTbKamar = new javax.swing.JPopupMenu();
        updateKamar = new javax.swing.JMenuItem();
        deleteKamar = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lblMenu = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        panelMenu = new javax.swing.JPanel();
        panelMenuPenyewa = new javax.swing.JPanel();
        jPanel32 = new javax.swing.JPanel();
        jLabel40 = new javax.swing.JLabel();
        btnMenuDashboardPenyewa = new javax.swing.JButton();
        jPanel33 = new javax.swing.JPanel();
        jLabel41 = new javax.swing.JLabel();
        btnMenuLaporanPenyewa = new javax.swing.JButton();
        jLabel61 = new javax.swing.JLabel();
        lbFullNameUser = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        lbFotoMenuPenyewa = new javax.swing.JLabel();
        panelMenuBendahara = new javax.swing.JPanel();
        btnMenuDashboard = new javax.swing.JButton();
        btnMenuTransaksiMasuk = new javax.swing.JButton();
        btnMenuTransaksiKeluar = new javax.swing.JButton();
        btnMenuLaporan = new javax.swing.JButton();
        lbFullName = new javax.swing.JLabel();
        btnMenuKamar = new javax.swing.JButton();
        btnMenuDonatur = new javax.swing.JButton();
        btnMenuPenyewa = new javax.swing.JButton();
        jPanel20 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanel21 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jPanel23 = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        btnMenuLaporanTunggakan = new javax.swing.JButton();
        panelMainBorder = new javax.swing.JPanel();
        panelBerandaPenyewa = new javax.swing.JPanel();
        jPanel34 = new javax.swing.JPanel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jPanel35 = new javax.swing.JPanel();
        jPanel36 = new javax.swing.JPanel();
        lbTglMasuk = new javax.swing.JLabel();
        jPanel37 = new javax.swing.JPanel();
        jLabel46 = new javax.swing.JLabel();
        jPanel40 = new javax.swing.JPanel();
        lbTotalTransaction = new javax.swing.JLabel();
        jPanel41 = new javax.swing.JPanel();
        jLabel50 = new javax.swing.JLabel();
        jPanel42 = new javax.swing.JPanel();
        lbDueDate = new javax.swing.JLabel();
        jPanel43 = new javax.swing.JPanel();
        jLabel52 = new javax.swing.JLabel();
        jPanel44 = new javax.swing.JPanel();
        lbTagihanBulanan = new javax.swing.JLabel();
        jPanel45 = new javax.swing.JPanel();
        jLabel54 = new javax.swing.JLabel();
        jPanel48 = new javax.swing.JPanel();
        jPanel38 = new javax.swing.JPanel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        txtTglMasukRenter = new javax.swing.JTextField();
        txtUsername = new javax.swing.JTextField();
        txtNik = new javax.swing.JTextField();
        txtgender = new javax.swing.JTextField();
        jScrollPane15 = new javax.swing.JScrollPane();
        txtAlamat = new javax.swing.JTextArea();
        txtFullName = new javax.swing.JTextField();
        txtPhoneNumber = new javax.swing.JTextField();
        txtPasswordRenter = new javax.swing.JPasswordField();
        panelBeranda = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        lbJmlKamarInBerandaAdmin = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        lbSisaSaldoInBerandaAdmin = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        lbUangMasukTodayInBerandaAdmin = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        lbJmlUangKelarTodayInBerandaAdmin = new javax.swing.JLabel();
        jPanel19 = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        jPanel27 = new javax.swing.JPanel();
        lbJmlPenyewaInBerandaAdmin = new javax.swing.JLabel();
        jPanel28 = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        jPanel29 = new javax.swing.JPanel();
        lbJmlDonaturInBerandaAdmin = new javax.swing.JLabel();
        jPanel30 = new javax.swing.JPanel();
        jLabel39 = new javax.swing.JLabel();
        jPanel31 = new javax.swing.JPanel();
        tabTableHomePageAdmin = new javax.swing.JTabbedPane();
        spTbDaftarjatuhTempo = new javax.swing.JScrollPane();
        tbBerandaDaftarJatuhTempo = new javax.swing.JTable();
        spTbDaftarKeterlambatan = new javax.swing.JScrollPane();
        tbBerandaDaftarKeterlambatan = new javax.swing.JTable();
        spTbDaftarKamarKosong = new javax.swing.JScrollPane();
        tbBerandaDaftarKamarKosong = new javax.swing.JTable();
        panelDonatur = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtNikDonatur = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtNamaLengkapDonatur = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        rbPriaDonatur = new javax.swing.JRadioButton();
        rbWanitaDonatur = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtAlamatDonatur = new javax.swing.JTextArea();
        jLabel7 = new javax.swing.JLabel();
        txtNoHpDonatur = new javax.swing.JTextField();
        btnSimpanDonatur = new javax.swing.JButton();
        btnBersihkanDonatur = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbDonatur = new javax.swing.JTable();
        panelTransaksiMasuk = new javax.swing.JPanel();
        panel_show_transaksi_masuk = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        tabTableIncomingTransaction = new javax.swing.JTabbedPane();
        jScrollPane8 = new javax.swing.JScrollPane();
        tbTransaksiPenyewa = new javax.swing.JTable();
        jScrollPane9 = new javax.swing.JScrollPane();
        tbTransaksiDonatur = new javax.swing.JTable();
        jScrollPane10 = new javax.swing.JScrollPane();
        tbTransaksiLainnya = new javax.swing.JTable();
        jLabel35 = new javax.swing.JLabel();
        lbSisaSaldo = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        lblTotalTransaksi = new javax.swing.JLabel();
        btnTambahTransaksiMasuk = new javax.swing.JButton();
        txtSearchIncomingTransaction = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        lblCariBendahara = new javax.swing.JLabel();
        panelTransaksiKeluar = new javax.swing.JPanel();
        panel_show_transaksi_keluar = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbTransaksiKeluar = new javax.swing.JTable();
        jLabel34 = new javax.swing.JLabel();
        lbSisaSaldoInTransKeluar = new javax.swing.JLabel();
        lblTotalTransaksiKeluar = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        btnTambahTransaksiKeluar = new javax.swing.JButton();
        txtSearchOutgoingTransaction = new javax.swing.JTextField();
        btnSearchOutgoingTransaction = new javax.swing.JButton();
        panelKamar = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        txtNoKamar = new javax.swing.JTextField();
        txtKapasitas = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        btnBersihkanKamar = new javax.swing.JButton();
        btnSimpanKamar = new javax.swing.JButton();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbKamar = new javax.swing.JTable();
        panelPenyewa = new javax.swing.JPanel();
        jPanel24 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        txtNikPenyewa = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtNamaLengkapPenyewa = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        rbPriaPenyewa = new javax.swing.JRadioButton();
        rbWanitaPenyewa = new javax.swing.JRadioButton();
        jLabel31 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txtAlamatPenyewa = new javax.swing.JTextArea();
        jLabel32 = new javax.swing.JLabel();
        txtNoHpPenyewa = new javax.swing.JTextField();
        lblUsername1 = new javax.swing.JLabel();
        txtUsernamePenyewa = new javax.swing.JTextField();
        lblPassword1 = new javax.swing.JLabel();
        txtPasswordPenyewa = new javax.swing.JPasswordField();
        lblstatus1 = new javax.swing.JLabel();
        rbStatusUserAktifPenyewa = new javax.swing.JRadioButton();
        rbStatusUserKeluarPenyewa = new javax.swing.JRadioButton();
        btnSimpanPenyewa = new javax.swing.JButton();
        btnBersihkanPenyewa = new javax.swing.JButton();
        lblKamar1 = new javax.swing.JLabel();
        cbKamarPenyewa = new javax.swing.JComboBox<>();
        lblUsername2 = new javax.swing.JLabel();
        txtJumlahTagihan = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        txtTglMasuk = new com.toedter.calendar.JDateChooser();
        lblUsername3 = new javax.swing.JLabel();
        lbFoto = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jPanel25 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tbPenyewa = new javax.swing.JTable();
        panelLaporan = new javax.swing.JPanel();
        panel_show_laporan = new javax.swing.JPanel();
        jPanel26 = new javax.swing.JPanel();
        reportTableTab = new javax.swing.JTabbedPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbLaporanSemuaTransaksi = new javax.swing.JTable();
        jScrollPane11 = new javax.swing.JScrollPane();
        tbLaporanTransaksiMasuk = new javax.swing.JTable();
        jScrollPane12 = new javax.swing.JScrollPane();
        tbLaporanTransaksiKeluar = new javax.swing.JTable();
        btnCetakLaporan = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        btnCetakLaporan1 = new javax.swing.JButton();
        txtDateTo = new com.toedter.calendar.JDateChooser();
        txtDateFrom = new com.toedter.calendar.JDateChooser();
        panelLaporanPenyewa = new javax.swing.JPanel();
        jPanel39 = new javax.swing.JPanel();
        jScrollPane16 = new javax.swing.JScrollPane();
        tbLaporanPenyewa = new javax.swing.JTable();
        btnCetakLaporan2 = new javax.swing.JButton();
        jLabel65 = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        btnCetakLaporan3 = new javax.swing.JButton();
        txtDateToUser = new com.toedter.calendar.JDateChooser();
        txtDateFromUser = new com.toedter.calendar.JDateChooser();
        panelLaporanTunggakan = new javax.swing.JPanel();
        panel_show_laporanTunggakan = new javax.swing.JPanel();
        jPanel47 = new javax.swing.JPanel();
        jScrollPane18 = new javax.swing.JScrollPane();
        tbLaporanTunggakan = new javax.swing.JTable();
        btnCetakLaporanTunggakan = new javax.swing.JButton();
        lbDateTimeRealTime = new javax.swing.JLabel();

        mnUbah.setText("Ubah");
        mnUbah.setToolTipText("");
        mnUbah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnUbahActionPerformed(evt);
            }
        });
        pmTbUser.add(mnUbah);

        mnHapus.setText("Hapus");
        mnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnHapusActionPerformed(evt);
            }
        });
        pmTbUser.add(mnHapus);

        updateDonatur.setText("Ubah");
        updateDonatur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateDonaturActionPerformed(evt);
            }
        });
        pmTbDonatur.add(updateDonatur);

        deleteDonatur.setText("Hapus");
        deleteDonatur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteDonaturActionPerformed(evt);
            }
        });
        pmTbDonatur.add(deleteDonatur);

        updateKamar.setText("Ubah");
        updateKamar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateKamarActionPerformed(evt);
            }
        });
        pmTbKamar.add(updateKamar);

        deleteKamar.setText("Hapus");
        deleteKamar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteKamarActionPerformed(evt);
            }
        });
        pmTbKamar.add(deleteKamar);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(250, 250, 250));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(204, 204, 204)));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setBackground(new java.awt.Color(204, 0, 0));
        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 0, 0));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("X");
        jLabel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1148, 1, 31, 39));

        lblMenu.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        lblMenu.setForeground(new java.awt.Color(1, 15, 30));
        lblMenu.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMenu.setText("Data Pengguna");
        jPanel2.add(lblMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(11, 1, 220, 39));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 0, 1180, 40));

        jPanel3.setBackground(new java.awt.Color(1, 15, 30));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        panelMenu.setBackground(new java.awt.Color(80, 80, 80));
        panelMenu.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 1, 1, 0, new java.awt.Color(102, 102, 102)));
        panelMenu.setLayout(new java.awt.CardLayout());

        panelMenuPenyewa.setBackground(new java.awt.Color(1, 15, 30));
        panelMenuPenyewa.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel32.setBackground(new java.awt.Color(35, 47, 60));
        jPanel32.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 0, 0, 0, new java.awt.Color(102, 102, 102)));
        jPanel32.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel40.setBackground(new java.awt.Color(255, 255, 255));
        jLabel40.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel40.setForeground(new java.awt.Color(255, 255, 255));
        jLabel40.setText("Beranda");
        jPanel32.add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 150, 20));

        panelMenuPenyewa.add(jPanel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 190, 20));

        btnMenuDashboardPenyewa.setBackground(new java.awt.Color(2, 44, 90));
        btnMenuDashboardPenyewa.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        btnMenuDashboardPenyewa.setForeground(new java.awt.Color(255, 255, 255));
        btnMenuDashboardPenyewa.setText("Beranda");
        btnMenuDashboardPenyewa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        btnMenuDashboardPenyewa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuDashboardPenyewaActionPerformed(evt);
            }
        });
        panelMenuPenyewa.add(btnMenuDashboardPenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 120, 190, 35));

        jPanel33.setBackground(new java.awt.Color(35, 47, 60));
        jPanel33.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel41.setBackground(new java.awt.Color(255, 255, 255));
        jLabel41.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel41.setForeground(new java.awt.Color(255, 255, 255));
        jLabel41.setText("Laporan");
        jPanel33.add(jLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 160, 20));

        panelMenuPenyewa.add(jPanel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 156, 190, 20));

        btnMenuLaporanPenyewa.setBackground(new java.awt.Color(1, 15, 30));
        btnMenuLaporanPenyewa.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        btnMenuLaporanPenyewa.setForeground(new java.awt.Color(255, 255, 255));
        btnMenuLaporanPenyewa.setText("Laporan Transaksi");
        btnMenuLaporanPenyewa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        btnMenuLaporanPenyewa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuLaporanPenyewaActionPerformed(evt);
            }
        });
        panelMenuPenyewa.add(btnMenuLaporanPenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 176, 190, 35));

        jLabel61.setForeground(new java.awt.Color(255, 255, 255));
        jLabel61.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel61.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel61MouseClicked(evt);
            }
        });
        panelMenuPenyewa.add(jLabel61, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 640, 190, 60));

        lbFullNameUser.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        lbFullNameUser.setForeground(new java.awt.Color(255, 255, 255));
        lbFullNameUser.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbFullNameUser.setText("Wahyu Imam");
        lbFullNameUser.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbFullNameUser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbFullNameUserMouseClicked(evt);
            }
        });
        panelMenuPenyewa.add(lbFullNameUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 700, 190, 30));

        jLabel62.setFont(new java.awt.Font("DialogInput", 1, 24)); // NOI18N
        jLabel62.setForeground(new java.awt.Color(255, 255, 255));
        jLabel62.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel62.setText("M E N U");
        jLabel62.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        panelMenuPenyewa.add(jLabel62, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 170, 40));

        lbFotoMenuPenyewa.setText("jLabel17");
        panelMenuPenyewa.add(lbFotoMenuPenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 590, 130, 110));

        panelMenu.add(panelMenuPenyewa, "card3");

        panelMenuBendahara.setBackground(new java.awt.Color(1, 15, 30));
        panelMenuBendahara.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnMenuDashboard.setBackground(new java.awt.Color(2, 44, 90));
        btnMenuDashboard.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        btnMenuDashboard.setForeground(new java.awt.Color(255, 255, 255));
        btnMenuDashboard.setText("Beranda");
        btnMenuDashboard.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        btnMenuDashboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuDashboardActionPerformed(evt);
            }
        });
        panelMenuBendahara.add(btnMenuDashboard, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 120, 190, 35));

        btnMenuTransaksiMasuk.setBackground(new java.awt.Color(1, 15, 30));
        btnMenuTransaksiMasuk.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        btnMenuTransaksiMasuk.setForeground(new java.awt.Color(255, 255, 255));
        btnMenuTransaksiMasuk.setText("Transaksi Masuk");
        btnMenuTransaksiMasuk.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        btnMenuTransaksiMasuk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuTransaksiMasukActionPerformed(evt);
            }
        });
        panelMenuBendahara.add(btnMenuTransaksiMasuk, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 296, 190, 35));

        btnMenuTransaksiKeluar.setBackground(new java.awt.Color(1, 15, 30));
        btnMenuTransaksiKeluar.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        btnMenuTransaksiKeluar.setForeground(new java.awt.Color(255, 255, 255));
        btnMenuTransaksiKeluar.setText("Transaksi Keluar");
        btnMenuTransaksiKeluar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        btnMenuTransaksiKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuTransaksiKeluarActionPerformed(evt);
            }
        });
        panelMenuBendahara.add(btnMenuTransaksiKeluar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 329, 190, 35));

        btnMenuLaporan.setBackground(new java.awt.Color(1, 15, 30));
        btnMenuLaporan.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        btnMenuLaporan.setForeground(new java.awt.Color(255, 255, 255));
        btnMenuLaporan.setText("Laporan Transaksi");
        btnMenuLaporan.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        btnMenuLaporan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuLaporanActionPerformed(evt);
            }
        });
        panelMenuBendahara.add(btnMenuLaporan, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 385, 190, 35));

        lbFullName.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        lbFullName.setForeground(new java.awt.Color(255, 255, 255));
        lbFullName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbFullName.setText("jLabel3");
        lbFullName.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbFullName.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbFullNameMouseClicked(evt);
            }
        });
        panelMenuBendahara.add(lbFullName, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 700, 190, 30));

        btnMenuKamar.setBackground(new java.awt.Color(1, 15, 30));
        btnMenuKamar.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        btnMenuKamar.setForeground(new java.awt.Color(255, 255, 255));
        btnMenuKamar.setText("Kamar");
        btnMenuKamar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        btnMenuKamar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuKamarActionPerformed(evt);
            }
        });
        panelMenuBendahara.add(btnMenuKamar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 174, 190, 35));

        btnMenuDonatur.setBackground(new java.awt.Color(1, 15, 30));
        btnMenuDonatur.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        btnMenuDonatur.setForeground(new java.awt.Color(255, 255, 255));
        btnMenuDonatur.setText("Donatur");
        btnMenuDonatur.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        btnMenuDonatur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuDonaturActionPerformed(evt);
            }
        });
        panelMenuBendahara.add(btnMenuDonatur, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 240, 190, 35));

        btnMenuPenyewa.setBackground(new java.awt.Color(1, 15, 30));
        btnMenuPenyewa.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        btnMenuPenyewa.setForeground(new java.awt.Color(255, 255, 255));
        btnMenuPenyewa.setText("Anggota");
        btnMenuPenyewa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        btnMenuPenyewa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuPenyewaActionPerformed(evt);
            }
        });
        panelMenuBendahara.add(btnMenuPenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 207, 190, 35));

        jPanel20.setBackground(new java.awt.Color(35, 47, 60));
        jPanel20.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 0, 0, 0, new java.awt.Color(102, 102, 102)));
        jPanel20.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel8.setBackground(new java.awt.Color(255, 255, 255));
        jLabel8.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Beranda");
        jPanel20.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 150, 20));

        panelMenuBendahara.add(jPanel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 190, 20));

        jPanel21.setBackground(new java.awt.Color(35, 47, 60));
        jPanel21.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel19.setBackground(new java.awt.Color(255, 255, 255));
        jLabel19.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Master");
        jPanel21.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 150, 20));

        panelMenuBendahara.add(jPanel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 154, 190, 20));

        jPanel22.setBackground(new java.awt.Color(35, 47, 60));
        jPanel22.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel22.setBackground(new java.awt.Color(255, 255, 255));
        jLabel22.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("Laporan");
        jPanel22.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 160, 20));

        panelMenuBendahara.add(jPanel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 365, 190, 20));

        jPanel23.setBackground(new java.awt.Color(35, 47, 60));
        jPanel23.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel23.setBackground(new java.awt.Color(255, 255, 255));
        jLabel23.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setText("Transaksi");
        jPanel23.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 160, 20));

        panelMenuBendahara.add(jPanel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 276, 190, 20));

        jLabel36.setForeground(new java.awt.Color(255, 255, 255));
        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel36.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel36MouseClicked(evt);
            }
        });
        panelMenuBendahara.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 640, 190, 60));

        jLabel15.setFont(new java.awt.Font("DialogInput", 1, 24)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("M E N U");
        jLabel15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        panelMenuBendahara.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 170, 40));

        btnMenuLaporanTunggakan.setBackground(new java.awt.Color(1, 15, 30));
        btnMenuLaporanTunggakan.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        btnMenuLaporanTunggakan.setForeground(new java.awt.Color(255, 255, 255));
        btnMenuLaporanTunggakan.setText("Laporan Tunggakan");
        btnMenuLaporanTunggakan.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        btnMenuLaporanTunggakan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuLaporanTunggakanActionPerformed(evt);
            }
        });
        panelMenuBendahara.add(btnMenuLaporanTunggakan, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 420, 190, 35));

        panelMenu.add(panelMenuBendahara, "card2");

        jPanel3.add(panelMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 190, 840));

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 191, 780));

        panelMainBorder.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 1, new java.awt.Color(102, 102, 102)));
        panelMainBorder.setLayout(new java.awt.CardLayout());

        panelBerandaPenyewa.setBackground(new java.awt.Color(255, 255, 255));
        panelBerandaPenyewa.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel34.setBackground(new java.awt.Color(255, 255, 255));
        jPanel34.setBorder(new javax.swing.border.MatteBorder(null));
        jPanel34.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel42.setFont(new java.awt.Font("Comic Sans MS", 0, 24)); // NOI18N
        jLabel42.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel42.setText("ASRAMA TENGUYUN PUTRA YOGYAKARTA");
        jPanel34.add(jLabel42, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1140, 33));

        jLabel43.setBackground(new java.awt.Color(255, 255, 255));
        jLabel43.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel43.setText("Telp. 088980006642");
        jPanel34.add(jLabel43, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 1140, -1));

        jLabel44.setBackground(new java.awt.Color(255, 255, 255));
        jLabel44.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel44.setText("Jl. Glagahsari/gg.manggis/UH04-354");
        jPanel34.add(jLabel44, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 1140, -1));

        javax.swing.GroupLayout jPanel35Layout = new javax.swing.GroupLayout(jPanel35);
        jPanel35.setLayout(jPanel35Layout);
        jPanel35Layout.setHorizontalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel35Layout.setVerticalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel34.add(jPanel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(-210, 80, -1, 20));

        panelBerandaPenyewa.add(jPanel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 1140, 90));

        jPanel36.setBackground(new java.awt.Color(255, 255, 255));
        jPanel36.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 0)));
        jPanel36.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbTglMasuk.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        lbTglMasuk.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTglMasuk.setText("1 Desember 2020");
        jPanel36.add(lbTglMasuk, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 230, 30));

        jPanel37.setBackground(new java.awt.Color(92, 78, 9));
        jPanel37.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel46.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel46.setForeground(new java.awt.Color(255, 255, 255));
        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel46.setText("Tanggal Masuk");
        jLabel46.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(255, 255, 255)));
        jPanel37.add(jLabel46, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        jPanel36.add(jPanel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        panelBerandaPenyewa.add(jPanel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 140, 230, 90));

        jPanel40.setBackground(new java.awt.Color(255, 255, 255));
        jPanel40.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 0)));
        jPanel40.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbTotalTransaction.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        lbTotalTransaction.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTotalTransaction.setText("Rp. 2300000");
        jPanel40.add(lbTotalTransaction, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 230, 30));

        jPanel41.setBackground(new java.awt.Color(167, 80, 23));
        jPanel41.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel50.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel50.setForeground(new java.awt.Color(255, 255, 255));
        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel50.setText("Total Pembayaran");
        jLabel50.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(255, 255, 255)));
        jPanel41.add(jLabel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        jPanel40.add(jPanel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        panelBerandaPenyewa.add(jPanel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 140, 230, 90));

        jPanel42.setBackground(new java.awt.Color(255, 255, 255));
        jPanel42.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 0)));
        jPanel42.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbDueDate.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        lbDueDate.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbDueDate.setText("1 Januari 2021");
        jPanel42.add(lbDueDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 230, 30));

        jPanel43.setBackground(new java.awt.Color(240, 67, 61));
        jPanel43.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel52.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel52.setForeground(new java.awt.Color(255, 255, 255));
        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel52.setText("Tanggal Jatuh Tempo");
        jLabel52.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(255, 255, 255)));
        jPanel43.add(jLabel52, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        jPanel42.add(jPanel43, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        panelBerandaPenyewa.add(jPanel42, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 140, 230, 90));

        jPanel44.setBackground(new java.awt.Color(255, 255, 255));
        jPanel44.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 0)));
        jPanel44.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbTagihanBulanan.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        lbTagihanBulanan.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTagihanBulanan.setText("Rp. 500000");
        jPanel44.add(lbTagihanBulanan, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 230, 30));

        jPanel45.setBackground(new java.awt.Color(0, 102, 102));
        jPanel45.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel54.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel54.setForeground(new java.awt.Color(255, 255, 255));
        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel54.setText("Tagihan Per Bulan");
        jLabel54.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(255, 255, 255)));
        jPanel45.add(jLabel54, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        jPanel44.add(jPanel45, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        panelBerandaPenyewa.add(jPanel44, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 140, 230, 90));

        jPanel48.setBackground(new java.awt.Color(86, 92, 100));
        jPanel48.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel48.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel38.setBackground(new java.awt.Color(255, 255, 255));
        jPanel38.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel47.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel47.setText("Kata Sandi :");
        jPanel38.add(jLabel47, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 130, 110, -1));

        jLabel48.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel48.setText("Nama Lengkap :");
        jPanel38.add(jLabel48, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 50, 110, -1));

        jLabel55.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel55.setText("NIK :");
        jPanel38.add(jLabel55, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 90, 110, -1));

        jLabel56.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel56.setText("Gender :");
        jPanel38.add(jLabel56, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 130, 110, -1));

        jLabel57.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel57.setText("Tanggal Masuk :");
        jPanel38.add(jLabel57, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, 110, -1));

        jLabel58.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel58.setText("Alamat :");
        jPanel38.add(jLabel58, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 210, 110, -1));

        jLabel59.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel59.setText("No. HP :");
        jPanel38.add(jLabel59, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 50, 110, -1));

        jLabel60.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel60.setText("Nama Pengguna :");
        jPanel38.add(jLabel60, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 90, 110, 20));

        txtTglMasukRenter.setEditable(false);
        txtTglMasukRenter.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel38.add(txtTglMasukRenter, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 170, 280, 30));

        txtUsername.setEditable(false);
        txtUsername.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel38.add(txtUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 90, 280, 30));

        txtNik.setEditable(false);
        txtNik.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel38.add(txtNik, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 90, 280, 30));

        txtgender.setEditable(false);
        txtgender.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel38.add(txtgender, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 130, 280, 30));

        txtAlamat.setEditable(false);
        txtAlamat.setColumns(20);
        txtAlamat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtAlamat.setRows(5);
        txtAlamat.setFocusTraversalPolicyProvider(true);
        jScrollPane15.setViewportView(txtAlamat);

        jPanel38.add(jScrollPane15, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 210, 280, -1));

        txtFullName.setEditable(false);
        txtFullName.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel38.add(txtFullName, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 50, 280, 30));

        txtPhoneNumber.setEditable(false);
        txtPhoneNumber.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel38.add(txtPhoneNumber, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 50, 280, 30));

        txtPasswordRenter.setEditable(false);
        txtPasswordRenter.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtPasswordRenter.setText("jPasswordField1");
        jPanel38.add(txtPasswordRenter, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 130, 280, 30));

        jPanel48.add(jPanel38, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 940, 340));

        panelBerandaPenyewa.add(jPanel48, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 270, 960, 360));

        panelMainBorder.add(panelBerandaPenyewa, "card9");

        panelBeranda.setBackground(new java.awt.Color(255, 255, 255));
        panelBeranda.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(102, 102, 102)));
        jPanel8.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel12.setFont(new java.awt.Font("Comic Sans MS", 0, 24)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("ASRAMA TENGUYUN PUTRA YOGYAKARTA");
        jPanel8.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1140, 33));

        jLabel13.setBackground(new java.awt.Color(255, 255, 255));
        jLabel13.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Telp. 088980006642");
        jPanel8.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 1140, -1));

        jLabel14.setBackground(new java.awt.Color(255, 255, 255));
        jLabel14.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("Jl. Glagahsari/gg.manggis/UH04-354");
        jPanel8.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 1140, -1));

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel8.add(jPanel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(-210, 80, -1, 20));

        panelBeranda.add(jPanel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 1140, 90));

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 0)));
        jPanel9.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbJmlKamarInBerandaAdmin.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        lbJmlKamarInBerandaAdmin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbJmlKamarInBerandaAdmin.setText("100 Kamar");
        jPanel9.add(lbJmlKamarInBerandaAdmin, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 220, 30));

        jPanel13.setBackground(new java.awt.Color(92, 78, 9));
        jPanel13.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setText("Jumlah Kamar");
        jLabel16.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(255, 255, 255)));
        jPanel13.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        jPanel9.add(jPanel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        panelBeranda.add(jPanel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 130, 230, 90));

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 0)));
        jPanel14.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbSisaSaldoInBerandaAdmin.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        lbSisaSaldoInBerandaAdmin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbSisaSaldoInBerandaAdmin.setText("Rp. 50.0000");
        jPanel14.add(lbSisaSaldoInBerandaAdmin, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 220, 30));

        jPanel15.setBackground(new java.awt.Color(81, 88, 123));
        jPanel15.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 255, 255));
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel25.setText("Sisa Saldo");
        jLabel25.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(255, 255, 255)));
        jPanel15.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        jPanel14.add(jPanel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        panelBeranda.add(jPanel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 240, 230, 90));

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 0)));
        jPanel16.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbUangMasukTodayInBerandaAdmin.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        lbUangMasukTodayInBerandaAdmin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbUangMasukTodayInBerandaAdmin.setText("Rp. 100.0000");
        jPanel16.add(lbUangMasukTodayInBerandaAdmin, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 230, 30));

        jPanel17.setBackground(new java.awt.Color(167, 80, 23));
        jPanel17.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel27.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(255, 255, 255));
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel27.setText("Uang Masuk Hari Ini");
        jLabel27.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(255, 255, 255)));
        jPanel17.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        jPanel16.add(jPanel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        panelBeranda.add(jPanel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 240, 230, 90));

        jPanel18.setBackground(new java.awt.Color(255, 255, 255));
        jPanel18.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0, 102, 0)));
        jPanel18.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbJmlUangKelarTodayInBerandaAdmin.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        lbJmlUangKelarTodayInBerandaAdmin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbJmlUangKelarTodayInBerandaAdmin.setText("Rp. 400.000");
        jPanel18.add(lbJmlUangKelarTodayInBerandaAdmin, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 230, 30));

        jPanel19.setBackground(new java.awt.Color(240, 67, 61));
        jPanel19.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel29.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(255, 255, 255));
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel29.setText("Uang Keluar Hari Ini");
        jLabel29.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(255, 255, 255)));
        jPanel19.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 240, 40));

        jPanel18.add(jPanel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        panelBeranda.add(jPanel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 240, 230, 90));

        jPanel27.setBackground(new java.awt.Color(255, 255, 255));
        jPanel27.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 0)));
        jPanel27.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbJmlPenyewaInBerandaAdmin.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        lbJmlPenyewaInBerandaAdmin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbJmlPenyewaInBerandaAdmin.setText("50 Anggota");
        jPanel27.add(lbJmlPenyewaInBerandaAdmin, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 220, 30));

        jPanel28.setBackground(new java.awt.Color(0, 102, 102));
        jPanel28.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel37.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel37.setForeground(new java.awt.Color(255, 255, 255));
        jLabel37.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel37.setText("Jumlah Anggota");
        jLabel37.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(255, 255, 255)));
        jPanel28.add(jLabel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        jPanel27.add(jPanel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        panelBeranda.add(jPanel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 130, 230, 90));

        jPanel29.setBackground(new java.awt.Color(255, 255, 255));
        jPanel29.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 0)));
        jPanel29.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbJmlDonaturInBerandaAdmin.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        lbJmlDonaturInBerandaAdmin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbJmlDonaturInBerandaAdmin.setText("50 Donatur");
        jPanel29.add(lbJmlDonaturInBerandaAdmin, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 230, 30));

        jPanel30.setBackground(new java.awt.Color(102, 0, 102));
        jPanel30.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel39.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel39.setForeground(new java.awt.Color(255, 255, 255));
        jLabel39.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel39.setText("Jumlah Donatur");
        jLabel39.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(255, 255, 255)));
        jPanel30.add(jLabel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        jPanel29.add(jPanel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 230, 40));

        panelBeranda.add(jPanel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 130, 230, 90));

        jPanel31.setBackground(new java.awt.Color(86, 92, 100));
        jPanel31.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel31.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabTableHomePageAdmin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabTableHomePageAdminMouseClicked(evt);
            }
        });

        spTbDaftarjatuhTempo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                spTbDaftarjatuhTempoKeyPressed(evt);
            }
        });

        tbBerandaDaftarJatuhTempo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "No.", "Tanggal Jatuh Tempo", "Nama Lengkap", "Nama / No Kamar", "Jumlah Tagihan"
            }
        ));
        spTbDaftarjatuhTempo.setViewportView(tbBerandaDaftarJatuhTempo);

        tabTableHomePageAdmin.addTab("Daftar Jatuh Tempo", spTbDaftarjatuhTempo);

        spTbDaftarKeterlambatan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                spTbDaftarKeterlambatanKeyPressed(evt);
            }
        });

        tbBerandaDaftarKeterlambatan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "No.", "Tanggal Jatuh Tempo", "Nama Lengkap", "Nama / No Kamar", "Jumlah Tagihan"
            }
        ));
        spTbDaftarKeterlambatan.setViewportView(tbBerandaDaftarKeterlambatan);

        tabTableHomePageAdmin.addTab("Daftar Keterlambatan", spTbDaftarKeterlambatan);

        spTbDaftarKamarKosong.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                spTbDaftarKamarKosongKeyPressed(evt);
            }
        });

        tbBerandaDaftarKamarKosong.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "No.", "Nama / No Kamar", "Kapasitas (Orang)", "Terisi (Orang)", "Tersedia (Orang)"
            }
        ));
        spTbDaftarKamarKosong.setViewportView(tbBerandaDaftarKamarKosong);

        tabTableHomePageAdmin.addTab("Daftar Kamar Kosong", spTbDaftarKamarKosong);

        jPanel31.add(tabTableHomePageAdmin, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 1120, 300));

        panelBeranda.add(jPanel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 350, 1140, 320));

        panelMainBorder.add(panelBeranda, "card2");

        panelDonatur.setBackground(new java.awt.Color(255, 255, 255));
        panelDonatur.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setText("NIK :");
        jPanel4.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, 50, -1));

        txtNikDonatur.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel4.add(txtNikDonatur, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, 210, 30));

        jLabel4.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel4.setText("Nama Lengkap :");
        jPanel4.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, -1));

        txtNamaLengkapDonatur.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel4.add(txtNamaLengkapDonatur, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, 210, 30));

        jLabel5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel5.setText(" Gender :");
        jPanel4.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, -1, -1));

        rbPriaDonatur.setBackground(new java.awt.Color(255, 255, 255));
        genderGrup.add(rbPriaDonatur);
        rbPriaDonatur.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbPriaDonatur.setText("Laki - Laki");
        rbPriaDonatur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbPriaDonaturActionPerformed(evt);
            }
        });
        jPanel4.add(rbPriaDonatur, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, -1, -1));

        rbWanitaDonatur.setBackground(new java.awt.Color(255, 255, 255));
        genderGrup.add(rbWanitaDonatur);
        rbWanitaDonatur.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbWanitaDonatur.setText("Perempuan");
        jPanel4.add(rbWanitaDonatur, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 170, -1, -1));

        jLabel6.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel6.setText("Alamat :");
        jPanel4.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 200, -1, -1));

        txtAlamatDonatur.setColumns(20);
        txtAlamatDonatur.setLineWrap(true);
        txtAlamatDonatur.setRows(5);
        txtAlamatDonatur.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jScrollPane1.setViewportView(txtAlamatDonatur);

        jPanel4.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 220, 210, 100));

        jLabel7.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel7.setText("No Hp :");
        jPanel4.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 340, -1, -1));

        txtNoHpDonatur.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel4.add(txtNoHpDonatur, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 360, 210, 30));

        btnSimpanDonatur.setBackground(new java.awt.Color(0, 78, 46));
        btnSimpanDonatur.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSimpanDonatur.setForeground(new java.awt.Color(255, 255, 255));
        btnSimpanDonatur.setText("Simpan");
        btnSimpanDonatur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanDonaturActionPerformed(evt);
            }
        });
        jPanel4.add(btnSimpanDonatur, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 430, 100, 30));

        btnBersihkanDonatur.setBackground(new java.awt.Color(255, 102, 0));
        btnBersihkanDonatur.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnBersihkanDonatur.setForeground(new java.awt.Color(255, 255, 255));
        btnBersihkanDonatur.setText("Bersihkan");
        btnBersihkanDonatur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBersihkanDonaturActionPerformed(evt);
            }
        });
        jPanel4.add(btnBersihkanDonatur, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 430, 100, 30));

        panelDonatur.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 270, 480));

        jPanel5.setBackground(new java.awt.Color(86, 92, 100));
        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane2.setBackground(new java.awt.Color(255, 255, 255));

        tbDonatur.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        tbDonatur.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "No.", "NIK", "Nama", "Gender", "Alamat", "No.Hp"
            }
        ));
        tbDonatur.setComponentPopupMenu(pmTbDonatur);
        tbDonatur.setGridColor(new java.awt.Color(204, 204, 204));
        jScrollPane2.setViewportView(tbDonatur);

        jPanel5.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 830, 640));

        panelDonatur.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 20, 850, 660));

        panelMainBorder.add(panelDonatur, "card3");

        panelTransaksiMasuk.setBackground(new java.awt.Color(255, 255, 255));
        panelTransaksiMasuk.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        panel_show_transaksi_masuk.setBackground(new java.awt.Color(255, 255, 255));
        panel_show_transaksi_masuk.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel6.setBackground(new java.awt.Color(86, 92, 100));
        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabTableIncomingTransaction.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabTableIncomingTransactionMouseClicked(evt);
            }
        });

        tbTransaksiPenyewa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No.", "Tanggal", "Nama Lengkap", "Nama / No Kamar", "Jangka Waktu (Bulan)", "Total", "Keterangan", "Tanggal Jatuh Tempo"
            }
        ));
        jScrollPane8.setViewportView(tbTransaksiPenyewa);

        tabTableIncomingTransaction.addTab("Anggota", jScrollPane8);

        tbTransaksiDonatur.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "No.", "Tanggal", "Nama Lengkap", "Jumlah Uang Masuk", "Keterangan"
            }
        ));
        jScrollPane9.setViewportView(tbTransaksiDonatur);

        tabTableIncomingTransaction.addTab("Donatur", jScrollPane9);

        tbTransaksiLainnya.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "No.", "Tanggal", "Dari", "Nama Lengkap", "Jumlah Uang Masuk", "Keterangan"
            }
        ));
        jScrollPane10.setViewportView(tbTransaksiLainnya);

        tabTableIncomingTransaction.addTab("Lainnya", jScrollPane10);

        jPanel6.add(tabTableIncomingTransaction, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 1140, 560));

        jLabel35.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(255, 255, 255));
        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel35.setText("Sisa Saldo :");
        jPanel6.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 580, 120, 20));

        lbSisaSaldo.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbSisaSaldo.setForeground(new java.awt.Color(255, 255, 255));
        lbSisaSaldo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbSisaSaldo.setText("Rp. 500000");
        jPanel6.add(lbSisaSaldo, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 580, 160, 20));

        jLabel63.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel63.setForeground(new java.awt.Color(255, 255, 255));
        jLabel63.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel63.setText("Total :");
        jPanel6.add(jLabel63, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 580, 120, 20));

        lblTotalTransaksi.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblTotalTransaksi.setForeground(new java.awt.Color(255, 255, 255));
        lblTotalTransaksi.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotalTransaksi.setText("Rp. 500000");
        jPanel6.add(lblTotalTransaksi, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 580, 160, 20));

        panel_show_transaksi_masuk.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 1160, 610));

        btnTambahTransaksiMasuk.setBackground(new java.awt.Color(24, 81, 109));
        btnTambahTransaksiMasuk.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnTambahTransaksiMasuk.setForeground(new java.awt.Color(255, 255, 255));
        btnTambahTransaksiMasuk.setText("Tambah");
        btnTambahTransaksiMasuk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahTransaksiMasukActionPerformed(evt);
            }
        });
        panel_show_transaksi_masuk.add(btnTambahTransaksiMasuk, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 130, 30));
        panel_show_transaksi_masuk.add(txtSearchIncomingTransaction, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 20, 220, 30));

        jButton1.setBackground(new java.awt.Color(255, 102, 0));
        jButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("Cari");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        panel_show_transaksi_masuk.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1110, 20, 60, 30));

        lblCariBendahara.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblCariBendahara.setText("Nama Anggota :");
        panel_show_transaksi_masuk.add(lblCariBendahara, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 20, 90, 30));

        panelTransaksiMasuk.add(panel_show_transaksi_masuk, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1180, 810));

        panelMainBorder.add(panelTransaksiMasuk, "card4");

        panelTransaksiKeluar.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        panel_show_transaksi_keluar.setBackground(new java.awt.Color(255, 255, 255));
        panel_show_transaksi_keluar.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel7.setBackground(new java.awt.Color(86, 92, 100));
        jPanel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel7.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tbTransaksiKeluar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "No.", "Tanggal", "Jumlah Uang Keluar", "Keterangan"
            }
        ));
        jScrollPane3.setViewportView(tbTransaksiKeluar);

        jPanel7.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 1140, 560));

        jLabel34.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel34.setForeground(new java.awt.Color(255, 255, 255));
        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel34.setText("Sisa Saldo :");
        jPanel7.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 580, 120, 20));

        lbSisaSaldoInTransKeluar.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbSisaSaldoInTransKeluar.setForeground(new java.awt.Color(255, 255, 255));
        lbSisaSaldoInTransKeluar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbSisaSaldoInTransKeluar.setText("Rp. 500000");
        jPanel7.add(lbSisaSaldoInTransKeluar, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 580, 160, 20));

        lblTotalTransaksiKeluar.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblTotalTransaksiKeluar.setForeground(new java.awt.Color(255, 255, 255));
        lblTotalTransaksiKeluar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotalTransaksiKeluar.setText("Rp. 500000");
        jPanel7.add(lblTotalTransaksiKeluar, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 580, 160, 20));

        jLabel64.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel64.setForeground(new java.awt.Color(255, 255, 255));
        jLabel64.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel64.setText("Total :");
        jPanel7.add(jLabel64, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 580, 120, 20));

        panel_show_transaksi_keluar.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 1160, 610));

        btnTambahTransaksiKeluar.setBackground(new java.awt.Color(24, 81, 109));
        btnTambahTransaksiKeluar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnTambahTransaksiKeluar.setForeground(new java.awt.Color(255, 255, 255));
        btnTambahTransaksiKeluar.setText("Tambah");
        btnTambahTransaksiKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahTransaksiKeluarActionPerformed(evt);
            }
        });
        panel_show_transaksi_keluar.add(btnTambahTransaksiKeluar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 130, 30));
        panel_show_transaksi_keluar.add(txtSearchOutgoingTransaction, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 20, 220, 30));

        btnSearchOutgoingTransaction.setBackground(new java.awt.Color(255, 102, 0));
        btnSearchOutgoingTransaction.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSearchOutgoingTransaction.setForeground(new java.awt.Color(255, 255, 255));
        btnSearchOutgoingTransaction.setText("Cari");
        btnSearchOutgoingTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchOutgoingTransactionActionPerformed(evt);
            }
        });
        panel_show_transaksi_keluar.add(btnSearchOutgoingTransaction, new org.netbeans.lib.awtextra.AbsoluteConstraints(1110, 20, 60, 30));

        panelTransaksiKeluar.add(panel_show_transaksi_keluar, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1180, 810));

        panelMainBorder.add(panelTransaksiKeluar, "card5");

        panelKamar.setBackground(new java.awt.Color(255, 255, 255));
        panelKamar.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel11.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel18.setText("Nama / No Kamar :");
        jPanel11.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        txtNoKamar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtNoKamar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNoKamarActionPerformed(evt);
            }
        });
        jPanel11.add(txtNoKamar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 260, 30));

        txtKapasitas.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel11.add(txtKapasitas, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 100, 30));

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel21.setText("Kapasitas (Orang) :");
        jPanel11.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, -1, -1));

        btnBersihkanKamar.setBackground(new java.awt.Color(255, 102, 0));
        btnBersihkanKamar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnBersihkanKamar.setForeground(new java.awt.Color(255, 255, 255));
        btnBersihkanKamar.setText("Bersihkan");
        btnBersihkanKamar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBersihkanKamarActionPerformed(evt);
            }
        });
        jPanel11.add(btnBersihkanKamar, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 160, 100, 30));

        btnSimpanKamar.setBackground(new java.awt.Color(0, 78, 46));
        btnSimpanKamar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSimpanKamar.setForeground(new java.awt.Color(255, 255, 255));
        btnSimpanKamar.setText("Simpan");
        btnSimpanKamar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanKamarActionPerformed(evt);
            }
        });
        jPanel11.add(btnSimpanKamar, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 160, 100, 30));

        panelKamar.add(jPanel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 290, 210));

        jPanel12.setBackground(new java.awt.Color(86, 92, 100));
        jPanel12.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tbKamar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "No.", "Nama / No Kamar", "Kapasitas", "Status", "Tersedia (Orang)"
            }
        ));
        tbKamar.setComponentPopupMenu(pmTbKamar);
        jScrollPane5.setViewportView(tbKamar);

        jPanel12.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 800, 640));

        panelKamar.add(jPanel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 20, 820, 660));

        panelMainBorder.add(panelKamar, "card6");

        panelPenyewa.setBackground(new java.awt.Color(255, 255, 255));
        panelPenyewa.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel24.setBackground(new java.awt.Color(255, 255, 255));
        jPanel24.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel24.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel9.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel9.setText("NIK :");
        jPanel24.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 10, 50, -1));

        txtNikPenyewa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel24.add(txtNikPenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 30, 190, 30));

        jLabel10.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel10.setText("Nama Lengkap :");
        jPanel24.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        txtNamaLengkapPenyewa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel24.add(txtNamaLengkapPenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 170, 30));

        jLabel30.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel30.setText(" Gender :");
        jPanel24.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, -1, -1));

        rbPriaPenyewa.setBackground(new java.awt.Color(255, 255, 255));
        genderGrup.add(rbPriaPenyewa);
        rbPriaPenyewa.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbPriaPenyewa.setText("Laki - Laki");
        rbPriaPenyewa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbPriaPenyewaActionPerformed(evt);
            }
        });
        jPanel24.add(rbPriaPenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, -1, -1));

        rbWanitaPenyewa.setBackground(new java.awt.Color(255, 255, 255));
        genderGrup.add(rbWanitaPenyewa);
        rbWanitaPenyewa.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbWanitaPenyewa.setText("Perempuan");
        jPanel24.add(rbWanitaPenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 90, -1, -1));

        jLabel31.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel31.setText("Tanggal Masuk :");
        jPanel24.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, -1, -1));

        txtAlamatPenyewa.setColumns(20);
        txtAlamatPenyewa.setLineWrap(true);
        txtAlamatPenyewa.setRows(5);
        txtAlamatPenyewa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jScrollPane6.setViewportView(txtAlamatPenyewa);

        jPanel24.add(jScrollPane6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, 370, 80));

        jLabel32.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel32.setText("No Hp :");
        jPanel24.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, -1, -1));

        txtNoHpPenyewa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel24.add(txtNoHpPenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 310, 170, 30));

        lblUsername1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblUsername1.setText("Unggah Foto :");
        jPanel24.add(lblUsername1, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 410, -1, -1));

        txtUsernamePenyewa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel24.add(txtUsernamePenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 370, 170, 30));

        lblPassword1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblPassword1.setText("Kata Sandi :");
        jPanel24.add(lblPassword1, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 350, -1, -1));

        txtPasswordPenyewa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel24.add(txtPasswordPenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 370, 190, 30));

        lblstatus1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblstatus1.setText("Status :");
        jPanel24.add(lblstatus1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 410, -1, -1));

        rbStatusUserAktifPenyewa.setBackground(new java.awt.Color(255, 255, 255));
        statusUserGrup.add(rbStatusUserAktifPenyewa);
        rbStatusUserAktifPenyewa.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbStatusUserAktifPenyewa.setText("AKTIF");
        jPanel24.add(rbStatusUserAktifPenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 430, -1, -1));

        rbStatusUserKeluarPenyewa.setBackground(new java.awt.Color(255, 255, 255));
        statusUserGrup.add(rbStatusUserKeluarPenyewa);
        rbStatusUserKeluarPenyewa.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbStatusUserKeluarPenyewa.setText("KELUAR");
        jPanel24.add(rbStatusUserKeluarPenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 430, -1, -1));

        btnSimpanPenyewa.setBackground(new java.awt.Color(0, 78, 46));
        btnSimpanPenyewa.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSimpanPenyewa.setForeground(new java.awt.Color(255, 255, 255));
        btnSimpanPenyewa.setText("Simpan");
        btnSimpanPenyewa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanPenyewaActionPerformed(evt);
            }
        });
        jPanel24.add(btnSimpanPenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 610, 100, 30));

        btnBersihkanPenyewa.setBackground(new java.awt.Color(255, 102, 0));
        btnBersihkanPenyewa.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnBersihkanPenyewa.setForeground(new java.awt.Color(255, 255, 255));
        btnBersihkanPenyewa.setText("Bersihkan");
        btnBersihkanPenyewa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBersihkanPenyewaActionPerformed(evt);
            }
        });
        jPanel24.add(btnBersihkanPenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 610, 100, 30));

        lblKamar1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblKamar1.setText("Kamar :");
        jPanel24.add(lblKamar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 460, -1, -1));

        cbKamarPenyewa.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cbKamarPenyewa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel24.add(cbKamarPenyewa, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 480, 170, 30));

        lblUsername2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblUsername2.setText("Jumlah Tagihan / Bulan :");
        jPanel24.add(lblUsername2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 520, -1, -1));

        txtJumlahTagihan.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel24.add(txtJumlahTagihan, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 540, 170, 30));

        jLabel33.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel33.setText("Alamat :");
        jPanel24.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 180, -1, -1));
        jPanel24.add(txtTglMasuk, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 210, 30));

        lblUsername3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblUsername3.setText("Nama Pengguna :");
        jPanel24.add(lblUsername3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 350, -1, -1));

        lbFoto.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel24.add(lbFoto, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 430, 120, 100));

        jButton2.setBackground(new java.awt.Color(204, 204, 204));
        jButton2.setForeground(new java.awt.Color(51, 51, 51));
        jButton2.setText("Pilih File");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel24.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 542, 120, 30));

        panelPenyewa.add(jPanel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 390, 650));

        jPanel25.setBackground(new java.awt.Color(86, 92, 100));
        jPanel25.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanel25.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane7.setBackground(new java.awt.Color(255, 255, 255));

        tbPenyewa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        tbPenyewa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No.", "NIK", "Nama Lengkap", "Gender", "Alamat", "No.Hp", "Nama Pengguna", "Kata Sandi", "Tanggal Masuk", "Aktif", "Nama / No Kamar", "Jumlah Tagihan / Bulan"
            }
        ));
        tbPenyewa.setComponentPopupMenu(pmTbUser);
        tbPenyewa.setGridColor(new java.awt.Color(204, 204, 204));
        jScrollPane7.setViewportView(tbPenyewa);

        jPanel25.add(jScrollPane7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 720, 640));

        panelPenyewa.add(jPanel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 20, 740, 660));

        panelMainBorder.add(panelPenyewa, "card7");

        panelLaporan.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        panel_show_laporan.setBackground(new java.awt.Color(255, 255, 255));
        panel_show_laporan.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel26.setBackground(new java.awt.Color(86, 92, 100));
        jPanel26.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel26.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        reportTableTab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                reportTableTabMouseClicked(evt);
            }
        });

        tbLaporanSemuaTransaksi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "No.", "Tanggal", "Jenis Transaksi", "Nama Lengkap", "Total", "Keterangan"
            }
        ));
        jScrollPane4.setViewportView(tbLaporanSemuaTransaksi);

        reportTableTab.addTab("Semua Transaksi", jScrollPane4);

        tbLaporanTransaksiMasuk.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No.", "Tanggal", "Dari", "Nama Lengkap", "Nama / No Kamar", "Jangka Waktu (bulan)", "Tanggal Jatuh Tempo", "Total", "Keterangan"
            }
        ));
        jScrollPane11.setViewportView(tbLaporanTransaksiMasuk);

        reportTableTab.addTab("Transaksi Masuk", jScrollPane11);

        tbLaporanTransaksiKeluar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "No.", "Tanggal", "Total", "Keterangan"
            }
        ));
        jScrollPane12.setViewportView(tbLaporanTransaksiKeluar);

        reportTableTab.addTab("Transaksi Keluar", jScrollPane12);

        jPanel26.add(reportTableTab, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 1140, 590));

        panel_show_laporan.add(jPanel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 1160, 610));

        btnCetakLaporan.setBackground(new java.awt.Color(255, 102, 0));
        btnCetakLaporan.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnCetakLaporan.setForeground(new java.awt.Color(255, 255, 255));
        btnCetakLaporan.setText("Tampil");
        btnCetakLaporan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCetakLaporanActionPerformed(evt);
            }
        });
        panel_show_laporan.add(btnCetakLaporan, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 30, 130, 30));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Dari :");
        panel_show_laporan.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 10, -1, -1));

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setText("Sampai :");
        panel_show_laporan.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 10, -1, -1));

        btnCetakLaporan1.setBackground(new java.awt.Color(3, 42, 83));
        btnCetakLaporan1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnCetakLaporan1.setForeground(new java.awt.Color(255, 255, 255));
        btnCetakLaporan1.setText("Cetak");
        btnCetakLaporan1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCetakLaporan1ActionPerformed(evt);
            }
        });
        panel_show_laporan.add(btnCetakLaporan1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 130, 30));
        panel_show_laporan.add(txtDateTo, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 30, 160, 30));
        panel_show_laporan.add(txtDateFrom, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 30, 160, 30));

        panelLaporan.add(panel_show_laporan, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1180, 810));

        panelMainBorder.add(panelLaporan, "card8");

        panelLaporanPenyewa.setBackground(new java.awt.Color(255, 255, 255));
        panelLaporanPenyewa.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel39.setBackground(new java.awt.Color(86, 92, 100));
        jPanel39.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel39.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tbLaporanPenyewa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "No.", "Tanggal", "Jangka Waktu (bulan)", "Tanggal Jatuh Tempo", "Subtotal"
            }
        ));
        jScrollPane16.setViewportView(tbLaporanPenyewa);

        jPanel39.add(jScrollPane16, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 1140, 590));

        panelLaporanPenyewa.add(jPanel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 1160, 610));

        btnCetakLaporan2.setBackground(new java.awt.Color(3, 42, 83));
        btnCetakLaporan2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnCetakLaporan2.setForeground(new java.awt.Color(255, 255, 255));
        btnCetakLaporan2.setText("Cetak");
        btnCetakLaporan2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCetakLaporan2ActionPerformed(evt);
            }
        });
        panelLaporanPenyewa.add(btnCetakLaporan2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 130, 30));

        jLabel65.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel65.setText("Dari :");
        panelLaporanPenyewa.add(jLabel65, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 10, -1, -1));

        jLabel66.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel66.setText("Sampai :");
        panelLaporanPenyewa.add(jLabel66, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 10, -1, -1));

        btnCetakLaporan3.setBackground(new java.awt.Color(255, 102, 0));
        btnCetakLaporan3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnCetakLaporan3.setForeground(new java.awt.Color(255, 255, 255));
        btnCetakLaporan3.setText("Tampil");
        btnCetakLaporan3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCetakLaporan3ActionPerformed(evt);
            }
        });
        panelLaporanPenyewa.add(btnCetakLaporan3, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 30, 130, 30));
        panelLaporanPenyewa.add(txtDateToUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 30, 170, 30));
        panelLaporanPenyewa.add(txtDateFromUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 30, 170, 30));

        panelMainBorder.add(panelLaporanPenyewa, "card10");

        panel_show_laporanTunggakan.setBackground(new java.awt.Color(255, 255, 255));
        panel_show_laporanTunggakan.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel47.setBackground(new java.awt.Color(86, 92, 100));
        jPanel47.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel47.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tbLaporanTunggakan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane18.setViewportView(tbLaporanTunggakan);

        jPanel47.add(jScrollPane18, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 17, 1140, 580));

        panel_show_laporanTunggakan.add(jPanel47, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 1160, 610));

        btnCetakLaporanTunggakan.setBackground(new java.awt.Color(3, 42, 83));
        btnCetakLaporanTunggakan.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnCetakLaporanTunggakan.setForeground(new java.awt.Color(255, 255, 255));
        btnCetakLaporanTunggakan.setText("Cetak");
        btnCetakLaporanTunggakan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCetakLaporanTunggakanActionPerformed(evt);
            }
        });
        panel_show_laporanTunggakan.add(btnCetakLaporanTunggakan, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 130, 30));

        javax.swing.GroupLayout panelLaporanTunggakanLayout = new javax.swing.GroupLayout(panelLaporanTunggakan);
        panelLaporanTunggakan.setLayout(panelLaporanTunggakanLayout);
        panelLaporanTunggakanLayout.setHorizontalGroup(
            panelLaporanTunggakanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1180, Short.MAX_VALUE)
            .addGroup(panelLaporanTunggakanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelLaporanTunggakanLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(panel_show_laporanTunggakan, javax.swing.GroupLayout.PREFERRED_SIZE, 1180, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        panelLaporanTunggakanLayout.setVerticalGroup(
            panelLaporanTunggakanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 810, Short.MAX_VALUE)
            .addGroup(panelLaporanTunggakanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelLaporanTunggakanLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(panel_show_laporanTunggakan, javax.swing.GroupLayout.PREFERRED_SIZE, 810, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        panelMainBorder.add(panelLaporanTunggakan, "card11");

        jPanel1.add(panelMainBorder, new org.netbeans.lib.awtextra.AbsoluteConstraints(192, 50, 1178, 690));

        lbDateTimeRealTime.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbDateTimeRealTime.setText("Rabu, 17 Januari 2022 14:45");
        jPanel1.add(lbDateTimeRealTime, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 740, 1160, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    
    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        // TODO add your handling code here:
        exit(0);
    }//GEN-LAST:event_jLabel1MouseClicked

    private void btnMenuDashboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuDashboardActionPerformed
        // TODO add your handling code here:
        showPanel(PANEL_MENU_BERANDA);
        setMenuBendahara();
    }//GEN-LAST:event_btnMenuDashboardActionPerformed

    private void rbPriaDonaturActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbPriaDonaturActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbPriaDonaturActionPerformed

    private void btnMenuDonaturActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuDonaturActionPerformed
        // TODO add your handling code here:
        showPanel(PANEL_MENU_DONATUR);
        showDonaturList();
    }//GEN-LAST:event_btnMenuDonaturActionPerformed

    private void btnSimpanDonaturActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanDonaturActionPerformed
        // TODO add your handling code here:
        Boolean numberOnly = RequestUtil.numberOnly(txtNikDonatur.getText());
        Logger.info("nik donatur is number : {}", numberOnly, Main.class);
        Boolean numberOnly2 = RequestUtil.numberOnly(txtNoHpDonatur.getText());
        Logger.info("phone number donatur is number : {}", numberOnly2, Main.class);
        Boolean alfabetOnly = RequestUtil.alfabetOnly(txtNamaLengkapDonatur.getText());
        Logger.info("donatur name is alfabet : {}", alfabetOnly, this.getClass());
        StoreDonaturRequest storeDonaturRequest = StoreDonaturRequest.builder()
                .nik(Boolean.TRUE.equals(numberOnly) ? txtNikDonatur.getText() : null)
                .namaLengkap(Boolean.TRUE.equals(alfabetOnly) ? txtNamaLengkapDonatur.getText() : null)
                .gender(RadioButtonUtil.getGender(rbPriaDonatur, rbWanitaDonatur))
                .alamat(txtAlamatDonatur.getText())
                .noHp(Boolean.TRUE.equals(numberOnly2) ? txtNoHpDonatur.getText() : null)
                .build();
        Response response;
        if (btnSimpanDonatur.getText().equals("Simpan")) {
            response = portalController.storeDonatur(storeDonaturRequest);
        } else {
            response = portalController.updateDonatur(storeDonaturRequest);
        }
        String message;
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            clearText(ButtonConstant.DONATUR);
            showDonaturList();
            message = "Berhasil simpan data donatur";
        } else {
            message = response.getResponseDto().getMessage();
        }
        JOptionPane.showMessageDialog(this, message);
    }//GEN-LAST:event_btnSimpanDonaturActionPerformed

    private void mnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnHapusActionPerformed
        // TODO add your handling code here:
        int selectRow = tbPenyewa.getSelectedRow();
        if (selectRow < 0) {
            JOptionPane.showMessageDialog(this, "Pilih data yang akan di hapus");
        } else {
            Response response = userController.deleteUserById(GetUserByNikRequest.builder()
                    .nik(tbPenyewa.getValueAt(selectRow, 1).toString())
                    .build());
            String message;
            if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
                message = "Berhasil hapus data anggota";
            } else {
                message = response.getResponseDto().getMessage();
            }
            JOptionPane.showMessageDialog(this, message);
            showUserList();
        }
    }//GEN-LAST:event_mnHapusActionPerformed

    private void mnUbahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnUbahActionPerformed
        // TODO add your handling code here:
        int selectedRow = tbPenyewa.getSelectedRow();
        if (selectedRow > -1) {
            txtNamaLengkapPenyewa.setText(tbPenyewa.getValueAt(selectedRow, 2).toString());
            txtNikPenyewa.setText(tbPenyewa.getValueAt(selectedRow, 1).toString());
            RadioButtonUtil.setGender(GenderConstant.valueOf(tbPenyewa.getValueAt(selectedRow, 3).toString()), rbPriaPenyewa, rbWanitaPenyewa);
            txtAlamatPenyewa.setText(tbPenyewa.getValueAt(selectedRow, 4).toString());
            txtNoHpPenyewa.setText(tbPenyewa.getValueAt(selectedRow, 5).toString());
            txtUsernamePenyewa.setText(tbPenyewa.getValueAt(selectedRow, 6).toString());
            txtPasswordPenyewa.setText(tbPenyewa.getValueAt(selectedRow, 7).toString());
            RadioButtonUtil.setStatusUser(UserStatusConstant.valueOf(tbPenyewa.getValueAt(selectedRow, 9).toString()), rbStatusUserAktifPenyewa, rbStatusUserKeluarPenyewa);
            Response response = portalController.getKamarKosongList();
            if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
                GetKamarKosongListResponse getKamarKosongListResponse = (GetKamarKosongListResponse) response.getResponseDto().getData();
                List<String> noKamarList = getKamarKosongListResponse.getKamarList().stream()
                        .map(GetKamarKosongDto::getNoKamar)
                        .collect(Collectors.toList());
                ComboBoxUtil.valueBuilder(cbKamarPenyewa, noKamarList);
                cbKamarPenyewa.setSelectedItem(tbPenyewa.getValueAt(selectedRow, 10).toString());
            }
            response = userController.getUserByNik(GetUserByNikRequest.builder()
                    .nik(tbPenyewa.getValueAt(selectedRow, 1).toString())
                    .build());
            if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
                GetUserByNikResponse getUserByNikResponse = (GetUserByNikResponse) response.getResponseDto().getData();
                txtTglMasuk.setDate(getUserByNikResponse.getUser().getTglMasuk());
                imageBase64 = getUserByNikResponse.getUser().getFoto();
                ImageUtil.convertToIcon(imageBase64, lbFoto);
            }
            txtJumlahTagihan.setText(String.valueOf(FormatCurencyUtil.toLong(tbPenyewa.getValueAt(selectedRow, 11).toString())));
            btnSimpanPenyewa.setText("Ubah");
        } else {
            JOptionPane.showMessageDialog(this, "Pilih data yang ingin di ubah");
        }
    }//GEN-LAST:event_mnUbahActionPerformed

    private void btnMenuTransaksiMasukActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuTransaksiMasukActionPerformed
        // TODO add your handling code here:
        showPanel(PANEL_SHOW_TRANSAKSI_MASUK);
        tabTableIncomingTransaction.setSelectedIndex(0);
        showIncomingTransactionHistoryList(GetHistoriTransaksiMasukListByFromTransaksiRequest.builder().fromTransaksi(FromTransactionConstant.ANGGOTA).build());
    }//GEN-LAST:event_btnMenuTransaksiMasukActionPerformed

    private void btnMenuLaporanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuLaporanActionPerformed
        // TODO add your handling code here:
        showPanel(PANEL_MENU_LAPORAN);
        reportTableTab.setSelectedIndex(0);
        showTransactionHistoryList(GetHistoriTransaksiListByTglTransaksiAndTipeTransaksiRequest.builder()
                .tipeTransaksi(TipeTransaksiConstant.ALL)
                .fromTglTransaksi(txtDateFrom.getDate())
                .toTglTransaksi(txtDateTo.getDate())
                .build());
    }//GEN-LAST:event_btnMenuLaporanActionPerformed

    private void btnBersihkanDonaturActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBersihkanDonaturActionPerformed
        // TODO add your handling code here:
        clearText(ButtonConstant.DONATUR);
    }//GEN-LAST:event_btnBersihkanDonaturActionPerformed

    private void btnTambahTransaksiMasukActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahTransaksiMasukActionPerformed
        // TODO add your handling code here:
        new tambahTransaksiMasuk(this, this, true).setVisible(true);
    }//GEN-LAST:event_btnTambahTransaksiMasukActionPerformed

    private void btnTambahTransaksiKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahTransaksiKeluarActionPerformed
        // TODO add your handling code here:
        new tambahTransaksiKeluar(this, this, true).setVisible(true);
    }//GEN-LAST:event_btnTambahTransaksiKeluarActionPerformed

    private void btnMenuTransaksiKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuTransaksiKeluarActionPerformed
        // TODO add your handling code here:
        showPanel(PANEL_SHOW_TRANSAKSI_KELUAR);
        showOutgoingTransactionHistoryList(GetTransactionHistoryOutgoingRequest.builder()
                .keyword(txtSearchOutgoingTransaction.getText())
                .build());
    }//GEN-LAST:event_btnMenuTransaksiKeluarActionPerformed

    private void btnMenuKamarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuKamarActionPerformed
        // TODO add your handling code here:
        showPanel(PANEL_MENU_KAMAR);
        showRoomList();
    }//GEN-LAST:event_btnMenuKamarActionPerformed

    private void txtNoKamarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNoKamarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNoKamarActionPerformed

    private void btnBersihkanKamarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBersihkanKamarActionPerformed
        // TODO add your handling code here:
        clearText(ButtonConstant.KAMAR);
    }//GEN-LAST:event_btnBersihkanKamarActionPerformed

    private void btnSimpanKamarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanKamarActionPerformed
        // TODO add your handling code here:
        Boolean numberOnly = RequestUtil.numberOnly(txtKapasitas.getText());
        Boolean alfabetANdNumberOnly = RequestUtil.alfabertAndNumberOnly(txtNoKamar.getText());
        Response response;
        StoreKamarRequest storeKamarRequest = StoreKamarRequest.builder()
                .noKamar(Boolean.TRUE.equals(alfabetANdNumberOnly) ? txtNoKamar.getText() : null)
                .kapasitas(Boolean.TRUE.equals(numberOnly) ? Integer.parseInt(txtKapasitas.getText()) : 0)
                .statusKamar(StatusKamarConstant.KOSONG)
                .build();
        if (btnSimpanKamar.getText().equals("Simpan")) {
            response = portalController.storeKamar(storeKamarRequest);
        } else {
            response = portalController.updateKamar(storeKamarRequest);
        }
        
        String message;
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            message = "Berhasil simpan data kamar.";
            showRoomList();
            clearText(ButtonConstant.KAMAR);
        } else {
            message = response.getResponseDto().getMessage();
        }
        JOptionPane.showMessageDialog(this, message);
    }//GEN-LAST:event_btnSimpanKamarActionPerformed

    private void btnMenuPenyewaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuPenyewaActionPerformed
        // TODO add your handling code here:
        showPanel(PANEL_MENU_PENYEWA);
        showUserList();
        Response response = portalController.getKamarKosongList();
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            GetKamarKosongListResponse getKamarKosongListResponse = (GetKamarKosongListResponse) response.getResponseDto().getData();
            List<String> noKamarList = getKamarKosongListResponse.getKamarList().stream()
                    .map(GetKamarKosongDto::getNoKamar)
                    .collect(Collectors.toList());
            ComboBoxUtil.valueBuilder(cbKamarPenyewa, noKamarList);
        } else {
            
        }
    }//GEN-LAST:event_btnMenuPenyewaActionPerformed

    private void rbPriaPenyewaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbPriaPenyewaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbPriaPenyewaActionPerformed

    private void btnSimpanPenyewaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanPenyewaActionPerformed
        // TODO add your handling code here:
        Boolean numberOnly = RequestUtil.numberOnly(txtNikPenyewa.getText());
        Logger.info("nik renter is number : {}", numberOnly, Main.class);
        Boolean numberOnly2 = RequestUtil.numberOnly(txtNoHpPenyewa.getText());
        Logger.info("phone number renter is number : {}", numberOnly2, Main.class);
        Boolean numberOnly3 = RequestUtil.numberOnly(txtJumlahTagihan.getText());
        Logger.info("payment amount is number : {}", numberOnly3, Main.class);
        Boolean fullName = RequestUtil.alfabertAndNumberOnly(txtNamaLengkapPenyewa.getText());
        StoreUserRequest storeUserRequest = StoreUserRequest.builder()
                .user(User.builder()
                        .nik(Boolean.TRUE.equals(numberOnly) ? txtNikPenyewa.getText() : null)
                        .namaLengkap(Boolean.TRUE.equals(fullName) ? txtNamaLengkapPenyewa.getText() : null)
                        .gender(RadioButtonUtil.getGender(rbPriaPenyewa, rbWanitaPenyewa))
                        .tglMasuk(txtTglMasuk.getDate())
                        .alamat(txtAlamatPenyewa.getText())
                        .noHp(Boolean.TRUE.equals(numberOnly2) ? txtNoHpPenyewa.getText() : null)
                        .username(txtUsernamePenyewa.getText())
                        .password(String.valueOf(txtPasswordPenyewa.getPassword()))
                        .status(RadioButtonUtil.getStatusUser(rbStatusUserAktifPenyewa, rbStatusUserKeluarPenyewa))
                        .kamar(Kamar.builder()
                                .noKamar(cbKamarPenyewa.getSelectedItem().toString())
                                .build())
                        .monthlyPayment(Boolean.TRUE.equals(numberOnly3) ? Long.parseLong(txtJumlahTagihan.getText()) : 0L)
                        .foto(imageBase64)
                        .hakAkses(HakAksesConstant.ANGGOTA.name())
                        .build())
                .build();
        Response response;
        if (btnSimpanPenyewa.getText().equals("Ubah")) {
            response = userController.updateUser(storeUserRequest);
        } else {
            response = userController.storeUser(storeUserRequest);
        }
        String message;
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            showUserList();
            clearText(ButtonConstant.PENYEWA);
            message = "Berhasil simpan data user.";
        } else {
            message = response.getResponseDto().getMessage();
        }
        JOptionPane.showMessageDialog(this, message);
    }//GEN-LAST:event_btnSimpanPenyewaActionPerformed

    private void btnBersihkanPenyewaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBersihkanPenyewaActionPerformed
        // TODO add your handling code here:
        clearText(ButtonConstant.PENYEWA);
    }//GEN-LAST:event_btnBersihkanPenyewaActionPerformed

    private void tabTableIncomingTransactionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabTableIncomingTransactionMouseClicked
        // TODO add your handling code here:
        try {
            int selectedTab = tabTableIncomingTransaction.getSelectedIndex();
            Logger.info("selected tab table incoming transaction : {}", selectedTab, Main.class);
            switch(selectedTab){
                case 0:
                    showIncomingTransactionHistoryList(GetHistoriTransaksiMasukListByFromTransaksiRequest.builder().fromTransaksi(FromTransactionConstant.ANGGOTA).build());
                    break;
                case 1:
                    showIncomingTransactionHistoryList(GetHistoriTransaksiMasukListByFromTransaksiRequest.builder().fromTransaksi(FromTransactionConstant.DONATUR).build());
                    break;
                case 2:
                    showIncomingTransactionHistoryList(GetHistoriTransaksiMasukListByFromTransaksiRequest.builder().fromTransaksi(FromTransactionConstant.LAINNYA).build());
                    break;
                default:
                    Logger.error("error array index", Main.class);
            }
        } catch (Exception e) {
            Logger.error("Error get incoming transaction : {}", e, Main.class);
            throw e;
        }
    }//GEN-LAST:event_tabTableIncomingTransactionMouseClicked

    private void btnCetakLaporanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCetakLaporanActionPerformed
        // TODO add your handling code here:
        int tabSelected = reportTableTab.getSelectedIndex();
        String tipeTransaction = null;
        switch(tabSelected) {
            case 0:
                tipeTransaction = TipeTransaksiConstant.ALL.name();
                break;
            case 1:
                tipeTransaction = TipeTransaksiConstant.MASUK.name();
                break;
            case 2:
                tipeTransaction = TipeTransaksiConstant.KELUAR.name();
                break;
        }
        showTransactionHistoryList(GetHistoriTransaksiListByTglTransaksiAndTipeTransaksiRequest.builder()
                .tipeTransaksi(TipeTransaksiConstant.valueOf(tipeTransaction))
                .fromTglTransaksi(txtDateFrom.getDate())
                .toTglTransaksi(txtDateTo.getDate())
                .build());
    }//GEN-LAST:event_btnCetakLaporanActionPerformed

    private void btnCetakLaporan1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCetakLaporan1ActionPerformed
        // TODO add your handling code here:
        int selectedTab = reportTableTab.getSelectedIndex();
        switch (selectedTab) {
            case 0:
                if (tbLaporanSemuaTransaksi.getRowCount() == 0) {
                    JOptionPane.showMessageDialog(this, "Data masih kosong");
                } else {
                    String fileName = "Laporan_semua_transaksi_";
                    if (txtDateFrom.getDate() == null || txtDateTo.getDate() == null) {
                        fileName += DateUtil.toString(new Date(), DateUtil.FORMAT_DATE_IDN_V2);
                    } else {
                        fileName += "dari_"+ DateUtil.toString(txtDateFrom.getDate(), DateUtil.FORMAT_DATE_IDN_V2);
                        fileName += "sampai_"+ DateUtil.toString(txtDateTo.getDate(), DateUtil.FORMAT_DATE_IDN_V2);
                    }
                    
                    Boolean success = ExportFileUtil.toPDF(tbLaporanSemuaTransaksi, fileName);
                    if (success) {
                        JOptionPane.showMessageDialog(this, "Berhasil download data.");
                    } else {
                        JOptionPane.showMessageDialog(this, "Gagal download data.");
                    }
                }
                break;
            case 1:
                if (tbLaporanSemuaTransaksi.getRowCount() == 0) {
                    JOptionPane.showMessageDialog(this, "Data masih kosong");
                } else {
                    String fileName = "Laporan_transaksi_masuk_";
                    if (txtDateFrom.getDate() == null || txtDateTo.getDate() == null) {
                        fileName += DateUtil.toString(new Date(), DateUtil.FORMAT_DATE_IDN_V2);
                    } else {
                        fileName += "dari_"+ DateUtil.toString(txtDateFrom.getDate(), DateUtil.FORMAT_DATE_IDN_V2);
                        fileName += "sampai_"+ DateUtil.toString(txtDateTo.getDate(), DateUtil.FORMAT_DATE_IDN_V2);
                    }
                    
                    Boolean success = ExportFileUtil.toPDF(tbLaporanSemuaTransaksi, fileName);
                    if (success) {
                        JOptionPane.showMessageDialog(this, "Berhasil download data.");
                    } else {
                        JOptionPane.showMessageDialog(this, "Gagal download data.");
                    }
                }
                break;
            case 2:
                if (tbLaporanSemuaTransaksi.getRowCount() == 0) {
                    JOptionPane.showMessageDialog(this, "Data masih kosong");
                } else {
                    String fileName = "Laporan_transaksi_keluar_";
                    if (txtDateFrom.getDate() == null || txtDateTo.getDate() == null) {
                        fileName += DateUtil.toString(new Date(), DateUtil.FORMAT_DATE_IDN_V2);
                    } else {
                        fileName += "dari_"+ DateUtil.toString(txtDateFrom.getDate(), DateUtil.FORMAT_DATE_IDN_V2);
                        fileName += "sampai_"+ DateUtil.toString(txtDateTo.getDate(), DateUtil.FORMAT_DATE_IDN_V2);
                    }
                    
                    Boolean success = ExportFileUtil.toPDF(tbLaporanSemuaTransaksi, fileName);
                    if (success) {
                        JOptionPane.showMessageDialog(this, "Berhasil download data.");
                    } else {
                        JOptionPane.showMessageDialog(this, "Gagal download data.");
                    }
                }
                break;
            default:
                break;
        }
    }//GEN-LAST:event_btnCetakLaporan1ActionPerformed

    private void jLabel36MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel36MouseClicked
        // TODO add your handling code here:
        new Profile(this, this, true).setVisible(true);
    }//GEN-LAST:event_jLabel36MouseClicked

    private void btnMenuDashboardPenyewaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuDashboardPenyewaActionPerformed
        // TODO add your handling code here:
        showPanel(PANEL_MENU_BERANDA_PENYEWA);
    }//GEN-LAST:event_btnMenuDashboardPenyewaActionPerformed

    private void btnMenuLaporanPenyewaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuLaporanPenyewaActionPerformed
        // TODO add your handling code here:
        showPanel(PANEL_MENU_LAPORAN_PENYEWA);
        showTransactionHistoryByUser(GetTransactionHistoryByUserIdAndTransactionDateRequest.builder()
                .userId(userOptional.getUser().getId())
                .build());
    }//GEN-LAST:event_btnMenuLaporanPenyewaActionPerformed

    private void jLabel61MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel61MouseClicked
        // TODO add your handling code here:
        new ProfilePenyewa(this, this, true).setVisible(true);
    }//GEN-LAST:event_jLabel61MouseClicked

    private void btnCetakLaporan2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCetakLaporan2ActionPerformed
        // TODO add your handling code here:
        if (tbLaporanPenyewa.getRowCount() == 0) {
            JOptionPane.showMessageDialog(this, "Data masih kosong");
        } else {
            String fileName = "Laporan_transaksi_";
            if (txtDateFromUser.getDate() == null || txtDateToUser.getDate() == null) {
                fileName += DateUtil.toString(new Date(), DateUtil.FORMAT_DATE_IDN_V2);
            } else {
                fileName += "dari_"+ DateUtil.toString(txtDateFromUser.getDate(), DateUtil.FORMAT_DATE_IDN_V2);
                fileName += "sampai_"+ DateUtil.toString(txtDateToUser.getDate(), DateUtil.FORMAT_DATE_IDN_V2);
            }
            Boolean success = ExportFileUtil.toPDF(tbLaporanPenyewa, fileName);
            if (success) {
                JOptionPane.showMessageDialog(this, "Berhasil download data.");
            } else {
                JOptionPane.showMessageDialog(this, "Gagal download data.");
            }
        }
    }//GEN-LAST:event_btnCetakLaporan2ActionPerformed

    private void btnCetakLaporan3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCetakLaporan3ActionPerformed
        // TODO add your handling code here:
        showTransactionHistoryByUser(GetTransactionHistoryByUserIdAndTransactionDateRequest.builder()
                .userId(userOptional.getUser().getId())
                .fromTransactionDate(txtDateFromUser.getDate())
                .toTransactionDate(txtDateToUser.getDate())
                .build());
    }//GEN-LAST:event_btnCetakLaporan3ActionPerformed

    private void tabTableHomePageAdminMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabTableHomePageAdminMouseClicked
        try {
            int selectedTab = tabTableHomePageAdmin.getSelectedIndex();
            Logger.info("selected tab table in homepage bendahara : {}", selectedTab, Main.class);
            switch(selectedTab){
                case 0:
                    showDueDateOverDueDateListInAdminHomepage(true);
                    break;
                case 1:
                    showDueDateOverDueDateListInAdminHomepage(false);
                    break;
                case 2:
                    showEmptyRoomListInAdminHomepage();
                    break;
                default:
                    Logger.error("error array index", Main.class);
                    break;
            }
        } catch (Exception e) {
            Logger.error("Error Exception : {}", e, Main.class);
            throw e;
        }
    }//GEN-LAST:event_tabTableHomePageAdminMouseClicked

    private void reportTableTabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_reportTableTabMouseClicked
        // TODO add your handling code here:
        try {
            int selectedTab = reportTableTab.getSelectedIndex();
            Logger.info("selected tab table report transaction : {}", selectedTab, Main.class);
            switch(selectedTab){
                case 0:
                    showTransactionHistoryList(GetHistoriTransaksiListByTglTransaksiAndTipeTransaksiRequest.builder()
                        .tipeTransaksi(TipeTransaksiConstant.ALL)
                        .fromTglTransaksi(txtDateFrom.getDate())
                        .toTglTransaksi(txtDateTo.getDate())
                        .build());
                    break;
                case 1:
                    showTransactionHistoryList(GetHistoriTransaksiListByTglTransaksiAndTipeTransaksiRequest.builder()
                        .tipeTransaksi(TipeTransaksiConstant.MASUK)
                        .fromTglTransaksi(txtDateFrom.getDate())
                        .toTglTransaksi(txtDateTo.getDate())
                        .build());
                    break;
                case 2:
                    showTransactionHistoryList(GetHistoriTransaksiListByTglTransaksiAndTipeTransaksiRequest.builder()
                        .tipeTransaksi(TipeTransaksiConstant.KELUAR)
                        .fromTglTransaksi(txtDateFrom.getDate())
                        .toTglTransaksi(txtDateTo.getDate())
                        .build());
                    break;
                default:
                    Logger.error("Error index", Main.class);
                    break;
            }
        } catch (Exception e) {
            Logger.error("Error report transaction : {}", e, Main.class);
            throw e;
        }
    }//GEN-LAST:event_reportTableTabMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try {
            int selectedTab = tabTableIncomingTransaction.getSelectedIndex();
            Logger.info("selected tab table incoming transaction : {}", selectedTab, this.getClass());
            switch(selectedTab){
                case 0:
                    showIncomingTransactionHistoryList(GetHistoriTransaksiMasukListByFromTransaksiRequest.builder()
                            .fromTransaksi(FromTransactionConstant.ANGGOTA)
                            .keyword(txtSearchIncomingTransaction.getText())
                            .build());
                    break;
                case 1:
                    showIncomingTransactionHistoryList(GetHistoriTransaksiMasukListByFromTransaksiRequest.builder()
                            .fromTransaksi(FromTransactionConstant.DONATUR)
                            .keyword(txtSearchIncomingTransaction.getText())
                            .build());
                    break;
                case 2:
                    showIncomingTransactionHistoryList(GetHistoriTransaksiMasukListByFromTransaksiRequest.builder()
                            .fromTransaksi(FromTransactionConstant.LAINNYA)
                            .keyword(txtSearchIncomingTransaction.getText())
                            .build());
                    break;
                default:
                    Logger.error("error array index", Main.class);
            }
        } catch (Exception e) {
            Logger.error("Error get incoming transaction : {}", e, Main.class);
            throw e;
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void updateKamarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateKamarActionPerformed
        // TODO add your handling code here:
        int selectedRow = tbKamar.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Pilih data yang akan di ubah");
        } else {
            txtNoKamar.setText(tbKamar.getValueAt(selectedRow, 1).toString());
            txtKapasitas.setText(tbKamar.getValueAt(selectedRow, 2).toString());
            txtNoKamar.setEditable(false);
            btnSimpanKamar.setText("Ubah");
        }
    }//GEN-LAST:event_updateKamarActionPerformed

    private void deleteKamarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteKamarActionPerformed
        // TODO add your handling code here:
        int selectedRow = tbKamar.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Pilih data yang akan di hapus");
        } else {
            Response response = portalController.deleteKamar(GetKamarByNoKamarRequest.builder()
                    .noKamar(tbKamar.getValueAt(selectedRow, 1).toString())
                    .build());
            String message;
            if (response.getResponseDto().getMessageCode().equals(MessageCodeConstant.SUCCESS)) {
                message = "berhasil hapus data kamar";
                showRoomList();
            } else {
                message = response.getResponseDto().getMessage();
            }
            JOptionPane.showMessageDialog(this, message);
        }
    }//GEN-LAST:event_deleteKamarActionPerformed

    private void spTbDaftarjatuhTempoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_spTbDaftarjatuhTempoKeyPressed
        // TODO add your handling code here:
        switch (evt.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
                spTbDaftarKeterlambatan.doLayout();
                break;
            default:
                break;
        }
    }//GEN-LAST:event_spTbDaftarjatuhTempoKeyPressed

    private void spTbDaftarKeterlambatanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_spTbDaftarKeterlambatanKeyPressed
        // TODO add your handling code here:
        switch (evt.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                spTbDaftarjatuhTempo.requestFocus();
                break;
            case KeyEvent.VK_RIGHT:
                spTbDaftarKamarKosong.requestFocus();
                break;
            default:
                break;
        }
    }//GEN-LAST:event_spTbDaftarKeterlambatanKeyPressed

    private void spTbDaftarKamarKosongKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_spTbDaftarKamarKosongKeyPressed
        // TODO add your handling code here:
        switch (evt.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                spTbDaftarKeterlambatan.requestFocus();
                break;
            default:
                break;
        }
    }//GEN-LAST:event_spTbDaftarKamarKosongKeyPressed

    private void updateDonaturActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateDonaturActionPerformed
        // TODO add your handling code here:
        int selectedRow = tbDonatur.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Pilih data yang akan di ubah");
        } else {
            txtNikDonatur.setText(tbDonatur.getValueAt(selectedRow, 1).toString());
            txtNamaLengkapDonatur.setText(tbDonatur.getValueAt(selectedRow, 2).toString());
            RadioButtonUtil.setGender(GenderConstant.valueOf(tbDonatur.getValueAt(selectedRow, 3).toString()), rbPriaDonatur, rbWanitaDonatur);
            txtAlamatDonatur.setText(tbDonatur.getValueAt(selectedRow, 4).toString());
            txtNoHpDonatur.setText(tbDonatur.getValueAt(selectedRow, 5).toString());
            txtNikDonatur.setEditable(false);
            btnSimpanDonatur.setText("Ubah");
        }
    }//GEN-LAST:event_updateDonaturActionPerformed

    private void deleteDonaturActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteDonaturActionPerformed
        // TODO add your handling code here:
        int selectedRow = tbDonatur.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Pilih data yang akan di hapus");
        } else {
            Response response = portalController.deleteDonatur(GetDonaturByNikRequest.builder()
                    .nik(tbDonatur.getValueAt(selectedRow, 1).toString())
                    .build());
            String message;
            if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
                message = "Berhasil hapus data donatur";
            } else {
                message = response.getResponseDto().getMessage();
            }
            JOptionPane.showMessageDialog(this, message);
            showDonaturList();
        }
    }//GEN-LAST:event_deleteDonaturActionPerformed

    private void btnSearchOutgoingTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchOutgoingTransactionActionPerformed
        // TODO add your handling code here:
        showOutgoingTransactionHistoryList(GetTransactionHistoryOutgoingRequest.builder()
                .keyword(txtSearchOutgoingTransaction.getText())
                .build());
    }//GEN-LAST:event_btnSearchOutgoingTransactionActionPerformed

    private void lbFullNameUserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbFullNameUserMouseClicked
        // TODO add your handling code here:
        new ProfilePenyewa(this, this, true).setVisible(true);
    }//GEN-LAST:event_lbFullNameUserMouseClicked

    private void lbFullNameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbFullNameMouseClicked
        // TODO add your handling code here:
        new Profile(this, this, true).setVisible(true);
    }//GEN-LAST:event_lbFullNameMouseClicked

    private void btnMenuLaporanTunggakanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuLaporanTunggakanActionPerformed
        // TODO add your handling code here:
        showPanel(PANEL_MENU_LAPORAN_TUNGGAKAN);
        showLaporanTunggakan();
    }//GEN-LAST:event_btnMenuLaporanTunggakanActionPerformed

    private void btnCetakLaporanTunggakanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCetakLaporanTunggakanActionPerformed
        // TODO add your handling code here:
        if (tbLaporanTunggakan.getRowCount() == 0) {
            JOptionPane.showMessageDialog(this, "Data masih kosong");
        } else {
            String fileName = "Laporan_tunggakan_semua_transaksi_" + DateUtil.toString(new Date(), DateUtil.FORMAT_DATE_IDN_V2);
            Boolean success = ExportFileUtil.toPDF(tbLaporanTunggakan, fileName);
            if (success) {
                JOptionPane.showMessageDialog(this, "Berhasil download data.");
            } else {
                JOptionPane.showMessageDialog(this, "Gagal download data.");
            }
        }
    }//GEN-LAST:event_btnCetakLaporanTunggakanActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        FileFilter ff = new FileNameExtensionFilter("images","jpeg");
        JFileChooser picchoose = new JFileChooser();
        picchoose.addChoosableFileFilter(ff);
        int open = picchoose.showOpenDialog(this);
        if (open == APPROVE_OPTION) {
            File pic = picchoose.getSelectedFile();
            String path = pic.getAbsolutePath();
            try {
                BufferedImage img = ImageUtil.resizeImage(ImageIO.read(picchoose.getSelectedFile()), lbFoto.getWidth(), lbFoto.getHeight());
                ImageIcon imageic = new ImageIcon(new ImageIcon(img).getImage().getScaledInstance(lbFoto.getWidth(), lbFoto.getHeight(), SCALE_DEFAULT));
                lbFoto.setIcon(imageic);

                imageBase64 = ImageUtil.encodeToString(img);
            } catch (Exception ex) {
                Logger.error("Filed to upload image ", ex, Main.class);
                JOptionPane.showMessageDialog(this, "Gagal Unggah Foto.");
            }
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new Main().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBersihkanDonatur;
    private javax.swing.JButton btnBersihkanKamar;
    private javax.swing.JButton btnBersihkanPenyewa;
    private javax.swing.JButton btnCetakLaporan;
    private javax.swing.JButton btnCetakLaporan1;
    private javax.swing.JButton btnCetakLaporan2;
    private javax.swing.JButton btnCetakLaporan3;
    private javax.swing.JButton btnCetakLaporanTunggakan;
    private javax.swing.JButton btnMenuDashboard;
    private javax.swing.JButton btnMenuDashboardPenyewa;
    private javax.swing.JButton btnMenuDonatur;
    private javax.swing.JButton btnMenuKamar;
    private javax.swing.JButton btnMenuLaporan;
    private javax.swing.JButton btnMenuLaporanPenyewa;
    private javax.swing.JButton btnMenuLaporanTunggakan;
    private javax.swing.JButton btnMenuPenyewa;
    private javax.swing.JButton btnMenuTransaksiKeluar;
    private javax.swing.JButton btnMenuTransaksiMasuk;
    private javax.swing.JButton btnSearchOutgoingTransaction;
    private javax.swing.JButton btnSimpanDonatur;
    private javax.swing.JButton btnSimpanKamar;
    private javax.swing.JButton btnSimpanPenyewa;
    private javax.swing.JButton btnTambahTransaksiKeluar;
    private javax.swing.JButton btnTambahTransaksiMasuk;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cbKamarPenyewa;
    private javax.swing.JMenuItem deleteDonatur;
    private javax.swing.JMenuItem deleteKamar;
    private javax.swing.ButtonGroup genderGrup;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel29;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel30;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel35;
    private javax.swing.JPanel jPanel36;
    private javax.swing.JPanel jPanel37;
    private javax.swing.JPanel jPanel38;
    private javax.swing.JPanel jPanel39;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel40;
    private javax.swing.JPanel jPanel41;
    private javax.swing.JPanel jPanel42;
    private javax.swing.JPanel jPanel43;
    private javax.swing.JPanel jPanel44;
    private javax.swing.JPanel jPanel45;
    private javax.swing.JPanel jPanel47;
    private javax.swing.JPanel jPanel48;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane18;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JLabel lbDateTimeRealTime;
    private javax.swing.JLabel lbDueDate;
    private javax.swing.JLabel lbFoto;
    private javax.swing.JLabel lbFotoMenuPenyewa;
    private javax.swing.JLabel lbFullName;
    private javax.swing.JLabel lbFullNameUser;
    private javax.swing.JLabel lbJmlDonaturInBerandaAdmin;
    private javax.swing.JLabel lbJmlKamarInBerandaAdmin;
    private javax.swing.JLabel lbJmlPenyewaInBerandaAdmin;
    private javax.swing.JLabel lbJmlUangKelarTodayInBerandaAdmin;
    private javax.swing.JLabel lbSisaSaldo;
    private javax.swing.JLabel lbSisaSaldoInBerandaAdmin;
    private javax.swing.JLabel lbSisaSaldoInTransKeluar;
    private javax.swing.JLabel lbTagihanBulanan;
    private javax.swing.JLabel lbTglMasuk;
    private javax.swing.JLabel lbTotalTransaction;
    private javax.swing.JLabel lbUangMasukTodayInBerandaAdmin;
    private javax.swing.JLabel lblCariBendahara;
    private javax.swing.JLabel lblKamar1;
    private javax.swing.JLabel lblMenu;
    private javax.swing.JLabel lblPassword1;
    private javax.swing.JLabel lblTotalTransaksi;
    private javax.swing.JLabel lblTotalTransaksiKeluar;
    private javax.swing.JLabel lblUsername1;
    private javax.swing.JLabel lblUsername2;
    private javax.swing.JLabel lblUsername3;
    private javax.swing.JLabel lblstatus1;
    private javax.swing.JMenuItem mnHapus;
    private javax.swing.JMenuItem mnUbah;
    private javax.swing.JPanel panelBeranda;
    private javax.swing.JPanel panelBerandaPenyewa;
    private javax.swing.JPanel panelDonatur;
    private javax.swing.JPanel panelKamar;
    private javax.swing.JPanel panelLaporan;
    private javax.swing.JPanel panelLaporanPenyewa;
    private javax.swing.JPanel panelLaporanTunggakan;
    private javax.swing.JPanel panelMainBorder;
    private javax.swing.JPanel panelMenu;
    private javax.swing.JPanel panelMenuBendahara;
    private javax.swing.JPanel panelMenuPenyewa;
    private javax.swing.JPanel panelPenyewa;
    private javax.swing.JPanel panelTransaksiKeluar;
    private javax.swing.JPanel panelTransaksiMasuk;
    private javax.swing.JPanel panel_show_laporan;
    private javax.swing.JPanel panel_show_laporanTunggakan;
    private javax.swing.JPanel panel_show_transaksi_keluar;
    private javax.swing.JPanel panel_show_transaksi_masuk;
    private javax.swing.JPopupMenu pmTbDonatur;
    private javax.swing.JPopupMenu pmTbKamar;
    private javax.swing.JPopupMenu pmTbUser;
    private javax.swing.JRadioButton rbPriaDonatur;
    private javax.swing.JRadioButton rbPriaPenyewa;
    private javax.swing.JRadioButton rbStatusUserAktifPenyewa;
    private javax.swing.JRadioButton rbStatusUserKeluarPenyewa;
    private javax.swing.JRadioButton rbWanitaDonatur;
    private javax.swing.JRadioButton rbWanitaPenyewa;
    private javax.swing.JTabbedPane reportTableTab;
    private javax.swing.JScrollPane spTbDaftarKamarKosong;
    private javax.swing.JScrollPane spTbDaftarKeterlambatan;
    private javax.swing.JScrollPane spTbDaftarjatuhTempo;
    private javax.swing.ButtonGroup statusUserGrup;
    private javax.swing.JTabbedPane tabTableHomePageAdmin;
    private javax.swing.JTabbedPane tabTableIncomingTransaction;
    private javax.swing.JTable tbBerandaDaftarJatuhTempo;
    private javax.swing.JTable tbBerandaDaftarKamarKosong;
    private javax.swing.JTable tbBerandaDaftarKeterlambatan;
    private javax.swing.JTable tbDonatur;
    private javax.swing.JTable tbKamar;
    private javax.swing.JTable tbLaporanPenyewa;
    private javax.swing.JTable tbLaporanSemuaTransaksi;
    private javax.swing.JTable tbLaporanTransaksiKeluar;
    private javax.swing.JTable tbLaporanTransaksiMasuk;
    private javax.swing.JTable tbLaporanTunggakan;
    private javax.swing.JTable tbPenyewa;
    private javax.swing.JTable tbTransaksiDonatur;
    private javax.swing.JTable tbTransaksiKeluar;
    private javax.swing.JTable tbTransaksiLainnya;
    private javax.swing.JTable tbTransaksiPenyewa;
    private javax.swing.JTextArea txtAlamat;
    private javax.swing.JTextArea txtAlamatDonatur;
    private javax.swing.JTextArea txtAlamatPenyewa;
    private com.toedter.calendar.JDateChooser txtDateFrom;
    private com.toedter.calendar.JDateChooser txtDateFromUser;
    private com.toedter.calendar.JDateChooser txtDateTo;
    private com.toedter.calendar.JDateChooser txtDateToUser;
    private javax.swing.JTextField txtFullName;
    private javax.swing.JTextField txtJumlahTagihan;
    private javax.swing.JTextField txtKapasitas;
    private javax.swing.JTextField txtNamaLengkapDonatur;
    private javax.swing.JTextField txtNamaLengkapPenyewa;
    private javax.swing.JTextField txtNik;
    private javax.swing.JTextField txtNikDonatur;
    private javax.swing.JTextField txtNikPenyewa;
    private javax.swing.JTextField txtNoHpDonatur;
    private javax.swing.JTextField txtNoHpPenyewa;
    private javax.swing.JTextField txtNoKamar;
    private javax.swing.JPasswordField txtPasswordPenyewa;
    private javax.swing.JPasswordField txtPasswordRenter;
    private javax.swing.JTextField txtPhoneNumber;
    private javax.swing.JTextField txtSearchIncomingTransaction;
    private javax.swing.JTextField txtSearchOutgoingTransaction;
    private com.toedter.calendar.JDateChooser txtTglMasuk;
    private javax.swing.JTextField txtTglMasukRenter;
    private javax.swing.JTextField txtUsername;
    private javax.swing.JTextField txtUsernamePenyewa;
    private javax.swing.JTextField txtgender;
    private javax.swing.JMenuItem updateDonatur;
    private javax.swing.JMenuItem updateKamar;
    // End of variables declaration//GEN-END:variables

    private ColumnTableConfigRequest columnTableModel() {
        Map<JTable, DefaultTableModel> columnTable = new HashMap<>();
        columnTable.put(tbBerandaDaftarKamarKosong, columnTableConfig.modelDaftarKamarKosongInBerandaAdmin);
        columnTable.put(tbBerandaDaftarKeterlambatan, columnTableConfig.modelDaftarKeterlambatanInBerandaAdmin);
        columnTable.put(tbLaporanTunggakan, columnTableConfig.modelDaftarLaporanTunggakan);
        columnTable.put(tbBerandaDaftarJatuhTempo, columnTableConfig.modelDaftarJatuhTempoInBerandaAdmin);
        columnTable.put(tbKamar, columnTableConfig.modelDaftarKamar);
        columnTable.put(tbPenyewa, columnTableConfig.modelDaftarPenyewa);
        columnTable.put(tbDonatur, columnTableConfig.modelDaftarDonatur);
        columnTable.put(tbTransaksiDonatur, columnTableConfig.modelDaftarTransaksiMasukFromDonatur);
        columnTable.put(tbTransaksiPenyewa, columnTableConfig.modelDaftarTransaksiMasukFromPenyewa);
        columnTable.put(tbTransaksiLainnya, columnTableConfig.modelDaftarTransaksiMasukFromLainnya);
        columnTable.put(tbTransaksiKeluar, columnTableConfig.modelDaftarTransaksiKeluar);
        columnTable.put(tbLaporanSemuaTransaksi, columnTableConfig.modelDaftarLaporanSemuaTransaksi);
        columnTable.put(tbLaporanTransaksiMasuk, columnTableConfig.modelDaftarLaporanTransaksiMasuk);
        columnTable.put(tbLaporanTransaksiKeluar, columnTableConfig.modelDaftarLaporanTransaksiKeluar);
        columnTable.put(tbLaporanPenyewa, columnTableConfig.modelDaftarLaporanTransaksiPenyewa);
        return ColumnTableConfigRequest.builder().columnTableConfigModelMap(columnTable).build();
    }
    
    private void setWarnaButton(ButtonConstant btn) {
        buttonConfig.setWarnaButton(ButtonColorRequest.builder()
                .btn(btn)
                .btnMenuDashboard(btnMenuDashboard)
                .btnMenuDashboardPenyewa(btnMenuDashboardPenyewa)
                .btnMenuDonatur(btnMenuDonatur)
                .btnMenuKamar(btnMenuKamar)
                .btnMenuLaporan(btnMenuLaporan)
                .btnMenuLaporanTunggakan(btnMenuLaporanTunggakan)
                .btnMenuLaporanPenyewa(btnMenuLaporanPenyewa)
                .btnMenuPenyewa(btnMenuPenyewa)
                .btnMenuTransaksiKeluar(btnMenuTransaksiKeluar)
                .btnMenuTransaksiMasuk(btnMenuTransaksiMasuk)
                .build());
    }
    
    private void showPanel(String menu) {
        switch(menu) {
            case PANEL_MENU_BERANDA:
                panelConfig.setPanel(panelMainBorder,panelBeranda);
                lblMenu.setText(LabelMenuConstant.BERANDA);
                setWarnaButton(ButtonConstant.BERANDA);
                columnTableConfig.setColumnTableBeranda(tbBerandaDaftarJatuhTempo, tbBerandaDaftarKeterlambatan, tbBerandaDaftarKamarKosong);
                break;
            case PANEL_MENU_DONATUR:
                panelConfig.setPanel(panelMainBorder,panelDonatur);
                lblMenu.setText(LabelMenuConstant.DONATUR);
                setWarnaButton(ButtonConstant.DONATUR);
                columnTableConfig.setColumnTableDonatur(tbDonatur);
                break;
            case PANEL_SHOW_TRANSAKSI_MASUK:
                panelConfig.setPanel(panelMainBorder,panelTransaksiMasuk);
                lblMenu.setText(LabelMenuConstant.TRANSAKSI_MASUK);
                lblCariBendahara.setText(AsramaConstant.LABEL_CARI_TRANSAKSI_MASUK_BENDAHARA_ANGGOTA);
                setWarnaButton(ButtonConstant.TRANSAKSI_MASUK);
                columnTableConfig.setColumnTableTransaksiMasuk(tbTransaksiPenyewa, tbTransaksiDonatur, tbTransaksiLainnya);
                break;
            case PANEL_SHOW_TRANSAKSI_KELUAR:
                panelConfig.setPanel(panelMainBorder,panelTransaksiKeluar);
                lblMenu.setText(LabelMenuConstant.TRANSAKSI_KELUAR);
                setWarnaButton(ButtonConstant.TRANSAKSI_KELUAR);
                columnTableConfig.setColumnTableTransaksiKeluar(tbTransaksiKeluar);
                break;
            case PANEL_MENU_KAMAR:
                panelConfig.setPanel(panelMainBorder,panelKamar);
                lblMenu.setText(LabelMenuConstant.KAMAR);
                setWarnaButton(ButtonConstant.KAMAR);
                columnTableConfig.setColumnTableKamar(tbKamar);
                break;
            case PANEL_MENU_PENYEWA:
                panelConfig.setPanel(panelMainBorder,panelPenyewa);
                lblMenu.setText(LabelMenuConstant.PENYEWA);
                setWarnaButton(ButtonConstant.PENYEWA);
                columnTableConfig.setColumnTablePenyewa(tbPenyewa);
                break;
            case PANEL_MENU_LAPORAN:
                panelConfig.setPanel(panelMainBorder,panelLaporan);
                lblMenu.setText(LabelMenuConstant.LAPORAN_TRANSAKSI);
                setWarnaButton(ButtonConstant.LAPORAN);
                columnTableConfig.setColumnTableLaporan(tbLaporanSemuaTransaksi, tbLaporanTransaksiMasuk, tbLaporanTransaksiKeluar);
                break;
            case PANEL_MENU_LAPORAN_TUNGGAKAN:
                panelConfig.setPanel(panelMainBorder, panelLaporanTunggakan);
                lblMenu.setText(LabelMenuConstant.LAPORAN_TUNGGAKAN);
                setWarnaButton(ButtonConstant.LAPORAN_TUNGGAKAN);
                columnTableConfig.setColumnTableLaporanTunggakan(tbLaporanTunggakan);
                break;
            case PANEL_MENU_BERANDA_PENYEWA:
                panelConfig.setPanel(panelMainBorder,panelBerandaPenyewa);
                lblMenu.setText(LabelMenuConstant.BERANDA);
                setWarnaButton(ButtonConstant.BERANDA_PENYEWA);
                break;
            case PANEL_MENU_LAPORAN_PENYEWA:
                panelConfig.setPanel(panelMainBorder,panelLaporanPenyewa);
                lblMenu.setText(LabelMenuConstant.LAPORAN_TRANSAKSI);
                setWarnaButton(ButtonConstant.LAPORAN_PENYEWA);
                columnTableConfig.setColumnTableLaporanPenyewa(tbLaporanPenyewa);
                break;
            default:
                Logger.error("Error show panel", Main.class);
                break;
        }
    }
    
    private void clearText(ButtonConstant menu) {
        try {
            switch(menu) {
                case KAMAR:
                    txtNoKamar.setText(null);
                    txtKapasitas.setText(null);
                    btnSimpanKamar.setText("Simpan");
                    txtNoKamar.setEditable(true);
                    break;
                case PENYEWA:
                    txtNamaLengkapPenyewa.setText(null);
                    txtNikPenyewa.setText(null);
                    genderGrup.clearSelection();
                    txtTglMasuk.setDate(null);
                    txtAlamatPenyewa.setText(null);
                    txtNoHpPenyewa.setText(null);
                    txtUsernamePenyewa.setText(null);
                    txtPasswordPenyewa.setText(null);
                    statusUserGrup.clearSelection();
                    cbKamarPenyewa.setSelectedIndex(0);
                    txtJumlahTagihan.setText(null);
                    btnSimpanPenyewa.setText("Simpan");
                    txtNikPenyewa.setEditable(true);
                    txtUsernamePenyewa.setEditable(true);
                    lbFoto.setIcon(null);
                    break;
                case DONATUR:
                    txtNamaLengkapDonatur.setText(null);
                    txtNikDonatur.setText(null);
                    genderGrup.clearSelection();
                    txtAlamatDonatur.setText(null);
                    txtNoHpDonatur.setText(null);
                    btnSimpanDonatur.setText("Simpan");
                    break;
                default:
                    Logger.error("Error clear text", Main.class);
                    break;
            }
        } catch (Exception e) {
            Logger.error("Error clear text : {}", e, Main.class);
            throw e;
        }
    }

    public GetUserByUsernameAndPasswordResponse getUserOptional() {
        return userOptional;
    }

    public UserController getUserController() {
        return userController;
    }

    public HistoriTransaksiController getHistoriTransaksiController() {
        return historiTransaksiController;
    }

    public PortalController getPortalController() {
        return portalController;
    }
    
    @Override
    public void run() {
        while (true) {
            lbDateTimeRealTime.setText(DateUtil.toString(new Date(), DateUtil.FORMAT_DATE_TIME_IDN_V3)+ " ");
        }
    }
}
