package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetDueDateOverDueDateResponse implements Serializable {
    private static final long serialVersionUID = -2383394176145512351L;

    private List<DueDateOverDueDateDto> dueDateOverDueDateList;
}
