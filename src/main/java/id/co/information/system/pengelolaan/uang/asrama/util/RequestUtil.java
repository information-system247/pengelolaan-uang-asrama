/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package id.co.information.system.pengelolaan.uang.asrama.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.apache.commons.lang.ArrayUtils;

/**
 *
 * @author wahyu
 */
public class RequestUtil {
    
    private static final List<Character> numberCharList = List.of('0','1','2','3','4','5','6','7','8','9');
    private static final List<Character> alfabetCharList = List.of('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                                                                   'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
    
    public static Boolean numberOnly(String input) {
        return RequestUtil.validateInput(input, numberCharList);
    }
    
    public static Boolean alfabetOnly(String input) {
        return RequestUtil.validateInput(input, alfabetCharList);
    }
    
    public static Boolean alfabertAndNumberOnly(String input) {
        List<Character> arrays = RequestUtil.merge(numberCharList, alfabetCharList);
        return RequestUtil.validateInput(input, arrays);
    }
    
    public static Boolean validateInput(String input, List<?> validateKey) {
        try {
            input = input.replace(" ", "");
            char[] charString = input.toCharArray();
            List<Character> inputList = new ArrayList<>();
            for (int i = 0; i < charString.length; i++) {
                inputList.add(charString[i]);
            }
            inputList = inputList.stream().sorted().collect(Collectors.toList());
            AtomicInteger counter = new AtomicInteger(0);
            inputList.forEach(y -> {
                Long count = validateKey.stream().filter(k -> k.equals(y)).count();
                if (count != 0L) {
                    counter.getAndIncrement();
                }
            });
            Logger.info("counter[{}] same input[{}]", counter, inputList.size(), RequestUtil.class);
            if (counter.get() == inputList.size()) {
                return Boolean.TRUE;
            }
            return Boolean.FALSE;
        } catch (Exception e) {
            Logger.error("Error tracking validate input : {}", RequestUtil.class);
            return Boolean.FALSE;
        }
    }
    
    private static List<Character> merge(List<?> array1, List<?> array2) {
        try {
            Object[] keyList = ArrayUtils.addAll(array1.toArray(), array2.toArray());
            List<Character> arrayMerge = new ArrayList<>();
            for (Object keyList1 : keyList) {
                arrayMerge.add((char) keyList1);
            }
            return arrayMerge;
        } catch (Exception e) {
            Logger.error("Error merge array : {}", e, RequestUtil.class);
            throw e;
        }
    }
}
