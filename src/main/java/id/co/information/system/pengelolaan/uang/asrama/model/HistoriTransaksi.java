/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.model;

import id.co.information.system.pengelolaan.uang.asrama.constant.FromTransactionConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.TipeTransaksiConstant;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HistoriTransaksi implements Serializable{
    private static final long serialVersionUID = -5513456604343596172L;

    private Long id;
    private String transaksiId;
    private Date tglTransaksi;
    private TipeTransaksiConstant tipeTransaksi;
    private FromTransactionConstant fromTransaction;
    private Long userId;
    private Integer jangkaWaktu;
    private Long monthlyPayment;
    private Date jatuhTempo;
    private Long donaturId;
    private String namaTransaksi;
    private String namaLengkap;
    private String keterangan;
    private Long total;
    private Long sisaSaldo;
    private String createAt;
}
