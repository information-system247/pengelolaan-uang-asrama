package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import id.co.information.system.pengelolaan.uang.asrama.model.HistoriTransaksi;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreHistoriTransaksiResponse implements Serializable {
    private static final long serialVersionUID = 2209601675829970161L;

    private HistoriTransaksi historiTransaksi;
}
