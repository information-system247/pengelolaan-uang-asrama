package id.co.information.system.pengelolaan.uang.asrama.controller;

import id.co.information.system.pengelolaan.uang.asrama.constant.MessageCodeConstant;
import id.co.information.system.pengelolaan.uang.asrama.dto.Response;
import id.co.information.system.pengelolaan.uang.asrama.dto.donatur.GetDonaturByNikRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.donatur.GetDonaturListResponse;
import id.co.information.system.pengelolaan.uang.asrama.dto.donatur.StoreDonaturRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.room.GetKamarByNoKamarRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.room.GetKamarListResponse;
import id.co.information.system.pengelolaan.uang.asrama.dto.room.StoreKamarRequest;
import id.co.information.system.pengelolaan.uang.asrama.service.PortalService;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;

public class PortalController extends BaseController{

    private final PortalService portalService = new PortalService();

    public Response getKamarList() {
        try {
            Response getKamarListResponse = portalService.getKamarList();
            displayResponse(getKamarListResponse);
            return getKamarListResponse;
        } catch (Exception e) {
            Logger.error("Error response room list : {}", e, PortalController.class);
            throw e;
        }
    }

    public Response getDonaturList() {
        try {
            Response getDonaturListResponse = portalService.getDonaturList();
            displayResponse(getDonaturListResponse);
            return getDonaturListResponse;
        } catch (Exception e) {
            Logger.error("Error response donatur list : {}", e, PortalController.class);
            throw e;
        }
    }

    public int numberOfRooms() {
        Response response = getKamarList();
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            GetKamarListResponse getKamarListResponse = (GetKamarListResponse) response.getResponseDto().getData();
            return getKamarListResponse.getKamarList().size();
        }
        return 0;
    }

    public int numberOfDonatur() {
        Response response = getDonaturList();
        if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
            GetDonaturListResponse getDonaturListResponse = (GetDonaturListResponse) response.getResponseDto().getData();
            return getDonaturListResponse.getDonaturList().size();
        }
        return 0;
    }

    public Response getKamarKosongList() {
        try {
            Response getKamarKosongListResponse = portalService.getKamarKosongList();
            displayResponse(getKamarKosongListResponse);
            return getKamarKosongListResponse;
        } catch (Exception e) {
            Logger.error("Error response empty room list : {}", e, PortalController.class);
            throw e;
        }
    }

    public Response storeKamar(StoreKamarRequest request) {
        try {
            Response storeKamarResponse = portalService.storeKamar(request);
            displayResponse(storeKamarResponse);
            return storeKamarResponse;
        } catch (Exception e) {
            Logger.error("Error response room store : {}", e, PortalController.class);
            throw e;
        }
    }

    public Response storeDonatur(StoreDonaturRequest request) {
        try {
            Response response = portalService.storeDonatur(request);
            displayResponse(response);
            return response;
        } catch (Exception e) {
            Logger.error("Error response store donatur : {}", e, PortalController.class);
            throw e;
        }
    }

    public Response getDonaturByNik(GetDonaturByNikRequest request) {
        try {
            Response response = portalService.getDonaturByNik(request);
            displayResponse(response);
            return response;
        } catch (Exception e) {
            Logger.error("Error response donatur by nik : {}", e, PortalController.class);
            throw e;
        }
    }
    
    public Response updateKamar(StoreKamarRequest request) {
        try {
            Response response = portalService.updateKamar(request);
            displayResponse(response);
            return response;
        } catch (Exception e) {
            Logger.error("Error response update kamar : {}", e, PortalController.class);
            throw e;
        }
    }
    
    public Response deleteKamar(GetKamarByNoKamarRequest request) {
        try {
            Response response = portalService.deleteKamar(request);
            displayResponse(response);
            return response;
        } catch (Exception e) {
            Logger.error("Error response delete kamar : {}", e, PortalController.class);
            throw e;
        }
    }
    
    public Response deleteDonatur(GetDonaturByNikRequest request) {
        try {
            Response response = portalService.deleteDonatur(request);
            displayResponse(response);
            return response;
        } catch (Exception e) {
            Logger.error("Error response delete donatur : {}", e, PortalController.class);
            throw e;
        }
    }
    
    public Response updateDonatur(StoreDonaturRequest request) {
        try {
            Response response = portalService.updateDonatur(request);
            displayResponse(response);
            return response;
        } catch (Exception e) {
            Logger.error("Error respose update donatur : {}", e, PortalController.class);
            throw e;
        }
    }
}
