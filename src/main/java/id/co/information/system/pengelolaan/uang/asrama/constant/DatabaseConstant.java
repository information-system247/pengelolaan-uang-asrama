/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.constant;

/**
 *
 * @author shinriokudo
 */
public class DatabaseConstant {
    
    private static final String HOST = "localhost"; //45.13.255.84
    private static final String DATABASE = "asrama"; //u7611492_asrama
    private static final String PARAM = "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    public static final String URL = "jdbc:mysql://" + HOST + "/" + DATABASE + "?" + PARAM;
    public static final String USERNAME = "root"; //u7611492_user_asrama1
    public static final String PASSWORD = ""; //yw1CcfY;j$&5
}
