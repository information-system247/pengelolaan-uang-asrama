package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HistoriTransaksiKeluarDto implements Serializable {
    private static final long serialVersionUID = 9042038833594196226L;

    private Date tglTransaksi;
    private Long amount;
    private String keterangan;
}
