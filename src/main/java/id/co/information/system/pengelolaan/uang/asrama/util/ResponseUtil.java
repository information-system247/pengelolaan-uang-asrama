/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.util;

import id.co.information.system.pengelolaan.uang.asrama.constant.MessageConstant;
import id.co.information.system.pengelolaan.uang.asrama.dto.Response;
import id.co.information.system.pengelolaan.uang.asrama.dto.ResponseDto;

import static id.co.information.system.pengelolaan.uang.asrama.constant.MessageCodeConstant.SOMETHING_WENT_WRONG;
import static id.co.information.system.pengelolaan.uang.asrama.constant.MessageCodeConstant.SUCCESS;

/**
 *
 * @author shinriokudo
 */
public class ResponseUtil {
    
    public static Response buildSuccessResponse(Object data) {
        return new Response(new ResponseDto<>(SUCCESS, MessageConstant.getMessageUtilValue(SUCCESS), data));
    }
    
    public static Response buildPendingResponse(String messageCode) {
        return new Response(new ResponseDto<>(messageCode, MessageConstant.getMessageUtilValue(messageCode), null));
    }
    
    public static Response buildFailedResponse(String message) {
        return new Response(new ResponseDto<>(SOMETHING_WENT_WRONG, message, null));
    }
    
    public static Response buildPendingResponse(String messageCode, Object data) {
        return new Response(new ResponseDto<>(messageCode, MessageConstant.getMessageUtilValue(messageCode), data));
    }
}
