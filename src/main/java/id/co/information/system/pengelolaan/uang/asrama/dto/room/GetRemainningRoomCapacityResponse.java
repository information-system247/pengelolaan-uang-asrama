package id.co.information.system.pengelolaan.uang.asrama.dto.room;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetRemainningRoomCapacityResponse implements Serializable {
    private static final long serialVersionUID = 8385872580560708806L;

    private List<RemainningRoomCapacityDto> remainningRoomCapacityList;
}
