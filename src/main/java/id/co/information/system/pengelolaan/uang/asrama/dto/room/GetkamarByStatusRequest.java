package id.co.information.system.pengelolaan.uang.asrama.dto.room;

import id.co.information.system.pengelolaan.uang.asrama.constant.StatusKamarConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetkamarByStatusRequest implements Serializable {
    private static final long serialVersionUID = 6836266411289357030L;

    private StatusKamarConstant statusKamar;
}
