package id.co.information.system.pengelolaan.uang.asrama.controller;

import id.co.information.system.pengelolaan.uang.asrama.constant.HakAksesConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.MessageCodeConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.UserStatusConstant;
import id.co.information.system.pengelolaan.uang.asrama.dto.Response;
import id.co.information.system.pengelolaan.uang.asrama.dto.user.*;
import id.co.information.system.pengelolaan.uang.asrama.service.UserService;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;

public class UserController extends BaseController{

    private UserService userService = new UserService();

    public Response getUserByHakAksesAndStatusList() {
        try {
            Response getUserByHakAksesAndStatusResponse = userService.getUserByHakAksesAndStatus(GetUserByHakAksesAndStatusRequest.builder().userStatus(UserStatusConstant.AKTIF).hakAkses(HakAksesConstant.ANGGOTA).build());
            displayResponse(getUserByHakAksesAndStatusResponse);
            return getUserByHakAksesAndStatusResponse;
        } catch (Exception e) {
            Logger.error("Error response user by access rights and status list : ", e, UserController.class);
            throw e;
        }
    }

    public int numberOfTenants() {
        try {
            Response response = getUserByHakAksesAndStatusList();
            if (MessageCodeConstant.SUCCESS.equals(response.getResponseDto().getMessageCode())) {
                GetUserByHakAksesAndStatusResponse getUserByHakAksesAndStatusResponse = (GetUserByHakAksesAndStatusResponse) response.getResponseDto().getData();
                return getUserByHakAksesAndStatusResponse.getUserList().size();
            }
            return 0;
        } catch (Exception e) {
            Logger.error("Error get number of tenants : ", e, UserController.class);
            throw e;
        }
    }

    public Response getUserList() {
        try {
            Response getUserListResponse = userService.getUserList();
            displayResponse(getUserListResponse);
            return getUserListResponse;
        } catch (Exception e) {
            Logger.error("Error response user list : ", e, UserController.class);
            throw e;
        }
    }

    public Response login(GetUserByUsernameAndPasswordRequest request) {
        try {
            Response getUserByUsernameAndPasswordResponse = userService.login(request);
            displayResponse(getUserByUsernameAndPasswordResponse);
            return getUserByUsernameAndPasswordResponse;
        } catch (Exception e) {
            Logger.error("Error response login : ", e, UserController.class);
            throw e;
        }
    }

    public Response updateUser(StoreUserRequest request) {
        try {
            Response userOptional = userService.updateUser(request);
            displayResponse(userOptional);
            return userOptional;
        } catch (Exception e) {
            Logger.error("Error response update user : ", e, UserController.class);
            throw e;
        }
    }

    public Response getUserByNik(GetUserByNikRequest request) {
        try {
            Response response = userService.getUserByNik(request);
            displayResponse(response);
            return response;
        } catch (Exception e) {
            Logger.error("Error response get user by nik : ", e, UserController.class);
            throw e;
        }
    }

    public Response storeUser(StoreUserRequest request) {
        try {
            Response response = userService.storeUser(request);
            displayResponse(response);
            return response;
        } catch (Exception e) {
            Logger.error("Error response store user : ", e, UserController.class);
            throw e;
        }
    }
    
    public Response deleteUser(GetUserByNikRequest request) {
        try {
            Response response = userService.deleteUser(request);
            displayResponse(response);
            return response;
        } catch (Exception e) {
            Logger.error("Error response delete user : ", e, UserController.class);
            throw e;
        }
    }
    
    public Response deleteUserById(GetUserByNikRequest request) {
        try {
            Response response = userService.deleteUserById(request);
            displayResponse(response);
            return response;
        } catch (Exception e) {
            Logger.error("Error response delete user by id : ", e, UserController.class);
            throw e;
        }
    }
}
