/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package id.co.information.system.pengelolaan.uang.asrama.constant;

/**
 *
 * @author wahyu
 */
public class PanelMenuConstant {
    
    public static final String PANEL_MENU_DONATUR = "donatur";
    public static final String PANEL_MENU_BERANDA = "beranda";
    public static final String PANEL_MENU_PENYEWA = "penyewa";
    public static final String PANEL_SHOW_TRANSAKSI_MASUK = "show transaksi masuk";
    public static final String PANEL_SHOW_TRANSAKSI_KELUAR = "show transaksi keluar";
    public static final String PANEL_MENU_KAMAR = "kamar";
    public static final String PANEL_MENU_LAPORAN = "laporan";
    public static final String PANEL_MENU_LAPORAN_TUNGGAKAN = "laporan tunggakan";
    public static final String PANEL_MENU_BERANDA_PENYEWA = "beranda penyewa";
    public static final String PANEL_MENU_LAPORAN_PENYEWA = "laporan penyewa";
}
