package id.co.information.system.pengelolaan.uang.asrama.repository;

import id.co.information.system.pengelolaan.uang.asrama.constant.GenderConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.StatusKamarConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.UserStatusConstant;
import id.co.information.system.pengelolaan.uang.asrama.model.Kamar;
import id.co.information.system.pengelolaan.uang.asrama.model.User;
import id.co.information.system.pengelolaan.uang.asrama.util.DateUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.ListUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserRepository extends CommonBaseRepository{

    private List<User> getFindList(String sql) {
        List<User> userList = new ArrayList<>();
        try {
            resultSet = find(sql);
            if (resultSet == null) {
                return userList;
            }
            while(resultSet.next()) {
                userList.add(userBuilder(resultSet));
            }
            return userList;
        } catch (SQLException e) {
            Logger.error("Error find list data user : ", e, UserRepository.class);
            return userList;
        } finally {
            closeConnection(true);
        }
    }

    private Optional<User> getFindOptional(String sql) {
        try {
            resultSet = find(sql);
            if (resultSet == null) {
                return Optional.empty();
            }
            if (resultSet.next()) {
                return Optional.of(userBuilder(resultSet));
            }
            return Optional.empty();
        } catch (SQLException e) {
            Logger.error("Error find optional data user : ", e, UserRepository.class);
            return Optional.empty();
        } finally {
            closeConnection(true);
        }
    }

    private User userBuilder(ResultSet resultSet) {
        try {
            return User.builder()
                    .id(Long.parseLong(resultSet.getString("u.id")))
                    .nik(resultSet.getString("nik"))
                    .namaLengkap(resultSet.getString("nama_lengkap"))
                    .gender(GenderConstant.getGenderCodeUtil(resultSet.getString("gender")))
                    .alamat(resultSet.getString("alamat"))
                    .noHp(resultSet.getString("no_hp"))
                    .username(resultSet.getString("username"))
                    .password(resultSet.getString("password"))
                    .status(UserStatusConstant.AKTIF.name().equals(resultSet.getString("u.status")) ? UserStatusConstant.AKTIF : UserStatusConstant.TIDAK_AKTIF)
                    .hakAkses(resultSet.getString("hak_akses"))
                    .kamar(Kamar.builder()
                            .id(Long.parseLong(resultSet.getString("k.id")))
                            .noKamar(resultSet.getString("no_kamar"))
                            .kapasitas(Integer.parseInt(resultSet.getString("kapasitas")))
                            .statusKamar(StatusKamarConstant.KOSONG.name().equals(resultSet.getString("k.status")) ? StatusKamarConstant.KOSONG : StatusKamarConstant.ISI)
                            .build())
                    .tglMasuk(DateUtil.toDateTime(resultSet.getString("tgl_masuk"), DateUtil.YYYY_MM_DD_HH_MM_SS))
                    .monthlyPayment(Long.parseLong(resultSet.getString("tagihan_bulanan")))
                    .foto(resultSet.getString("foto"))
                    .build();
        } catch (NumberFormatException | SQLException e) {
            Logger.error("Error Builder user repository : ", e, UserRepository.class);
            return User.builder().build();
        }
    }

    public Boolean save(User user) {
        try {
            StringBuilder query = new StringBuilder("INSERT INTO user(nik, nama_lengkap, gender, alamat, no_hp, username, password, status, hak_akses, no_kamar, tgl_masuk, tagihan_bulanan, foto) VALUES ('")
                    .append(user.getNik()).append("','")
                    .append(user.getNamaLengkap()).append("','")
                    .append(user.getGender().getGender()).append("','")
                    .append(user.getAlamat()).append("','")
                    .append(user.getNoHp()).append("','")
                    .append(user.getUsername()).append("','")
                    .append(user.getPassword()).append("','")
                    .append(user.getStatus()).append("','")
                    .append(user.getHakAkses()).append("','")
                    .append(user.getKamar().getNoKamar()).append("','")
                    .append(DateUtil.toString(user.getTglMasuk(), DateUtil.YYYY_MM_DD_HH_MM_SS)).append("','")
                    .append(user.getMonthlyPayment()).append("','")
                    .append(user.getFoto()).append("')");
            return execute(new String(query));
        } catch (Exception e) {
            Logger.error("Error query save user : {}", e, UserRepository.class);
            return false;
        } finally {
            closeConnection(false);
        }
    }

    public Boolean update(User user) {
        try {
            StringBuilder query = new StringBuilder("UPDATE user SET nama_lengkap = '")
                    .append(user.getNamaLengkap())
                    .append("', gender = '").append(user.getGender().getGender())
                    .append("', alamat = '").append(user.getAlamat())
                    .append("', no_hp = '").append(user.getNoHp())
                    .append("', username = '").append(user.getUsername())
                    .append("', password = '").append(user.getPassword())
                    .append("', status = '").append(user.getStatus())
                    .append("', hak_akses = '").append(user.getHakAkses())
                    .append("', no_kamar = '").append(user.getKamar().getNoKamar())
                    .append("', tgl_masuk = '").append(DateUtil.toString(user.getTglMasuk(), DateUtil.YYYY_MM_DD_HH_MM_SS))
                    .append("', tagihan_bulanan = '").append(user.getMonthlyPayment())
                    .append("', foto = '").append(user.getFoto())
                    .append("' WHERE nik = '").append(user.getNik()).append("'");
            return execute(new String(query));
        } catch (Exception e) {
            Logger.error("Error query update user : ", e, UserRepository.class);
            return false;
        } finally {
            closeConnection(false);
        }
    }

    public Boolean deleteById(Long id) {
        try {
            String query = "DELETE FROM user WHERE id="+ id;
            return execute(query);
        } catch (Exception e) {
            Logger.error("Error query delete user : ", e, UserRepository.class);
            return false;
        } finally {
            closeConnection(false);
        }
    }

    @SuppressWarnings("all")
    public List<User> findAll() {
        return getFindList("SELECT * FROM user u JOIN kamar k ON u.no_kamar=k.no_kamar ORDER BY nama_lengkap ASC");
    }

    @SuppressWarnings("all")
    public Optional<User> findById(Long id) {
        return getFindOptional("SELECT *, k.id AS kamar_id, k.status AS status_kamar FROM user u JOIN kamar k ON u.no_kamar=k.no_kamar WHERE u.id= "+ id);
    }

    @SuppressWarnings("all")
    public Optional<User> findByNik(String nik) {
        return getFindOptional("SELECT * FROM user u JOIN kamar k ON u.no_kamar=k.no_kamar WHERE nik = '"+ nik +"'");
    }

    @SuppressWarnings("all")
    public Optional<User> findByUsername(String username) {
        return getFindOptional("SELECT * FROM user u JOIN kamar k ON u.no_kamar=k.no_kamar WHERE username = '"+ username +"'");
    }

    @SuppressWarnings("all")
    public Optional<User> findByUsernameAndPassword(String username, String password) {
        return getFindOptional("SELECT * FROM user u JOIN kamar k ON u.no_kamar=k.no_kamar WHERE username = '"+ username +"' AND password = '"+ password +"'");
    }

    @SuppressWarnings("all")
    public List<User> findNotExistId(Long id) {
        return getFindList("SELECT * FROM user u JOIN kamar k ON u.no_kamar=k.no_kamar WHERE id != '"+ id +"' ORDER BY nama_lengkap ASC");
    }

    @SuppressWarnings("all")
    public Optional<User> findFirstByIdExistsHistoryTransaksi(Long id) {
        return getFindOptional("SELECT * FROM user u JOIN kamar k ON u.no_kamar=k.no_kamar WHERE EXISTS (SELECT * FROM histori_transaksi ht WHERE ht.user_id=u.id AND u.id = '"+ id +"') ORDER BY tgl_transaksi DESC LIMIT 1");
    }

    @SuppressWarnings("all")
    public List<User> findByNoKamarIn(List<String> noKamarList) {
        return getFindList("SELECT * FROM user u JOIN kamar k ON u.no_kamar=k.no_kamar WHERE u.no_kamar IN('"+ ListUtil.toString(noKamarList.toString()) +"') ORDER BY nama_lengkap ASC");
    }

    @SuppressWarnings("all")
    public Optional<User> findByHakAkses(String hakAkses) {
        return getFindOptional("SELECT * FROM user u JOIN kamar k ON u.no_kamar=k.no_kamar WHERE hak_akses='"+ hakAkses +"'");
    }

    @SuppressWarnings("all")
    public List<User> findByStatus(String status) {
        return getFindList("SELECT * FROM user u JOIN kamar k ON u.no_kamar=k.no_kamar WHERE u.status='"+ status +"' ORDER BY nama_lengkap ASC");
    }

    @SuppressWarnings("all")
    public List<User> findByHakAksesAndStatusOrderByNamaLengkapAsc(String hakAkses, String status) {
        return getFindList("SELECT * FROM user u JOIN kamar k ON u.no_kamar=k.no_kamar WHERE u.hak_akses='"+ hakAkses +"' AND u.status='"+ status +"' ORDER BY nama_lengkap ASC");
    }
    @SuppressWarnings("all")
    public List<User> findByIdIn(List<Long> userId) {
        return getFindList("SELECT *, k.id AS kamar_id, k.status AS status_kamar FROM user u JOIN kamar k ON u.no_kamar=k.no_kamar WHERE u.id IN('"+ ListUtil.toString(userId.toString()) +"')");
    }
}
