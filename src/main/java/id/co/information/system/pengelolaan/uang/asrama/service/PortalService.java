package id.co.information.system.pengelolaan.uang.asrama.service;

import id.co.information.system.pengelolaan.uang.asrama.constant.HakAksesConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.StatusKamarConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.UserStatusConstant;
import id.co.information.system.pengelolaan.uang.asrama.dto.Response;
import id.co.information.system.pengelolaan.uang.asrama.dto.donatur.*;
import id.co.information.system.pengelolaan.uang.asrama.dto.room.*;
import id.co.information.system.pengelolaan.uang.asrama.model.Donatur;
import id.co.information.system.pengelolaan.uang.asrama.model.Kamar;
import id.co.information.system.pengelolaan.uang.asrama.model.User;
import id.co.information.system.pengelolaan.uang.asrama.repository.DonaturRepository;
import id.co.information.system.pengelolaan.uang.asrama.repository.KamarRepository;
import id.co.information.system.pengelolaan.uang.asrama.repository.UserRepository;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;
import id.co.information.system.pengelolaan.uang.asrama.util.ResponseUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static id.co.information.system.pengelolaan.uang.asrama.constant.InfoConstant.REQUEST;
import static id.co.information.system.pengelolaan.uang.asrama.constant.MessageCodeConstant.*;
import id.co.information.system.pengelolaan.uang.asrama.model.HistoriTransaksi;
import id.co.information.system.pengelolaan.uang.asrama.repository.HistoriTransaksiRepository;

public class PortalService {

    private final KamarRepository kamarRepository = new KamarRepository();
    private final DonaturRepository donaturRepository = new DonaturRepository();
    private final UserRepository userRepository = new UserRepository();
    private final HistoriTransaksiRepository historiTransaksiRepository = new HistoriTransaksiRepository();

    public Response getKamarList() {
        Logger.info("start getKamarList", PortalService.class);
        try {

            List<Kamar> kamarList = kamarRepository.findAll();
            if (kamarList.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            List<String> noKamarList = kamarList.stream()
                    .filter(kamar -> !StatusKamarConstant.KOSONG.equals(kamar.getStatusKamar()))
                    .map(Kamar::getNoKamar)
                    .collect(Collectors.toList());

            List<User> userList = userRepository.findByNoKamarIn(noKamarList);
            if (userList.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            userList = userList.stream()
                    .filter(user -> HakAksesConstant.ANGGOTA.name().equals(user.getHakAkses()))
                    .filter(user -> UserStatusConstant.AKTIF.equals(user.getStatus()))
                    .collect(Collectors.toList());

            Map<Kamar, Long> usedRoomCapacityMap = userList.stream().collect(Collectors.groupingBy(User::getKamar, Collectors.counting()));
            List<GetkamarDto> kamarDtoList = new ArrayList<>();
            usedRoomCapacityMap.forEach((kamar, amount) -> {
                int tersedia = kamar.getKapasitas() - amount.intValue();
                if ( tersedia > -1) {
                    GetkamarDto.GetkamarDtoBuilder kamarDtoBuilder = GetkamarDto.builder();
                    kamarDtoBuilder.noKamar(kamar.getNoKamar());
                    kamarDtoBuilder.kapasitas(kamar.getKapasitas());
                    kamarDtoBuilder.tersedia(tersedia);
                    kamarDtoBuilder.statusKamar(kamar.getStatusKamar());
                    if (tersedia == 0) {
                        kamarDtoBuilder.statusKamar(StatusKamarConstant.PENUH);
                    }
                    kamarDtoList.add(kamarDtoBuilder.build());
                }
            });

            kamarList.stream()
                    .filter(kamar -> StatusKamarConstant.KOSONG.equals(kamar.getStatusKamar()))
                    .forEach( kamar ->
                            kamarDtoList.add(GetkamarDto.builder()
                                    .noKamar(kamar.getNoKamar())
                                    .kapasitas(kamar.getKapasitas())
                                    .tersedia(kamar.getKapasitas())
                                    .statusKamar(StatusKamarConstant.KOSONG)
                                    .build())
                    );

            // todo : sorted by room number
            return ResponseUtil.buildSuccessResponse(GetKamarListResponse.builder()
                    .kamarList(kamarDtoList)
                    .build());
        } catch (Exception e) {
            Logger.error("Error room list : ", e, PortalService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response getKamarByNoKamar(GetKamarByNoKamarRequest request) {
        Logger.info("start getKamarByNoKamar", PortalService.class);
        Logger.info(REQUEST, request, PortalService.class);
        try {
            if (request.getNoKamar() == null || request.getNoKamar().isEmpty()) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Optional<Kamar> kamaOptional = kamarRepository.findByNoKamar(request.getNoKamar());
            if (kamaOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            return ResponseUtil.buildSuccessResponse(GetKamarByNoKamarResponse.builder()
                    .kamar(kamaOptional.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error room optional by room number : ", e, PortalService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response storeKamar(StoreKamarRequest request) {
        Logger.info("start storeKamar", PortalService.class);
        Logger.info(REQUEST, request, PortalService.class);
        try {
            if (request.getNoKamar() == null || request.getNoKamar().isEmpty() || request.getKapasitas() < 1) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Optional<Kamar> kamarOptional = kamarRepository.findByNoKamar(request.getNoKamar());
            if (kamarOptional.isPresent()) {
                return ResponseUtil.buildPendingResponse(ROOM_FOUND);
            }

            Boolean storeKamar = kamarRepository.save(Kamar.builder()
                    .noKamar(request.getNoKamar())
                    .kapasitas(request.getKapasitas())
                    .statusKamar(StatusKamarConstant.KOSONG)
                    .build());
            if (Boolean.FALSE.equals(storeKamar)) {
                return ResponseUtil.buildPendingResponse(SQL_ERROR);
            }

            Optional<Kamar> kamarOptional1 = kamarRepository.findByNoKamar(request.getNoKamar());
            return ResponseUtil.buildSuccessResponse(StoreKamarResponse.builder()
                    .kamar(kamarOptional1.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error room store : ", e, PortalService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response updateKamar(StoreKamarRequest request) {
        Logger.info("start updateKamar", PortalService.class);
        Logger.info(REQUEST, request, PortalService.class);
        try {
            if (request.getNoKamar() == null || request.getNoKamar().isEmpty() || 
                    request.getKapasitas() < 1 || request.getStatusKamar() == null) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Optional<Kamar> kamarOptional = kamarRepository.findByNoKamar(request.getNoKamar());
            if (kamarOptional.isPresent() && !kamarOptional.get().getNoKamar().equals(request.getNoKamar())) {
                return ResponseUtil.buildPendingResponse(ROOM_FOUND);
            }

            List<User> userList = userRepository.findByNoKamarIn(List.of(request.getNoKamar()));
            if (request.getKapasitas() < userList.size()) {
                return ResponseUtil.buildPendingResponse(CANNOT_UPDATE_CAPACITY);
            }

            Boolean updateKamar = kamarRepository.update(Kamar.builder()
                    .noKamar(request.getNoKamar())
                    .kapasitas(request.getKapasitas())
                    .statusKamar(request.getStatusKamar())
                    .build());
            if (Boolean.FALSE.equals(updateKamar)) {
                return ResponseUtil.buildPendingResponse(SQL_ERROR);
            }

            Optional<Kamar> kamarOptional1 = kamarRepository.findByNoKamar(request.getNoKamar());
            return ResponseUtil.buildSuccessResponse(StoreKamarResponse.builder()
                    .kamar(kamarOptional1.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error update room : ", e, PortalService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response deleteKamar(GetKamarByNoKamarRequest request) {
        Logger.info("start deleteKamar", PortalService.class);
        Logger.info(REQUEST, request, PortalService.class);
        try {
            if (request.getNoKamar() == null || request.getNoKamar().isEmpty()) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Optional<Kamar> kamarOptional = kamarRepository.findFisrtByNoKamarExistsUser(request.getNoKamar());
            if (kamarOptional.isPresent()) {
                return ResponseUtil.buildPendingResponse(CANNOT_DELETED);
            }

            Boolean deleteKamar = kamarRepository.deleteByNoKamar(request.getNoKamar());
            if (Boolean.FALSE.equals(deleteKamar)) {
                return ResponseUtil.buildPendingResponse(SQL_ERROR);
            }

            return ResponseUtil.buildSuccessResponse(DeleteKamarResponse.builder()
                    .success(deleteKamar)
                    .build());
        } catch (Exception e) {
            Logger.error("Error deleted room : ", e, PortalService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response storeDonatur(StoreDonaturRequest request) {
        Logger.info("start storeDonatur", PortalService.class);
        Logger.info(REQUEST, request, PortalService.class);
        try {
            if (request.getNik() == null || request.getNik().isEmpty() || 
                    request.getNamaLengkap() == null || request.getNamaLengkap().isEmpty() || 
                    request.getGender() == null || request.getAlamat() == null || request.getAlamat().isEmpty() || 
                    request.getNoHp() == null || request.getNoHp().isEmpty()) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Optional<Donatur> donaturOptional = donaturRepository.findByNik(request.getNik());
            if (donaturOptional.isPresent()) {
                return ResponseUtil.buildPendingResponse(NIK_FOUND);
            }

            Donatur donatur = Donatur.builder()
                    .nik(request.getNik())
                    .namaLengkap(request.getNamaLengkap())
                    .gender(request.getGender())
                    .alamat(request.getAlamat())
                    .noHp(request.getNoHp())
                    .build();
            Boolean storeDonatur = donaturRepository.save(donatur);
            if (Boolean.FALSE.equals(storeDonatur)) {
                return ResponseUtil.buildPendingResponse(SQL_ERROR);
            }

            Optional<Donatur> donaturOptional1 = donaturRepository.findByNik(request.getNik());
            return ResponseUtil.buildSuccessResponse(StoreDonaturResponse.builder()
                    .donatur(donaturOptional1.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error store donatur : {}", e, PortalService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response updateDonatur(StoreDonaturRequest request) {
        Logger.info("start updateDonatur", PortalService.class);
        Logger.info(REQUEST, request, PortalService.class);
        try {
            if (request.getNik() == null || request.getNik().isEmpty() || 
                    request.getNamaLengkap() == null || request.getNamaLengkap().isEmpty() || 
                    request.getGender() == null || request.getAlamat() == null || request.getAlamat().isEmpty() || 
                    request.getNoHp() == null || request.getNoHp().isEmpty()) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Optional<Donatur> donaturOptional = donaturRepository.findByNik(request.getNik());
            if (donaturOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            Donatur donatur = Donatur.builder()
                    .nik(request.getNik())
                    .namaLengkap(request.getNamaLengkap())
                    .gender(request.getGender())
                    .noHp(request.getNoHp())
                    .alamat(request.getAlamat())
                    .build();
            Boolean updateDonatur = donaturRepository.update(donatur);
            if (Boolean.FALSE.equals(updateDonatur)) {
                return ResponseUtil.buildPendingResponse(SQL_ERROR);
            }

            Optional<Donatur> donaturOptional1 = donaturRepository.findByNik(request.getNik());
            return ResponseUtil.buildSuccessResponse(StoreDonaturResponse.builder()
                    .donatur(donaturOptional1.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error updated doantur : ", e, PortalService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response deleteDonatur(GetDonaturByNikRequest request) {
        Logger.info("start deleteDonatur", PortalService.class);
        Logger.info(REQUEST, request, PortalService.class);
        try {
            if (request.getNik() == null || request.getNik().isEmpty()) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }
            
            Optional<Donatur> donaturOptional = donaturRepository.findByNik(request.getNik());
            if (donaturOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            Optional<HistoriTransaksi> historiTransaksiOptional = historiTransaksiRepository.findByDonaturIdLimit1(donaturOptional.get().getId());
            if (historiTransaksiOptional.isPresent()) {
                return ResponseUtil.buildPendingResponse(CANNOT_DELETED);
            }
            
            Boolean deleteDonatur = donaturRepository.deleteByNik(request.getNik());
            if (Boolean.FALSE.equals(deleteDonatur)) {
                return ResponseUtil.buildPendingResponse(SQL_ERROR);
            }

            return ResponseUtil.buildSuccessResponse(DeleteDonaturResponse.builder()
                    .success(deleteDonatur)
                    .build());
        } catch (Exception e) {
            Logger.error("Error deleted donatur : ", e, PortalService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response getDonaturByNik(GetDonaturByNikRequest request) {
        Logger.info("start getDonaturByNik", PortalService.class);
        Logger.info(REQUEST, request, PortalService.class);
        try {
            if (request.getNik() == null || request.getNik().isEmpty()) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            Optional<Donatur> donaturOptional = donaturRepository.findByNik(request.getNik());
            if (donaturOptional.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            return ResponseUtil.buildSuccessResponse(GetDonaturByNikResponse.builder()
                    .donatur(donaturOptional.orElse(null))
                    .build());
        } catch (Exception e) {
            Logger.error("Error donatur optional by nik : ", e, PortalService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response getKamarByStatus(GetkamarByStatusRequest request) {
        Logger.info("start getKamarByStatus", PortalService.class);
        Logger.info(REQUEST, request, PortalService.class);
        try {
            if (request.getStatusKamar() == null) {
                return ResponseUtil.buildPendingResponse(BAD_REQUEST);
            }

            List<Kamar> kamarList = kamarRepository.findByStatusIn(List.of(request.getStatusKamar()));
            if (kamarList.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            return ResponseUtil.buildSuccessResponse(GetKamarByStatusResponse.builder()
                    .kamarList(kamarList)
                    .build());
        } catch (Exception e) {
            Logger.error("Error get room by status : ", e, PortalService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response getDonaturList() {
        Logger.info("start getDonaturList", PortalService.class);
        try {
            List<Donatur> donaturList = donaturRepository.findAll();
            if (donaturList.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            return ResponseUtil.buildSuccessResponse(GetDonaturListResponse.builder()
                    .donaturList(donaturList)
                    .build());
        } catch (Exception e) {
            Logger.error("Error get donatur list : ", e, PortalService.class);
            return ResponseUtil.buildFailedResponse(e.getMessage());
        }
    }

    public Response getKamarKosongList() {
        Logger.info("start getKamarKosongList", PortalService.class);
        try {
            List<Kamar> kamarList = kamarRepository.findByStatusIn(List.of(StatusKamarConstant.KOSONG, StatusKamarConstant.ISI));
            Logger.info("kamar list size : ", kamarList.size(), PortalService.class);
            if (kamarList.isEmpty()) {
                return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
            }

            List<String> nokamarList = kamarList.stream()
                    .map(Kamar::getNoKamar)
                    .collect(Collectors.toList());
            List<User> userList = userRepository.findByNoKamarIn(nokamarList);
            Logger.info("user list size : ", userList.size(), PortalService.class);
            if (userList.isEmpty()) {
                List<Kamar> kamars = kamarRepository.findByNoKamarIn(nokamarList);
                if (kamars.isEmpty()) {
                    return ResponseUtil.buildPendingResponse(DATA_NOT_FOUND);
                }
                List<GetKamarKosongDto> kamarKosongs = kamars.stream()
                        .map(kamar -> GetKamarKosongDto.builder()
                                .noKamar(kamar.getNoKamar())
                                .kapasitas(kamar.getKapasitas())
                                .terisi(0)
                                .tersedia(0)
                                .build())
                        .collect(Collectors.toList());
                return ResponseUtil.buildSuccessResponse(GetKamarKosongListResponse.builder()
                        .kamarList(kamarKosongs)
                        .build());
            }

            userList = userList.stream()
                    .filter(user -> HakAksesConstant.ANGGOTA.name().equals(user.getHakAkses()))
                    .filter(user -> UserStatusConstant.AKTIF.equals(user.getStatus()))
                    .collect(Collectors.toList());

            Map<Kamar, Long> usedRoomCapacityMap = userList.stream().collect(Collectors.groupingBy(User::getKamar, Collectors.counting()));
            List<GetKamarKosongDto> kamarKosongList = new ArrayList<>();
            usedRoomCapacityMap.forEach((kamar, amount) -> {
                int tersedia = kamar.getKapasitas() - amount.intValue();
                if ( tersedia != 0) {
                    kamarKosongList.add(GetKamarKosongDto.builder()
                            .noKamar(kamar.getNoKamar())
                            .kapasitas(kamar.getKapasitas())
                            .terisi(amount.intValue())
                            .tersedia(tersedia)
                            .build());
                }
            });

            kamarList.stream().filter(kamar -> StatusKamarConstant.KOSONG.equals(kamar.getStatusKamar())).forEach(kamar -> {
                kamarKosongList.add(GetKamarKosongDto.builder()
                        .noKamar(kamar.getNoKamar())
                        .kapasitas(kamar.getKapasitas())
                        .terisi(0)
                        .tersedia(kamar.getKapasitas())
                        .build());
            });

            return ResponseUtil.buildSuccessResponse(GetKamarKosongListResponse.builder()
                    .kamarList(kamarKosongList)
                    .build());
        } catch (Exception e) {
            Logger.error("Error get empty room list : ", e, PortalService.class);
            return ResponseUtil.buildSuccessResponse(e.getMessage());
        }
    }
}
