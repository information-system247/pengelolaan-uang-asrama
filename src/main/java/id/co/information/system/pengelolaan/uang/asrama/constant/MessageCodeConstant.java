/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.constant;

/**
 *
 * @author shinriokudo
 */
public class MessageCodeConstant {
    
    public static final String SUCCESS = "200";
    public static final String SOMETHING_WENT_WRONG = "0001";
    public static final String DATA_NOT_FOUND = "0002";
    public static final String BAD_REQUEST = "0003";
    public static final String SQL_ERROR = "0004";
    public static final String USERNAME_FOUND = "0005";
    public static final String NIK_FOUND = "0006";
    public static final String USERNAME_OR_PASSWORD_WRONG = "0007";
    public static final String CANNOT_DELETED = "0008";
    public static final String BENDAHARA_IS_USED = "0009";
    public static final String ROOM_FOUND = "0010";
    public static final String FULL_ROOM = "0011";
    public static final String CANNOT_UPDATE_CAPACITY = "0012";
}
