package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import id.co.information.system.pengelolaan.uang.asrama.constant.FromTransactionConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetHistoriTransaksiMasukListByFromTransaksiRequest implements Serializable {
    private static final long serialVersionUID = 2860473013436076069L;

    private FromTransactionConstant fromTransaksi;
    private String keyword;
}
