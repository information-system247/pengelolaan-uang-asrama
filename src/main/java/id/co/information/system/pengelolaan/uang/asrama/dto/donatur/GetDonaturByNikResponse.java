package id.co.information.system.pengelolaan.uang.asrama.dto.donatur;

import id.co.information.system.pengelolaan.uang.asrama.model.Donatur;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetDonaturByNikResponse implements Serializable {
    private static final long serialVersionUID = 1589778909601978672L;

    private Donatur donatur;
}
