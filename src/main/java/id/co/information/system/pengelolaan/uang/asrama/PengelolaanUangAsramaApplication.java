/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama;

import id.co.information.system.pengelolaan.uang.asrama.constant.GenderConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.HakAksesConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.StatusKamarConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.UserStatusConstant;
import id.co.information.system.pengelolaan.uang.asrama.dto.room.StoreKamarRequest;
import id.co.information.system.pengelolaan.uang.asrama.dto.user.StoreUserRequest;
import id.co.information.system.pengelolaan.uang.asrama.model.Kamar;
import id.co.information.system.pengelolaan.uang.asrama.model.User;
import id.co.information.system.pengelolaan.uang.asrama.service.PortalService;
import id.co.information.system.pengelolaan.uang.asrama.service.UserService;
import id.co.information.system.pengelolaan.uang.asrama.util.DateUtil;
import id.co.information.system.pengelolaan.uang.asrama.util.Logger;
import id.co.information.system.pengelolaan.uang.asrama.view.Login;
import java.util.Date;

/**
 *
 * @author shinriokudo
 */
public class PengelolaanUangAsramaApplication {
    
    public static void main(String[] args) {
        new Login().setVisible(true);
//        UserService userService = new UserService();
//        PortalService portalService = new PortalService();
//        Logger.info("date : {}", DateUtil.toDateTime("Tue Mar 15 01:13:55 ICT 2022", DateUtil.YYYY_MM_DD), PengelolaanUangAsramaApplication.class);
//        Logger.info("date : ", DateUtil.toString(new Date(), DateUtil.YYYY_MM_DD), PengelolaanUangAsramaApplication.class);
//        portalService.storeKamar(StoreKamarRequest.builder()
//                .noKamar("K001")
//                .kapasitas(1)
//                .statusKamar(StatusKamarConstant.KOSONG)
//                .build());
//        userService.storeUser(StoreUserRequest.builder()
//                .user(User.builder()
//                        .nik("1234567890")
//                        .namaLengkap("wahyu imam")
//                        .gender(GenderConstant.PRIA)
//                        .alamat("test")
//                        .noHp("088980006642")
//                        .username("admin")
//                        .password("admin")
//                        .status(UserStatusConstant.AKTIF)
//                        .hakAkses(HakAksesConstant.BENDAHARA.toString())
//                        .kamar(Kamar.builder()
//                                .noKamar("K001")
//                                .build())
//                        .tglMasuk(new Date())
//                        .monthlyPayment(0L)
//                        .build())
//                .build());
//        Logger.info(userService.getUserList().toString(), PengelolaanUangAsramaApplication.class);
//        Logger.info(GenderConstant.getGenderCodeUtil("P").toString(), PengelolaanUangAsramaApplication.class);
    }
}
