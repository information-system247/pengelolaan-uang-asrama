/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.information.system.pengelolaan.uang.asrama.dto;

import java.io.Serializable;
import lombok.Data;

@Data
public class Response implements Serializable{
    private static final long serialVersionUID = -3578883128442507565L;

    private ResponseDto<? extends Object> responseDto;
    
    public Response(ResponseDto<? extends Object> data) {
        this.responseDto = data;
    }
}
