package id.co.information.system.pengelolaan.uang.asrama.dto.room;

import id.co.information.system.pengelolaan.uang.asrama.model.Kamar;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetKamarByStatusResponse implements Serializable {
    private static final long serialVersionUID = 4793486120279433104L;

    private List<Kamar> kamarList;
}
