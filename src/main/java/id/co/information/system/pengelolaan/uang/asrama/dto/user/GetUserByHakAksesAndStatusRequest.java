package id.co.information.system.pengelolaan.uang.asrama.dto.user;

import id.co.information.system.pengelolaan.uang.asrama.constant.HakAksesConstant;
import id.co.information.system.pengelolaan.uang.asrama.constant.UserStatusConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUserByHakAksesAndStatusRequest implements Serializable {
    private static final long serialVersionUID = -3879457598833548390L;

    private HakAksesConstant hakAkses;
    private UserStatusConstant userStatus;
}
