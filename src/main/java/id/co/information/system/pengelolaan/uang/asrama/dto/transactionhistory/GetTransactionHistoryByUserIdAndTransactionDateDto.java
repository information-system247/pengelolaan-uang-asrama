package id.co.information.system.pengelolaan.uang.asrama.dto.transactionhistory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetTransactionHistoryByUserIdAndTransactionDateDto implements Serializable {
    private static final long serialVersionUID = 6575770007717358705L;

    private Date transactionDate;
    private Integer jangkaWaktu;
    private Date dueDate;
    private Long total;
}
