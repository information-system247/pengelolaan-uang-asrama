-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 15, 2022 at 11:16 PM
-- Server version: 10.5.15-MariaDB-cll-lve
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asrama`
--

-- --------------------------------------------------------

--
-- Table structure for table `donatur`
--

CREATE TABLE `donatur` (
  `id` int(11) NOT NULL,
  `nik` varchar(30) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `gender` enum('L','P') NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_hp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `donatur`
--

INSERT INTO `donatur` (`id`, `nik`, `nama_lengkap`, `gender`, `alamat`, `no_hp`) VALUES
(8, '2152564758', 'Yeni Sara', 'P', 'Playen', '081876521435');

-- --------------------------------------------------------

--
-- Table structure for table `histori_transaksi`
--

CREATE TABLE `histori_transaksi` (
  `id` int(11) NOT NULL,
  `transaksi_id` varchar(15) NOT NULL,
  `tgl_transaksi` datetime DEFAULT current_timestamp(),
  `tipe_transaksi` enum('MASUK','KELUAR') NOT NULL,
  `transaksi_dari` varchar(25) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `jangka_waktu` int(2) DEFAULT NULL,
  `total_per_bulan` int(11) DEFAULT NULL,
  `jatuh_tempo` datetime DEFAULT NULL,
  `donatur_id` int(11) DEFAULT NULL,
  `nama_transaksi` varchar(50) DEFAULT NULL,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `sisa_saldo` int(11) DEFAULT NULL,
  `create_at` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `histori_transaksi`
--

INSERT INTO `histori_transaksi` (`id`, `transaksi_id`, `tgl_transaksi`, `tipe_transaksi`, `transaksi_dari`, `user_id`, `jangka_waktu`, `total_per_bulan`, `jatuh_tempo`, `donatur_id`, `nama_transaksi`, `nama_lengkap`, `keterangan`, `total`, `sisa_saldo`, `create_at`) VALUES
(24, 'TRX202203140001', '2022-03-14 00:28:49', 'MASUK', 'DONATUR', 0, 0, 0, NULL, 8, 'null', 'null', 'test', 1000000, 1000000, 'wahyu imam'),
(25, 'TRX202203140002', '2022-03-14 02:59:20', 'MASUK', 'ANGGOTA', 31, 1, 400000, '2022-04-14 05:58:08', 0, 'null', 'null', 'bayar kos', 400000, 1400000, 'wahyu imam'),
(26, 'TRX202203140003', '2022-03-14 03:02:04', 'MASUK', 'LAINNYA', 0, 0, 0, NULL, 0, 'test', 'udin', 'test', 600000, 2000000, 'wahyu imam'),
(27, 'TRX202203140004', '2022-03-14 03:04:47', 'KELUAR', 'null', 0, 0, 0, NULL, 0, 'null', 'null', 'bayar wifi, sampah, pdam', 300000, 1700000, 'wahyu imam'),
(28, 'TRX202203140005', '2022-03-14 03:20:38', 'MASUK', 'ANGGOTA', 32, 1, 500000, '2022-04-14 10:08:08', 0, 'null', 'null', 'bayar kos', 500000, 2200000, 'wahyu imam'),
(29, 'TRX202203150001', '2022-03-15 01:21:55', 'MASUK', 'ANGGOTA', 33, 2, 600000, '2022-05-15 08:20:33', 0, 'null', 'null', 'lunas', 1200000, 3400000, 'wahyu imam'),
(30, 'TRX202203150002', '2022-03-15 01:23:27', 'KELUAR', 'null', 0, 0, 0, NULL, 0, 'null', 'null', 'baksos', 50000, 3350000, 'wahyu imam');

-- --------------------------------------------------------

--
-- Table structure for table `kamar`
--

CREATE TABLE `kamar` (
  `id` int(11) NOT NULL,
  `no_kamar` varchar(20) NOT NULL,
  `kapasitas` int(1) NOT NULL,
  `status` enum('KOSONG','ISI','PENUH') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kamar`
--

INSERT INTO `kamar` (`id`, `no_kamar`, `kapasitas`, `status`) VALUES
(7, 'K001', 1, 'PENUH'),
(11, 'K003', 6, 'KOSONG'),
(12, 'K002', 8, 'KOSONG'),
(13, 'K021', 7, 'KOSONG'),
(14, 'lk21', 3, 'KOSONG');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nik` varchar(30) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `gender` enum('L','P') NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` enum('AKTIF','TIDAK_AKTIF') NOT NULL DEFAULT 'AKTIF',
  `hak_akses` varchar(20) NOT NULL,
  `no_kamar` varchar(20) NOT NULL,
  `tgl_masuk` datetime DEFAULT current_timestamp(),
  `tagihan_bulanan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nik`, `nama_lengkap`, `gender`, `alamat`, `no_hp`, `username`, `password`, `status`, `hak_akses`, `no_kamar`, `tgl_masuk`, `tagihan_bulanan`) VALUES
(25, '1234567890', 'wahyu imam', 'L', 'test', '088980006642', 'admin', 'DWgk42NwwHZEhLvsWYm81w==', 'AKTIF', 'BENDAHARA', 'K001', '2022-03-12 23:10:17', 0),
(31, '12343576788', 'sasya', 'P', 'jogja', '0888980657', 'user', 'nauufbvnsNSoxyKOJ7ALxg==', 'AKTIF', 'ANGGOTA', 'K002', '2022-03-14 05:58:08', 450000),
(32, '125465346', 'inka', 'P', 'tets', '9079878564', 'user2', '9Ls+/De7s1UFG90rCLNMSQ==', 'AKTIF', 'ANGGOTA', 'K003', '2022-03-14 03:08:08', 500000),
(33, '6754321', 'kristi putri?><L{:\"????', 'P', 'samirono', '987643', 'kristi', 'DeoxEUfgFxy9sWjEneeKWA==', 'AKTIF', 'ANGGOTA', 'K002', '2022-03-15 01:20:33', 600000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `donatur`
--
ALTER TABLE `donatur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nik` (`nik`);

--
-- Indexes for table `histori_transaksi`
--
ALTER TABLE `histori_transaksi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `transaksi_id` (`transaksi_id`);

--
-- Indexes for table `kamar`
--
ALTER TABLE `kamar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nama_no_kamar` (`no_kamar`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nik` (`nik`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `donatur`
--
ALTER TABLE `donatur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `histori_transaksi`
--
ALTER TABLE `histori_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `kamar`
--
ALTER TABLE `kamar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
